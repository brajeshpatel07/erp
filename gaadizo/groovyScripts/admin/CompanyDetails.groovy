import java.util.*
import java.sql.Timestamp;
import org.apache.ofbiz.entity.*
import org.apache.ofbiz.base.util.*
import org.apache.ofbiz.entity.util.*
import org.apache.ofbiz.entity.GenericValue;
import org.apache.ofbiz.entity.condition.*

import org.apache.ofbiz.base.util.StringUtil;
module = "CustomerDetails.groovy"
partyId = parameters.partyId;
if(UtilValidate.isEmpty(partyId) && UtilValidate.isNotEmpty(parameters.companyId)){
	partyId = parameters.companyId;
}
orderHeaderAndJobCardGV = null;
vehicleList = null;
if(UtilValidate.isNotEmpty(partyId)) {
    
    println("======partyId=====12121212====="+partyId+"===========");
    partyGV = from("Party").where("partyId", partyId).queryOne()
    		println("======partyId=====12121212====="+partyGV.partyTypeId+"===========");
    partyAndPersonGV = null;
    userLoginGV = null;
    partyAndPersonList = null;
    //get the order types
    if("PERSON".equals(partyGV.partyTypeId)){
    	partyAndPersonList = from("PartyAndPerson").where("partyId", partyId).queryList()
    } else  if("PARTY_GROUP".equals(partyGV.partyTypeId)){
    	partyAndPersonList = from("PartyAndGroup").where("partyId", partyId).queryList()
    }
    println("======partyId=====12121212====="+partyAndPersonList+"===========");
    if(UtilValidate.isNotEmpty(partyAndPersonList)) {
    	partyAndPersonGV = EntityUtil.getFirst(partyAndPersonList);
    	println("======partyId=====12121212====="+partyAndPersonGV.partyTypeId+"===========");
    	context.partyAndPersonGV = partyAndPersonGV;

	partyContactDetailByPurposeList = from("PartyContactDetailByPurpose").where("partyId", partyId).queryList();
	shippingPCDPGV=null;
	billingPCDPGV=null;
	emailPCDPGV=null;
	phonePCDPGV=null;
	if(UtilValidate.isNotEmpty(partyContactDetailByPurposeList)) {
		for(pcdpGV in partyContactDetailByPurposeList)
		{
			if("BILLING_LOCATION".equals(pcdpGV.contactMechPurposeTypeId)){
				billingPCDPGV = pcdpGV;
			}
			if("SHIPPING_LOCATION".equals(pcdpGV.contactMechPurposeTypeId)){
				shippingPCDPGV = pcdpGV;
			} else if("GENERAL_LOCATION".equals(pcdpGV.contactMechPurposeTypeId)){
				shippingPCDPGV = pcdpGV;
			}
			
			if("PRIMARY_EMAIL".equals(pcdpGV.contactMechPurposeTypeId)){
				emailPCDPGV = pcdpGV;
			}
			if("PHONE_MOBILE".equals(pcdpGV.contactMechPurposeTypeId)){
				phonePCDPGV = pcdpGV;
			}
			
				
		}
	}
	addressList = StringUtil.split(shippingPCDPGV.address1,",");
	addressMap = new HashMap();
	int addresscount = 1;
	for(String address : addressList) {
		addressMap.put("address"+addresscount,address);
		addresscount++;
	}
	println("====70070707==addressMap=========="+addressMap+"===========");
	context.addressMap = addressMap;
	context.billingPCDPGV=billingPCDPGV;
	context.shippingPCDPGV=shippingPCDPGV;
	context.emailPCDPGV=emailPCDPGV;
	context.phonePCDPGV=phonePCDPGV;
	
	orderRoleList= from("OrderRole").where("partyId", partyId, "roleTypeId", "BILL_TO_CUSTOMER").queryList();
	if(UtilValidate.isNotEmpty(orderRoleList)) {
		orderRoleGV = EntityUtil.getFirst(orderRoleList);
		println("======orderRoleGV=========="+orderRoleGV+"===========");
		orderHeaderAndJobCardList = from("OrderHeaderAndJobCard").where("customerId", partyId)orderBy("orderDate DESC", "orderId DESC").queryList();
		context.orderHeaderAndJobCardList=orderHeaderAndJobCardList;
		if(UtilValidate.isNotEmpty(orderHeaderAndJobCardList)) {
			
			orderHeaderAndJobCardGV = EntityUtil.getFirst(orderHeaderAndJobCardList);
			context.orderHeaderAndJobCardGV=orderHeaderAndJobCardGV;
			vehicleModelIds = EntityUtil.getFieldListFromEntityList(orderHeaderAndJobCardList, "vehicleModelId", true);
			//get the customer Details
			vehiclesCond = [];
			vehiclesCond.add(EntityCondition.makeCondition("modelId", EntityOperator.IN, vehicleModelIds));
			System.out.println("vehiclesCond==========%%%%%%%%%%%%%%%=============================="+vehiclesCond);
			vehicleList = from("Vehicle").where(vehiclesCond).orderBy("modelId").queryList();
			System.out.println("vehicleList==========%%%%%%%%%%%%%%%=============================="+vehicleList);
			if(UtilValidate.isNotEmpty(vehicleList)) {
				lastVehicleGV=vehicleList;
				context.lastVehicleGV=lastVehicleGV;
				println("================lastVehicleGV============="+lastVehicleGV);
				context.vehicleList=vehicleList;
			}
		}
	}
		
}
println("===========orderHeaderAndJobCardGV====="+orderHeaderAndJobCardGV+"===========");

//Few Static Values
context.insuranceCompany="HDFC";
context.paymentMode="Cash On Delivery";
context.customerType="Gaadizo";
}


//============================

