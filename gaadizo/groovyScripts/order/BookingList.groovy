import java.util.*
import java.sql.Timestamp
import org.apache.ofbiz.entity.*
import org.apache.ofbiz.base.util.*
import org.apache.ofbiz.entity.util.*
import org.apache.ofbiz.entity.GenericValue;
import org.apache.ofbiz.entity.condition.*

module = "BookingList.groovy"
jobCardList = null;
if(UtilValidate.isNotEmpty(parameters.bookingId) || UtilValidate.isNotEmpty(parameters.bookingId) || UtilValidate.isNotEmpty(parameters.fromDate) || UtilValidate.isNotEmpty(parameters.thruDate) || UtilValidate.isNotEmpty(parameters.CustomerName)){
   //jobCardList = from("OrderHeaderAndJobCard").where("orderId",parameters.bookingId).orderBy("orderId").queryList()
   System.out.println("fromDate========================================"+parameters.fromDate);
   System.out.println("thruDate========================================"+parameters.thruDate);
   System.out.println("bookingId========================================"+parameters.bookingId);
   System.out.println("CustomerName========================================"+parameters.CustomerName);
	fromDate = parameters.fromDate
    thruDate = parameters.thruDate
    orderId = parameters.bookingId
    CustomerName = parameters.CustomerName.trim();
    orderHeaderAndJobCardCond = []
    if (orderId) {
        orderHeaderAndJobCardCond.add(EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("orderId")), EntityOperator.EQUALS, orderId.toUpperCase()))
    }
    if (CustomerName) {
        orderHeaderAndJobCardCond.add(EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("firstName")), EntityOperator.LIKE, "%" + CustomerName.toUpperCase() + "%"));//,EntityCondition.makeCondition("lastName", EntityOperator.LIKE, "%" + CustomerName + "%"),EntityOperator.OR])
    }
    if (fromDate) {
	//if (eventDate.length() < 14) eventDate = eventDate + " " + "00:00:00.000"
        orderHeaderAndJobCardCond.add(EntityCondition.makeCondition("orderDate", EntityOperator.GREATER_THAN_EQUAL_TO, Timestamp.valueOf(fromDate)))
    }
    if (thruDate) {
	//if (eventDate.length() < 14) eventDate = eventDate + " " + "00:00:00.000"
        orderHeaderAndJobCardCond.add(EntityCondition.makeCondition("orderDate", EntityOperator.LESS_THAN_EQUAL_TO, Timestamp.valueOf(thruDate)))
    }
orderHeaderAndJobCardCond.add(EntityCondition.makeCondition(EntityFunction.UPPER(EntityFieldValue.makeFieldValue("ownerId")), EntityOperator.EQUALS, userLogin.ownerCompanyId.toUpperCase()))
     System.out.println("CustomerName========================================"+orderHeaderAndJobCardCond);
    jobCardList = from("OrderHeaderAndJobCard").where(orderHeaderAndJobCardCond).queryList()
} else {
	jobCardList = from("OrderHeaderAndJobCard").where(EntityCondition.makeCondition("ownerId", EntityOperator.EQUALS, userLogin.ownerCompanyId)).orderBy("orderId").queryList()
}




context.jobCardList = jobCardList

