package com.gaadizo.data;

import java.util.HashMap;
import java.util.Map;
import java.sql.Timestamp;

public class JobCardBean {

    private Map<String, String> productRegionMap;
    private String resultXML;
    private String result;
    private Map map;
    private String jobCardId;
    private String orderId;
    private String customerId;
    private Timestamp serviceDate;
    private String vehicleId;
    private String vehicleModelId;
    private String vehicleModel;
    private String registationNumber;
    private String status;
    private String firstName;
    private String lastName;
    private String vehicleMakeId;
    private String vehicleMake;
    private String serviceProviderId;
    private String serviceProvider;
    private String serviceAvailed;
    private String oilType;
    private String offerCode;
    private String offerDetails;
    private String paymentMode;
    private String serviceTime;
    private String pickup;
    private String createdBy;
    private String gaadizoCredit;
    
    public Map<String, String> getProductRegionMap() {
        return productRegionMap;
    }
    public void setProductRegionMap(Map<String, String> productRegionMap) {
        this.productRegionMap = productRegionMap;
    }
    public String getResultXML() {
        return resultXML;
    }
    public void setResultXML(String resultXML) {
        this.resultXML = resultXML;
    }
    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }
    public Map getMap() {
        return map;
    }
    public void setMap(Map map) {
        this.map = map;
    }
    public String getJobCardId() {
        return jobCardId;
    }
    public void setJobCardId(String jobCardId) {
        this.jobCardId = jobCardId;
    }
    public String getOrderId() {
        return orderId;
    }
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    public String getCustomerId() {
        return customerId;
    }
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
    public Timestamp getServiceDate() {
        return serviceDate;
    }
    public void setServiceDate(Timestamp serviceDate) {
        this.serviceDate = serviceDate;
    }
    public String getVehicleId() {
        return vehicleId;
    }
    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }
    public String getVehicleModelId() {
        return vehicleModelId;
    }
    public void setVehicleModelId(String vehicleModelId) {
        this.vehicleModelId = vehicleModelId;
    }
    public String getVehicleModel() {
        return vehicleModel;
    }
    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }
    public String getRegistationNumber() {
        return registationNumber;
    }
    public void setRegistationNumber(String registationNumber) {
        this.registationNumber = registationNumber;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getVehicleMakeId() {
        return vehicleMakeId;
    }
    public void setVehicleMakeId(String vehicleMakeId) {
        this.vehicleMakeId = vehicleMakeId;
    }
    public String getVehicleMake() {
        return vehicleMake;
    }
    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }
    public String getServiceProviderId() {
        return serviceProviderId;
    }
    public void setServiceProviderId(String serviceProviderId) {
        this.serviceProviderId = serviceProviderId;
    }
    public String getServiceProvider() {
        return serviceProvider;
    }
    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }
    public String getServiceAvailed() {
        return serviceAvailed;
    }
    public void setServiceAvailed(String serviceAvailed) {
        this.serviceAvailed = serviceAvailed;
    }
    public String getOilType() {
        return oilType;
    }
    public void setOilType(String oilType) {
        this.oilType = oilType;
    }
    public String getOfferCode() {
        return offerCode;
    }
    public void setOfferCode(String offerCode) {
        this.offerCode = offerCode;
    }
    public String getOfferDetails() {
        return offerDetails;
    }
    public void setOfferDetails(String offerDetails) {
        this.offerDetails = offerDetails;
    }
    public String getPaymentMode() {
        return paymentMode;
    }
    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }
    public String getServiceTime() {
        return serviceTime;
    }
    public void setServiceTime(String serviceTime) {
        this.serviceTime = serviceTime;
    }
    public String getPickup() {
        return pickup;
    }
    public void setPickup(String pickup) {
        this.pickup = pickup;
    }
    public String getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    public String getGaadizoCredit() {
        return gaadizoCredit;
    }
    public void setGaadizoCredit(String gaadizoCredit) {
        this.gaadizoCredit = gaadizoCredit;
    }   
}