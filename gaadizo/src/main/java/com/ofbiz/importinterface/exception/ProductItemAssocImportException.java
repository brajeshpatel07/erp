package com.ofbiz.importinterface.exception;

import com.ofbiz.importinterface.exception.BaseException;

public class ProductItemAssocImportException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProductItemAssocImportException(String message){
		super(message);
	}
}
