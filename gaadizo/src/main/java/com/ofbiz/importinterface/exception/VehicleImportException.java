package com.ofbiz.importinterface.exception;

import com.ofbiz.importinterface.exception.BaseException;

public class VehicleImportException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VehicleImportException(String message){
		super(message);
	}
}
