package com.ofbiz.importinterface.exception;

import com.ofbiz.importinterface.exception.BaseException;

public class PVPriceImportException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PVPriceImportException(String message){
		super(message);
	}
}
