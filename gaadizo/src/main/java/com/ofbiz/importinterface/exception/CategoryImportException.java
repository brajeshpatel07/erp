package com.ofbiz.importinterface.exception;

import com.ofbiz.importinterface.exception.BaseException;

public class CategoryImportException extends BaseException {

	/**
	 * Category Import Exception
	 */
	private static final long serialVersionUID = 1L;

	public CategoryImportException(String message){
		super(message);
	}
}
