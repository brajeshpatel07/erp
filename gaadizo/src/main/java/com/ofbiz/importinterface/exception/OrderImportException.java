package com.ofbiz.importinterface.exception;

import com.ofbiz.importinterface.exception.BaseException;

public class OrderImportException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OrderImportException(String message){
		super(message);
	}
}
