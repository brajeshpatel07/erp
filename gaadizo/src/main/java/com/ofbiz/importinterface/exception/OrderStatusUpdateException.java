package com.ofbiz.importinterface.exception;


public class OrderStatusUpdateException extends BaseException {

	public OrderStatusUpdateException(String message) {
		super(message);
	}

}
