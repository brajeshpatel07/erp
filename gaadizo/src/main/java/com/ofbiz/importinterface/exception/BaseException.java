/**
 * 
 */
package com.ofbiz.importinterface.exception;

import java.lang.Exception;

public class BaseException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BaseException(String message) {
		super(message);
	}

	

}
