package com.ofbiz.importinterface.exception;

import com.ofbiz.importinterface.exception.BaseException;

public class ProductImportException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProductImportException(String message){
		super(message);
	}
}
