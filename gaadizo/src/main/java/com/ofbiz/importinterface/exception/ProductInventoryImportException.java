package com.ofbiz.importinterface.exception;

import com.ofbiz.importinterface.exception.BaseException;

public class ProductInventoryImportException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProductInventoryImportException(String message){
		super(message);
	}
}
