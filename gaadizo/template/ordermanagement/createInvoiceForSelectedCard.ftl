<#-- Row -->
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h4 class="txt-dark">
					<form action="<@ofbizUrl>PackOrder</@ofbizUrl>" id="orderId" name="orderId">
						Job Card : 
						<select name="orderId" id="orderId">
							<option>--Select--</option>
							<#if jobCardList?exists && jobCardList?has_content>
								<#list jobCardList as jobCard>
									<option value="${jobCard.primaryOrderId!}" <#if parameters.jobCardId?exists && parameters.jobCardId?has_content && (parameters.jobCardId == jobCard.jobCardId)>selected</#if>>#${jobCard.jobCardId!}</option>
								</#list>
							</#if>
						</select>
						</form>
					</h4>
				</div>
				
	</div>
</div>
</div></div>


<script language="JavaScript" type="text/javascript">
jQuery(document).ready(function() {
jQuery('#orderId').change(function() {
            document.orderId.submit();
        });
    
    
});
</script>