<div class="title2"><span>Booking List</span></div>

<div class="table-responsive margintop20">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabledetails" id="sortableData">
<thead>
  <tr>
    <th>Booking Id</th>
    <th>Customer Name</th>
    <th>Order Amount</th>
    <th>Pick-up</th>
    <th>Service Date</th>
    <th>Vehicle Make</th>
    <th>Vehicle Model</th>
    <th>Registration No.</th>
    <th>Status</th>
    <th>ACTIONS</th>
  </tr>
  </thead>
  <tbody>
												    <#list jobCardList as jobCard>
													<tr>
														<td>
														<a class="buttontext" href="<@ofbizUrl>modifyJobCard?jobCardId=${jobCard.jobCardId!}&isUpdate=Y</@ofbizUrl>" title="Code">
     <span class="bookid">${jobCard.orderId!}</span> </a>
														
														</td>
														<td>${jobCard.firstName!} ${jobCard.lastName!}</td>
														<td><@ofbizCurrency amount=jobCard.grandTotal isoCode="INR"/></td>
														<td>${jobCard.pickup!}</td>
														<td>${jobCard.serviceDate!}</td>
														<td>${jobCard.vehicleId!}</td>
														<td>
														${jobCard.vehicleId!}	
														</td>
														<td>${jobCard.registationNumber!}</td>
														<td><span class="label label-success">${jobCard.status!}</span></td>
														<td>
														<a class="buttontext" href="<@ofbizUrl>modifyJobCard?jobCardId=${jobCard.jobCardId!}&isUpdate=Y</@ofbizUrl>" title="Code"><i class="fa fa-eye"></i></a>
														
														<a  target="_blank" href="<@ofbizUrl>jobCardPDF?jobCardId=${jobCard.jobCardId!parameters.jobCardId!}&orderId=${jobCard.orderId!}</@ofbizUrl>" title="Print Job Card" ><img src="\erpTheme\images\exportToPdfIcon.gif"/></a>
														
														<a href="javascript:document.selectItemForCardForm${jobCard_index}.submit();"><img src="\erpTheme\images\exportToPdfIcon.gif"/>New</a>
	    												<form action="<@ofbizUrl>clearcartFromJobCard</@ofbizUrl>" name="selectItemForCardForm${jobCard_index}" id="selectItemForCardForm${jobCard_index}" method="post">
            												<input type="hidden" name="customerId" value="${jobCard.customerId!}" />
        												</form>
														</td>
													</tr>
													</#list>
													
												</tbody>
</table>
</div>
