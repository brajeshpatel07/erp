<div class="title2"><span>Spare Part Issue</span></div>
<div class="row jobcard">
<div class="col-sm-2"><span>JOB CARD:</span></div> <div class="col-sm-4">
	
	<form action="<@ofbizUrl secure="${request.isSecure()?string}">sparePartIssueDetail</@ofbizUrl>" id="selectJobCardId" name="selectJobCardId">
						
						<select class="form-control" name="jobCardId" id="jobCardId">
							<option>--Select--</option>
							<#if jobCardList?exists && jobCardList?has_content>
								<#list jobCardList as jobCard>
									<option value="${jobCard.jobCardId!}" <#if parameters.jobCardId?exists && parameters.jobCardId?has_content && (parameters.jobCardId == jobCard.jobCardId)>selected</#if>>#${jobCard.jobCardId!} - ${jobCard.vehicleId!}</option>
								</#list>
							</#if>
						</select>
						</form>
</div>

</div>

<div class="row">
<div class="col-sm-9 table-responsive">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="sparetable">
  <tr>
    <td>Customer Name</td>
    <td>${(orderHeaderAndjobCardGV.firstName)!}</td>
    <td>Vehicle Manufacturer</td>
    <td>${(orderHeaderAndjobCardGV.vehicleManufacturer)!}</td>
  </tr>
  <tr>
    <td>Mobile Number</td>
    <td>${(orderHeaderAndjobCardGV.advisor)!}90969070666</td>
    <td>Vehicle Model</td>
    <td>${(orderHeaderAndjobCardGV.vehicleModel)!}</td>
  </tr>
  <tr>
    <td>Advisor</td>
    <td>${(orderHeaderAndjobCardGV.advisor)!}</td>
    <td>Services Availed</td>
    <td>${(orderHeaderAndjobCardGV.serviceAvailed)!}</td>
  </tr>
</table> 
</div>
</div> 

<div class="title2 margintop20"><span>Spare Part List</span></div>
<div class="row listing">
<div class="col-sm-12 text-right">
<form action="<@ofbizUrl>selectItemForCard</@ofbizUrl>" name="selectItemForCardForm" id="selectItemForCardForm" method="post">
                                    	<input type="hidden" name="jobCardId" value="${orderHeaderAndjobCardGV.jobCardId!}" />
                                    	<input type="hidden" name="orderId" value="${orderHeaderAndjobCardGV.orderId!}" />
                                    	<input type="hidden" name="vehicleId" value="${orderHeaderAndjobCardGV.vehicleId!}" />
                                    </form>
                                    <div  class="pull-right"><a class="button-blue" href="javascript:document.selectItemForCardForm.submit();">Add spare Part</a></div>
<#-- form action="<@ofbizUrl>selectItemForCard</@ofbizUrl>" name="selectItemForCardForm" id="selectItemForCardForm" method="post">
                                    	<input type="hidden" name="selectedJobCardId" value="${(orderHeaderAndjobCardGV.jobCardId)!}" />
                                    	<input type="hidden" name="selectedOrderId" value="${(orderHeaderAndjobCardGV.orderId)!}" />
                                    	<input type="hidden" name="selectedVehicleId" value="${(orderHeaderAndjobCardGV.vehicleId)!}" />
                                    </form>
<a class="button-blue" href="javascript:document.selectItemForCardForm.submit();"><i class="fa fa-plus"></i> Add Service</a -->

</div>
</div>

<div class="table-responsive margintop20">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabledetails">
  <tr>
    <th>Spare Part Id</th>
    <th>Spare Part Description</th>
    <th>Brand</th>
    <th>UOM</th>
    <th>Qty to issue</th>
    <th>Available Qty</th>
    <th>Bin Code</th>
    <th>Price</th>
    <th>ACTIONS</th>
  </tr>
  <#if jobCardDetails?has_content>
												<#list jobCardDetails as jobCardDetail>
													
												<tr>
												  <td class="title">${jobCardDetail.productId!}</td>
												  <td><div id="prefetch">${jobCardDetail.itemDescription!}</div></td>
												  <td>Brand Name</td>
												  <td>Ltr.</td>
												  <td>${jobCardDetail.quantity!}</td>
												  <td>${productInventoryMap.get(jobCardDetail.productId)!}</td>
												  <td>${productInventoryMap.get(jobCardDetail.productId+"binLoc")!}</td>
												  <td>${jobCardDetail.unitPrice!}</td>
												  <td><a href="javascript:document.updateItemInfo.action='<@ofbizUrl>cancelOrderItemFromSpareIssue</@ofbizUrl>';document.updateItemInfo.orderItemSeqId.value='${jobCardDetail.orderItemSeqId}';document.updateItemInfo.shipGroupSeqId.value='00001';document.updateItemInfo.submit()" class="buttontext">${uiLabelMap.CommonCancel}</a></td>
												</tr>
												</#list>
												<#else>
												   <tr>
												  <td class="title">No Job card items.</td>
												  </tr>
												</#if>
  <#-- tr>
    <td>XXDOF-07</td>
    <td>Estilo Service Labour Charge</td>
    <td>Brand Name</td>
    <td>LTR</td>
    <td>2</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>1000</td>
    <td><a href="#"><i class="fa fa-plus"></i></a> <a href="#"><i class="fa fa-minus"></i></a></td>
  </tr>
  <tr>
    <td>XXDOF-07</td>
    <td>Estilo Service Labour Charge</td>
    <td>Brand Name</td>
    <td>LTR</td>
    <td>2</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>1000</td>
    <td><a href="#"><i class="fa fa-plus"></i></a> <a href="#"><i class="fa fa-minus"></i></a></td>
  </tr>
  <tr>
    <td>XXDOF-07</td>
    <td>Estilo Service Labour Charge</td>
    <td>Brand Name</td>
    <td>LTR</td>
    <td>2</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>1000</td>
    <td><a href="#"><i class="fa fa-plus"></i></a> <a href="#"><i class="fa fa-minus"></i></a></td>
  </tr>
  <tr>
    <td>XXDOF-07</td>
    <td>Estilo Service Labour Charge</td>
    <td>Brand Name</td>
    <td>LTR</td>
    <td>2</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>1000</td>
    <td><a href="#"><i class="fa fa-plus"></i></a> <a href="#"><i class="fa fa-minus"></i></a></td>
  </tr>
  <tr>
    <td>XXDOF-07</td>
    <td>Estilo Service Labour Charge</td>
    <td>Brand Name</td>
    <td>LTR</td>
    <td>2</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>1000</td>
    <td><a href="#"><i class="fa fa-plus"></i></a> <a href="#"><i class="fa fa-minus"></i></a></td>
  </tr -->
</table>
</div>

<script language="JavaScript" type="text/javascript">
jQuery(document).ready(function() {
jQuery('#jobCardId').change(function() {
            jQuery('#jobCardDetailDate').show();
            jQuery('#jobCardDetail').show();
            document.selectJobCardId.submit();
        });
    jQuery('#addServiceButton').click(function(){
        
        jQuery("#addItemInJobCard").toggle();
    });
    
    
});
function deleteItemFromJobcart(element){
alert(element);
}
</script>