            <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                    <!-- Title -->
                    <div class="row heading-bg">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h5 class="txt-dark">Job Card Creation</h5>
                        </div>
                    
                        <!-- Breadcrumb -->
                        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                            <ol class="breadcrumb">
                                <li><a href="<@ofbizUrl>main</@ofbizUrl>">Dashboard</a></li>
                                <#-- li><a href="#"><span>form</span></a></li>
                                <li class="active"><span>form-layout</span></li -->
                            </ol>
                        </div>
                        <!-- /Breadcrumb -->
                    
                    </div>
                    <!-- /Title -->
                    
                    <!-- Row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default card-view">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h4 class="txt-dark">Job Card Number : <code>#${(orderHeaderAndjobCardGV.jobCardId)!}</code></h4>
                                    </div>
                                    
                                    <form action="<@ofbizUrl>selectItemForCard</@ofbizUrl>" name="selectItemForCardForm" id="selectItemForCardForm" method="post">
                                    	<input type="hidden" name="jobCardId" value="${(orderHeaderAndjobCardGV.jobCardId)!}" />
                                    	<input type="hidden" name="orderId" value="${(orderHeaderAndjobCardGV.orderId)!}" />
                                    	<input type="hidden" name="vehicleId" value="${(orderHeaderAndjobCardGV.vehicleId)!}" />
                                    </form>
                                    <div  class="pull-right"><a class="button-blue" href="javascript:document.selectItemForCardForm.submit();">Add spare Part</a></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="panel-group accordion-struct accordion-style-1" id="accordion_2" role="tablist" aria-multiselectable="true">
                                            <div class="panel panel-default">
                                                <div class="panel-heading activestate" role="tab" id="heading_10">
                                                    <h4 class="txt-dark capitalize-font"><a role="button" data-toggle="collapse" data-parent="#accordion_2" href="#collapse_10" aria-expanded="true" ><div class="icon-ac-wrap pr-20"><span class="plus-ac"><i class="ti-plus"></i></span><span class="minus-ac"><i class="ti-minus"></i></span></div>Customer Details</a></h4>
                                                </div>
                                                <div id="collapse_10" class="panel-collapse collapse in" role="tabpanel">
                                                    <div class="panel-body pa-15">
                                                        <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Customer Name</label>
                                                                        <input type="text" readonly="readonly" id="CustomerName" class="form-control" placeholder="Enter Name" value="<#if orderHeaderAndjobCardGV?has_content || personGV?has_content>${(orderHeaderAndjobCardGV.firstName)!(personGV.firstName)!""} ${orderHeaderAndjobCardGV.lastName!}</#if>">
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Email</label>
                                                                        <input type="email" readonly="readonly" id="Email" class="form-control" placeholder="Enter Email" value="${partyEmail!}">
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                            </div>
                                                            
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Mobile Number</label>
                                                                        <input type="tel" readonly="readonly" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if partyAddress?has_content>${(partyAddress.contactNumber!)!}</#if>">
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Address</label>
                                                                        <#--<input type="text" id="Address" class="form-control" placeholder="Enter Address" <#if partyAndPostalAddressGV.address1?has_content>value="${partyAndPostalAddressGV.address1!}"</#if>>-->
                                                                        <input type="text" readonly="readonly" id="Address" class="form-control" placeholder="Enter Address" <#if partyAddress?has_content>value="${partyAddress.address1!} ${partyAddress.address2!}"</#if>> 
                                                                    </div>
                                                                </div>
                                                                <!-- /span -->
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">City</label>
                                                                        <input type="tel" readonly="readonly" id="MobileNumber" class="form-control" value="<#if partyAddress?has_content>${(partyAddress.city!)!}</#if>">
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Postal Code</label>
                                                                        <input type="text" readonly="readonly" id="Address" class="form-control" placeholder="Enter Address" <#if partyAddress?has_content>value="${(partyAddress.postalCode!)!} ${partyAddress.address2!}"</#if>> 
                                                                    </div>
                                                                </div>
                                                                <!-- /span -->
                                                            </div>
                                                            </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading_11">
                                                    <h4 class="txt-dark capitalize-font"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_2" href="#collapse_11" aria-expanded="false"  ><div class="icon-ac-wrap pr-20"><span class="plus-ac"><i class="ti-plus"></i></span><span class="minus-ac"><i class="ti-minus"></i></span></div>Vehicle Details </a></h4>
                                                </div>
                                                <div id="collapse_11" class="panel-collapse collapse" role="tabpanel">
                                                <div class="panel-body pa-15"> 
                                                            <div class="row">
                                                                <div class="col-md-4 ">
                                                                    <div class="form-group">
                                                                    <label class="control-label mb-10">Vehicle Manufacturer</label>
                                                                    <#if vehicleGV?exists && vehicleGV?has_content && vehicleGV.manufacturerName?has_content><span>${vehicleGV.manufacturerName!}</span></#if>
                                                                    <#-- <select class="form-control">
                                                                        
                                                                             <#if vehicleGV?exists && vehicleGV?has_content && vehicleGV.vehicleId?has_content>
                                                                                 <option value="${vehicleGV.manufacturerId!}">${vehicleGV.manufacturerName!}</option>
                                                                             <#else>
                                                                                <#if vehicleMNameList?exists && vehicleMNameList?has_content>
                                                                            		 <#list vehicleMNameList as vehicleF>
                                                                            			<option value="${vehicleF!}">${vehicleF!}</option>
                                                                            		 </#list>
                                                                                </#if>
                                                                              </#if>
                                                                          
                                                                        </select -->
                                                                    </div>    
                                                                </div>
                                                                <div class="col-md-4 ">
                                                                    <div class="form-group">
                                                                    <label class="control-label mb-10">Vehicle Model</label>
                                                                    <#if vehicleGV?exists && vehicleGV?has_content && vehicleGV.vehicleId?has_content><span>${vehicleGV.vehicleId!}</span></#if>
                                                                    <#--    <select class="form-control">
                                                                        
                                                                        <#if vehicleGV?exists && vehicleGV?has_content && vehicleGV.vehicleId?has_content>
                                                                             <option value="${vehicleGV.modelId!}">${vehicleGV.modelName!}</option>
                                                                         <#else>
                                                                            <option>--Select Vehicle Model--</option>
                                                                            <#if vehicleModelNameList?exists && vehicleModelNameList?has_content>
	                                                                            <#list vehicleModelNameList as vehicleModelName>
	                                                                                <option value="${vehicleModelName!}">${vehicleModelName!}</option>
	                                                                            </#list>
                                                                            </#if>
                                                                            </#if>
                                                                        </select -->
                                                                    </div>    
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Variant</label>
                                                                          <#if vehicleGV?exists && vehicleGV?has_content && vehicleGV.variant?has_content><span>${vehicleGV.variant!}</span></#if>
                                                                        <#-- <select class="form-control">
                                                                        <#if vehicleDetail?exists && vehicleDetail?has_content && vehicleDetail.variant?has_content>
                                                                             <option value="${vehicleDetail.variant!}">${vehicleDetail.variant!}</option>
                                                                         <#else>
                                                                            <#if vehicleVariantList?exists && vehicleVariantList?has_content> 
                                                                            <#list vehicleVariantList as vehicleVariant>
                                                                                <option value="${vehicleVariant!}">${vehicleVariant!}</option>
                                                                            </#list>
                                                                            </#if>
                                                                            </#if>
                                                                        </select -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Vehicle Reg. No.</label>
                                                                         <#if orderHeaderAndjobCardGV?exists && orderHeaderAndjobCardGV?has_content && orderHeaderAndjobCardGV.registationNumber?has_content><span>${orderHeaderAndjobCardGV.registationNumber!}</span></#if>
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Manufacturing Year</label>
                                                                        <#if vehicleDetail?exists && vehicleDetail?has_content && vehicleDetail.year?has_content><span>${vehicleDetail.year!}</span></#if>
                                                                        <#-- <select class="form-control">
                                                                        <#if vehicleDetail?exists && vehicleDetail?has_content && vehicleDetail.year?has_content>
                                                                             <option value="${vehicleDetail.year!}">${vehicleDetail.year!}</option>
                                                                         <#else>
                                                                            <option>--Select Year--</option>
                                                                             <#if vehicleYList?exists && vehicleYList?has_content>
                                                                             <#list vehicleYList as vehicleY>
                                                                                <option value="${vehicleY!}">${vehicleY!}</option>
                                                                            </#list>
                                                                            </#if>
                                                                            </#if>
                                                                        </select> -->
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Anniv. Date</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <!-- /Row -->
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Last Service Date</label>
                                                                         <#if orderHeaderAndjobCardGV?exists && orderHeaderAndjobCardGV?has_content && orderHeaderAndjobCardGV.lastServiceDate?has_content><span>${orderHeaderAndjobCardGV.lastServiceDate!}</span></#if>
                                                                        <#-- input type="text" id="LastServiceDate" class="form-control" data-mask="99/99/9999" placeholder="DD/MM/YYYY" -->
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Service Date</label>
                                                                        <#if orderHeaderAndjobCardGV?exists && orderHeaderAndjobCardGV?has_content && orderHeaderAndjobCardGV.serviceDate?has_content><span>${orderHeaderAndjobCardGV.serviceDate!}</span></#if>
                                                                        <#-- <input type="text" id="ServiceDate" class="form-control" data-mask="99/99/9999" placeholder="DD/MM/YYYY"> -->
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Insurance Company</label>
                                                                        <#if orderHeaderAndjobCardGV?exists && orderHeaderAndjobCardGV?has_content && orderHeaderAndjobCardGV.vehicleInsuranceComp?has_content><span>${orderHeaderAndjobCardGV.vehicleInsuranceComp!}</span></#if>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- /Row -->
                                                            <div class="row">
                                                                
                                                                <!--/span-->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Insurance Date</label>
                                                                        <#if orderHeaderAndjobCardGV?exists && orderHeaderAndjobCardGV?has_content && orderHeaderAndjobCardGV.insuranceExpiryDate?has_content><span>${orderHeaderAndjobCardGV.insuranceExpiryDate!}</span></#if>
                                                                        
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Payment mode</label>
                                                                        <#if orderHeaderAndjobCardGV?exists && orderHeaderAndjobCardGV?has_content && orderHeaderAndjobCardGV.paymentMode?has_content><span>${orderHeaderAndjobCardGV.paymentMode!}</span></#if>
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Customer Type</label>
                                                                        <#if orderHeaderAndjobCardGV?exists && orderHeaderAndjobCardGV?has_content && orderHeaderAndjobCardGV.customerType?has_content><span>${orderHeaderAndjobCardGV.customerType!}</span></#if>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                </div>
                                            </div>
                                            <#-- add_new_service_div starts -->
                                        </div> 
                                        <!-- add_new_service_div ends-->
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                    </div>
                    </div>
                    <!-- /Row -->
            <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark">Service Detail</h6>
                                </div>
                                <div class="pull-right">
                                    <button class="btn btn-primary btn-rounded btn-icon left-icon btn-xs" id="addServiceButton"> <i class="fa fa-plus"></i> <span>Add Service</span></button>    
                                </div>
                                <div id="addItemInJobCard" style="display:none;">
                                   ${screens.render("component://gaadizo/widget/GaadizoScreens.xml#JobCardItemEdit")}
                                </div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table id="datable_1" class="table  display table-hover mb-30">
                                                <thead>
                                                    <tr>
                                                        <th>Service Item Id</th>
                                                        <th>Item Description</th>
                                                        <th>Service Cost</th>
                                                        <th>Discount %</th>
                                                        <th>Tax</th>
                                                        <th>Total Amount</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                <#assign allitemSubTotal = 0>
                                                <#assign allitemTaxTotal = 0>
                                                <#assign allitemDiscountTotal = 0>
                                                <#assign allitemTotal = 0>
                                                 <#if jobCardDetails?exists && jobCardDetails?has_content>
                                                <#list jobCardDetails as jobCardDetail>
                                                    
                                                    <tr>
                                                    <#assign itemTotalOnLine = 0>
                                                        <td>
                                                        
                                                        <#-- <a class="buttontext" href="<@ofbizUrl>GlAccountNavigate?productId=${jobCardDetail.orderItemSeqId!}</@ofbizUrl>" title="Code"> </a> -->
                                                          ${jobCardDetail.productId!}
                                                        
                                                        </td>
                                                        <td>${jobCardDetail.itemDescription!}</td>
                                                        <td>
                                                            <#if jobCardDetail.unitPrice?has_content>
                                                                <#assign itemTotalOnLine = jobCardDetail.unitPrice.multiply(jobCardDetail.quantity) />
                                                            <#else>
                                                                <#assign itemTotalOnLine = 0 />
                                                            </#if>
                                                            <#assign allitemSubTotal = allitemSubTotal + itemTotalOnLine >
                                                            <span class="label label-danger"><@ofbizCurrency amount=itemTotalOnLine isoCode="INR"/></span>
                                                        </td>
                                                        <td>
                                                        <#assign totaldiscountOnLine = 0>
                                                        <#list orderAdjustmentList as orderAdjustment>
                                                            <#if "PROMOTION_ADJUSTMENT" == orderAdjustment.orderAdjustmentTypeId! && orderAdjustment.orderItemSeqId == jobCardDetail.orderItemSeqId>
                                                                <#assign totaldiscountOnLine = totaldiscountOnLine + orderAdjustment.amount>
                                                                <#assign allitemDiscountTotal = allitemDiscountTotal + orderAdjustment.amount>
                                                            </#if>
                                                        </#list>
                                                        <#assign itemTotalOnLine = itemTotalOnLine + totaldiscountOnLine>
                                                        <span class="label label-danger"><@ofbizCurrency amount=totaldiscountOnLine isoCode="INR"/>
                                                        </td>
                                                        <td>
                                                        <#assign totalTaxOnLine = 0>
                                                        <#list orderAdjustmentList as orderAdjustment>
                                                            <#if "SALES_TAX" == orderAdjustment.orderAdjustmentTypeId! && orderAdjustment.orderItemSeqId == jobCardDetail.orderItemSeqId>
                                                                <#assign totalTaxOnLine = totalTaxOnLine + orderAdjustment.amount>
                                                                <#assign allitemTaxTotal = allitemTaxTotal + orderAdjustment.amount>
                                                            </#if>
                                                        </#list>
                                                        <#assign itemTotalOnLine = itemTotalOnLine + totalTaxOnLine>
                                                        <#assign allitemTotal = itemTotalOnLine + allitemTotal>
                                                        <span class="label label-danger"><@ofbizCurrency amount=totalTaxOnLine isoCode="INR"/></span>
                                                        </td>
                                                        <td>
                                                            <@ofbizCurrency amount=itemTotalOnLine isoCode="INR"/>    
                                                        </td>
                                                        <td>
                                                        <form name="updateItemInfo_00001_${jobCardDetail.orderItemSeqId!}" method="post" action="<@ofbizUrl>cancelOrderItem</@ofbizUrl>">
                                                            <input type="hidden" name="orderId" value="${orderHeaderAndjobCardGV.orderId!}"/>
                                                            <input type="hidden" name="jobCardId" value="${orderHeaderAndjobCardGV.jobCardId!}"/>
                                                            <input type="hidden" name="orderItemSeqId" value="${jobCardDetail.orderItemSeqId!}"/>
                                                            <input type="hidden" name="shipGroupSeqId" value="00001"/>
                                                             <a href="javascript:document.updateItemInfo_00001_${jobCardDetail.orderItemSeqId!}.submit();" class="btn btn-primary btn-icon-anim btn-circle fa fa-minus">-</a>
                                                        </form>
                                                        
                                                        </td>
                                                        
                                                    </tr>
                                                    
                                                </#list>
                                                 </#if>
                                                    
                                                </tbody>
                                                <tfoot>
                                                <tr class="bg-grey">
                                                  <td class="title"></td>
                                                  <td>Total</td>
                                                  <td><@ofbizCurrency amount=allitemSubTotal isoCode="INR"/></td>
                                                  <td><@ofbizCurrency amount=allitemDiscountTotal isoCode="INR"/></td> 
                                                  <td><@ofbizCurrency amount=allitemTaxTotal isoCode="INR"/></td>
                                                  <td><@ofbizCurrency amount=allitemTotal isoCode="INR"/></td>
                                                  <td></td>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>    
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                                </div>
                <!-- /Row -->
                <!-- /Row -->
                <div class="form-actions mt-10 fright">
                    <button type="button" class="btn btn-default">Cancel</button> 
                    <#if orderHeaderAndjobCardGV?exists && orderHeaderAndjobCardGV.orderId?has_content>
                        <button class="btn btn-success btn-lable-wrap left-label"> <span class="btn-label"><i class="fa fa-check"></i> </span><span class="btn-text">Update Job Card</span></button>
                    <#else>
                    <button class="btn btn-success btn-lable-wrap left-label"> <span class="btn-label"><i class="fa fa-check"></i> </span><span class="btn-text">Create Job Card</span></button>
                </#if>          
                </div>

<script language="JavaScript" type="text/javascript">
jQuery(document).ready(function() {
    jQuery('#addServiceButton').click(function(){
        
        jQuery("#addItemInJobCard").toggle();
    });
});
</script>