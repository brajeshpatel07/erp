<div class="title2"><span>Spare Part Addition</span></div>

<div class="paddingbott20">
<div id="accordion1" class="panel-group accordion">
 <div class="panel">
    <div class="panel-title"><a data-parent="#accordion1" data-toggle="collapse" href="#accordion11" class="" aria-expanded="true"><h3> Gernal</h3></a></div>
    <div id="accordion11" class="panel-collapse collapse in" role="tablist" aria-expanded="true">
      <div class="panel-content">
      <form action="" method="get">
		<div class="row form-group">
			<div class="col-sm-4"><label>Spare Part No.</label><input type="text" id="productId" name="productId" class="form-control" placeholder="Enter Spare Part No."></div>
			<div class="col-sm-4"><label>Category</label><select name="category" class="form-control"><option>Select Category</option></select></div>
			<div class="col-sm-4"><label>Spare Part Name</label><input type="text" name="productName" class="form-control" placeholder="Enter Spare Part Name"></div>
			<div class="col-sm-4"><label>Product Type</label><select name="productTypeId" class="form-control"><option>Select Product Type</option></select></div>
			<div class="col-sm-4"><label>Brand</label><select class="form-control" name="brandName"><option>Select Brand Type</option></select></div>
			<div class="col-sm-4"><label>MRP</label><input type="text" name="price" class="form-control" placeholder="Enter MRP"></div>
			<div class="col-sm-4"><label>Vehicle Model</label><select name="vehicleModelId" class="form-control"><option>Select Vehicle Model</option></select></div>
			<div class="col-sm-4"><label>Vehicle Variant</label><select name="vehicleVariant"  class="form-control"><option>Select Vehicle Variant</option></select></div>
		</div>

<div class="paddingbott30"><a class="button-blue" href="#"><i class="fa fa-plus"></i>  Upload Image</a></div>
</form>
      </div>
    </div>
  </div>
  
  <div class="panel">
    <div class="panel-title"><a class="collapsed" data-parent="#accordion1" data-toggle="collapse" href="#accordion12" aria-expanded="false"><h3> INVENTORY</h3></a></div>
    <div id="accordion12" class="panel-collapse collapse" role="tablist" aria-expanded="false">
      <div class="panel-content">
        <form action="" method="get">
<div class="row">
    <div class="col-md-5ths col-xs-6"><label>QOH</label><input type="text" name="" class="form-control" placeholder="40"></div>
    <div class="col-md-5ths col-xs-6"><label>ATP</label><input type="text" name="" class="form-control" placeholder="25"></div>
    <div class="col-md-5ths col-xs-6"><label>Minimum Qty</label><input type="text" name="" class="form-control" placeholder="30"></div>
    <div class="col-md-5ths col-xs-6"><label>Inventory Group</label><input type="text" name="" class="form-control" placeholder="Spare Parts"></div>
    <div class="col-md-5ths col-xs-6"><label>Storage Type</label><input type="text" name="" class="form-control" placeholder="Rack"></div>
    <div class="col-md-5ths col-xs-6"><label>Inventory Units</label><input type="text" name="" class="form-control" placeholder="PCS"></div>
    <div class="col-md-5ths col-xs-6"><label>Pack Size</label><input type="text" name="" class="form-control" placeholder="1"></div>
    <div class="col-md-5ths col-xs-6"><label>Packing Type</label><input type="text" name="" class="form-control" placeholder="EACHES"></div>
    <div class="col-md-5ths col-xs-6"><label>LBH</label><input type="text" name="" class="form-control" placeholder="4x5x6"></div>
    <div class="col-md-5ths col-xs-6"><label>Weight</label><input type="text" name="" class="form-control" placeholder="300 GMS"></div>
</div>

</form>
      </div>
    </div>
  </div>
  
  
<div class="panel">
   <div class="panel-title"><a class="collapsed" data-parent="#accordion1" data-toggle="collapse" href="#accordion13" aria-expanded="false"><h3> PURCHASE</h3></a></div>
    <div id="accordion13" class="panel-collapse collapse" role="tablist" aria-expanded="false">
          <div class="panel-content">
        <form action="" method="get">
<div class="row form-group">
    <div class="col-sm-4"><label>Spare Part No.</label><input type="text" name="" class="form-control" placeholder="Enter Spare Part No."></div>
    <div class="col-sm-4"><label>Category</label><select class="form-control"><option>Select Category</option></select></div>
    <div class="col-sm-4"><label>Spare Part Name</label><input type="text" name="" class="form-control" placeholder="Enter Spare Part Name"></div>
    <div class="col-sm-4"><label>Product Type</label><select class="form-control"><option>Select Product Type</option></select></div>
    <div class="col-sm-4"><label>Brand</label><select class="form-control"><option>Select Brand Type</option></select></div>
    <div class="col-sm-4"><label>MRP</label><input type="text" name="" class="form-control" placeholder="Enter MRP"></div>
    <div class="col-sm-4"><label>Vehicle Model</label><select class="form-control"><option>Select Vehicle Model</option></select></div>
    <div class="col-sm-4"><label>Vehicle Variant</label><select class="form-control"><option>Select Vehicle Variant</option></select></div>
</div>

</form>
      </div>
    </div>
  </div>
  
  
<div class="panel">
    <div class="panel-title"><a class="collapsed" data-parent="#accordion1" data-toggle="collapse" href="#accordion14" aria-expanded="false"><h3> SALES</h3></a></div>
     <div id="accordion14" class="panel-collapse collapse" role="tablist" aria-expanded="false">
     
      <div class="panel-content">
        <form action="" method="get">
<div class="row form-group">
    <div class="col-sm-4"><label>Spare Part No.</label><input type="text" name="" class="form-control" placeholder="Enter Spare Part No."></div>
    <div class="col-sm-4"><label>Category</label><select class="form-control"><option>Select Category</option></select></div>
    <div class="col-sm-4"><label>Spare Part Name</label><input type="text" name="" class="form-control" placeholder="Enter Spare Part Name"></div>
    <div class="col-sm-4"><label>Product Type</label><select class="form-control"><option>Select Product Type</option></select></div>
    <div class="col-sm-4"><label>Brand</label><select class="form-control"><option>Select Brand Type</option></select></div>
    <div class="col-sm-4"><label>MRP</label><input type="text" name="" class="form-control" placeholder="Enter MRP"></div>
    <div class="col-sm-4"><label>Vehicle Model</label><select class="form-control"><option>Select Vehicle Model</option></select></div>
    <div class="col-sm-4"><label>Vehicle Variant</label><select class="form-control"><option>Select Vehicle Variant</option></select></div>
</div>

</form>
      </div>
    </div>
  </div>
  
  
</div>



</div>

 

<div class="button-tab leftalign"><button type="submit" name="" class="btn create">ADD SPARE PART</button><button type="submit" name="" class="btn create">UPDATE SPARE PART</button></div>

</div>
</div>


</div>
</section>