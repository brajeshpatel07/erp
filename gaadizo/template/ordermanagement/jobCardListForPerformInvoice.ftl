<#-- Row -->
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h4 class="txt-dark">
					<form action="<@ofbizUrl>createPerformInvoice</@ofbizUrl>" id="selectJobCardId" name="selectJobCardId">
						Job Card : 
						<select name="orderId" id="orderId">
							<option>--Select--</option>
							<#if jobCardList?exists && jobCardList?has_content>
								<#list jobCardList as jobCard>
									<option value="${jobCard.orderId!}" <#if parameters.jobCardId?exists && parameters.jobCardId?has_content && (parameters.jobCardId == jobCard.jobCardId)>selected</#if>>#${jobCard.jobCardId!} - ${jobCard.vehicleId!}</option>
								</#list>
							</#if>
						</select>
						</form>
					</h4>
				</div>
				<#-- div <#if !parameters.jobCardId?has_content>style="display:none"</#if> id="jobCardDetailDate">
					<div class="pull-right">
						<h4 class="txt-dark">Service Date : <code>13/03/2018</code></h4>
					</div>
					<div class="clearfix"></div>
				</div -->
			</div>
			<#-- div <#if !parameters.jobCardId?has_content>style="display:none"</#if> id="jobCardDetail">
				<div class="row">
					<div class="col-md-6">
						<table style="clear: both" class="table table-bordered table-striped" id="user_2">
							<tbody>
								<tr>
									<td style="width:35%">Customer Name</td>
									
									 
									<td style="width:65%"><a href="#" id="CustomerName" data-type="text" data-pk="1" data-title="Customer Name">${(orderHeaderAndjobCardGV.customerName)!}</a></td>
								</tr>
								<tr>
									<td>Mobile Number</td>
									<td><a href="#" id="MobileNumber" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Mobile Number"></a></td>
								</tr>
								<tr>
									<td>Advisor</td>
									<td><a href="#" id="Advisor" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Advisor Name">Advisor Name</a></td>
								</tr>
							</tbody>
						</table>	
					</div>
					<div class="col-md-6">
						<table style="clear: both" class="table table-bordered table-striped" id="user_2">
							<tbody>
								<tr>
									<td style="width:35%">Vehicle Manufacturer</td>
									<td style="width:65%">${(vehicleDetail.manufacturerName)!}</td>
								</tr>
								<tr>
									<td>Vehicle Model</td>
									<td>${(vehicleDetail.modelId)!}</td>
								</tr>
								<tr>
									<td>Services Availed</td>
									<td><a href="#">Car Wash</a>, <a href="#">Extensive Service</a></td>
								</tr>
								</tbody>
							</table>
						</div>
						<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">Spare Part List</h6>
								</div>
								<div class="pull-right">
									<button class="btn btn-primary btn-rounded btn-icon left-icon btn-xs"> <i class="fa fa-plus"></i> <span>Add Spare Part</span></button>								</div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div>
											<table class="tablesaw table-hover table" data-tablesaw-mode="columntoggle">
												<thead>
													<tr>
													  <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Spare Part ID</th>
													  <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Spare Part Description</th>
													  <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Brand</th>
													  <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">UOM</th>
													  <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Qty to Issue</th>
													  <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Available Qty</th>
													  <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Bin Code</th>
													  <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Price</th>
													  <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Actions</th>
													</tr>
												</thead>
												<tbody>
													
												<#if jobCardDetails?has_content>
												<#list jobCardDetails as jobCardDetail>
													
												<tr>
												  <td class="title">${jobCardDetail.productId!}</td>
												  <td><div id="prefetch">${jobCardDetail.itemDescription!}</div></td>
												  <td>Brand Name</td>
												  <td>Ltr.</td>
												  <td>${jobCardDetail.quantity!}</td>
												  <td>${productInventoryMap.get(jobCardDetail.productId)!}</td>
												  <td>${productInventoryMap.get(jobCardDetail.productId+"binLoc")!}</td>
												  <td>${jobCardDetail.unitPrice!}</td>
												  <td><a href="#" data-tooltip="Remove" data-position="top" class="top icon-btn"><i class="fa fa-minus-square"></i></a></td>
												</tr>
												</#list>
												<#else>
												   <tr>
												  <td class="title">No Job card items.</td>
												  </tr>
												</#if>
												
												</tbody>
												<tfoot>
												<tr class="bg-grey">
												  <td></td>
												  <td>Total</td>
												  <td></td>
												  <td></td>
												  <td></td>
												  <td></td>
												  <td></td>
												  <td>&#x20B9; 2700</td>
												  <td></td>
												</tr>
												</tfoot>
											</table>
											
											<div class="form-actions mt-10 fright">
												<button type="button" class="btn btn-default">Cancel</button> 
												<button class="btn btn-success btn-lable-wrap left-label"> <span class="btn-label"><i class="fa fa-check"></i> </span><span class="btn-text">Issue Spare Parts</span></button>	
											</div>
										</div>	
									</div>
								</div>
							</div>
						<!--/span-->
					</div>
                </div>
			</div>
		</div>
	</div -->
</div>
</div>
</div>

		
<script>
jQuery(document).ready(function(){
	 jQuery('#orderId').change(function() {
	        jQuery('#jobCardDetailDate').show();
	        jQuery('#jobCardDetail').show();
	        document.selectJobCardId.submit();
	    });
});
</script>