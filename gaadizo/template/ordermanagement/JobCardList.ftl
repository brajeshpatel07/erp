<div class="title2"><span>Booking List</span></div>
<div class="table-responsive margintop20">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabledetails" id="sortableData">
<thead>
  <tr>
    <th>JobCard Id</th>
    <th>Customer Name</th>
    <th>Order Amount</th>
    <th>Pick-up</th>
    <th>Service Date</th>
    <th>Vehicle Make</th>
    <th>Vehicle Model</th>
    <th>Registration No.</th>
    <th>Status</th>
    <th>ACTIONS</th>
  </tr>
  </thead>
  <tbody>
												    <#list jobCardList as jobCard>
													<tr>
														<td>
														<a class="buttontext" href="<@ofbizUrl>modifyJobCard?jobCardId=${jobCard.jobCardId!}&isUpdate=Y</@ofbizUrl>" title="Code">
      <span class="bookid">${jobCard.jobCardId!}</span></a>
														
														</td>
														<td>${jobCard.firstName!} ${jobCard.lastName!}</td>
														<td><@ofbizCurrency amount=jobCard.grandTotal isoCode="INR"/></td>
														<td>${jobCard.pickup!}</td>
														<td>${jobCard.serviceDate!}</td>
														<td>${jobCard.vehicleId!}</td>
														<td>
														${jobCard.vehicleId!}	
														</td>
														<td>${jobCard.registationNumber!}</td>
														<td><span class="label label-success">${jobCard.status!}</span></td>
														<td><#-- button class="btn btn-primary btn-icon-anim btn-circle" id="open_right_sidebar"><i class="fa fa-file-text"></i></button>
														<button class="btn btn-primary btn-icon-anim btn-circle" id="open_right_sidebar"><i class="fa fa-user"></i></button>
														<button class="btn btn-primary btn-icon-anim btn-circle" id="open_right_sidebar"><i class="fa fa-car"></i></button -->
														<form action="<@ofbizUrl>selectItemForCard</@ofbizUrl>" name="selectItemForCardForm${jobCard.jobCardId!}" id="selectItemForCardForm${jobCard.jobCardId!}" method="post">
                                    	<input type="hidden" name="selectedJobCardId" value="${jobCard.jobCardId!}" />
                                    	<input type="hidden" name="selectedOrderId" value="${jobCard.orderId!}" />
                                    	<input type="hidden" name="selectedVehicleId" value="${jobCard.vehicleId!}" />
                                    </form>
                                    <div  class="pull-right"><a class="fa-plus-circle" href="javascript:document.selectItemForCardForm${jobCard.jobCardId!}.submit();"></a></div>
														<a class="buttontext" href="<@ofbizUrl>modifyJobCard?jobCardId=${jobCard.jobCardId!}&isUpdate=Y</@ofbizUrl>" title="Code"><i class="fa fa-pencil"></i></a>
														<a  target="_blank" href="<@ofbizUrl>jobCardPDF?jobCardId=${jobCard.jobCardId!parameters.jobCardId!}&orderId=${jobCard.orderId!}</@ofbizUrl>" title="Print Job Card" ><img src="\erp\images\exportToPdfIcon.gif"/></a>
														<a target="_blank" href="javascript:void(0);"><img src="\erp\images\exportToPdfIcon.gif"/></a>
														<#-- <@ofbizUrl>jobCardPDF?jobCardId=${shoppingCart.jobCardId!parameters.jobCardId!}</@ofbizUrl> -->
														</td>
													</tr>
													</#list>
													
												</tbody>
</table>
</div>
<script type="text/javascript">
             jQuery(document).ready(function() 
    { 
        $('#sortableData').DataTable();
    
    }); 
        </script>	