<#assign readOnly = "false">

<#if personGV?exists && personGV?has_content>
    <#assign readOnly = "true">
</#if>
<div class="wrapper theme-1-active pimary-color-blue slide-nav-toggle">
    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">
            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Job Card Creation</h5>
                </div>
                    
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="<@ofbizUrl>main</@ofbizUrl>">Dashboard</a></li>
                        <li><a href="#"><span>form</span></a></li>
                        <li class="active"><span>form-layout</span></li>
                    </ol>
                </div>
                <!-- /Breadcrumb -->
                
            </div>
            <!-- /Title -->
       <form action="<@ofbizUrl>createCustomerFromJC</@ofbizUrl>" name="customerForm" id="customerForm" method="post">

            <!-- Row -->
            <div class="row">
            <#assign shoppingCart = sessionAttributes.shoppingCart!>
<#if shoppingCart?has_content>
    <#assign shoppingCartSize = shoppingCart.size()>
<#else>
    <#assign shoppingCartSize = 0>
</#if>
                <div class="col-md-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h4 class="txt-dark">Job Card Number : <code>#<#if shoppingCart?exists && shoppingCart?has_content>${shoppingCart.jobCardId!}</#if></code></h4>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="panel-group accordion-struct accordion-style-1" id="accordion_2" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading activestate" role="tab" id="heading_10">
                                            <h4 class="txt-dark capitalize-font"><a role="button" data-toggle="collapse" data-parent="#accordion_2" href="#collapse_10" aria-expanded="true" ><div class="icon-ac-wrap pr-20"><span class="plus-ac"><i class="ti-plus"></i></span><span class="minus-ac"><i class="ti-minus"></i></span></div>Customer Details</a></h4>
                                        </div>
                                            <div id="collapse_10" class="panel-collapse collapse in" role="tabpanel">
                                                <div class="panel-body pa-15">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label mb-10">Customer Name</label>
                                                                <input type="text" name="customerName" <#if readOnly == "true">readonly="readonly"<#else>required</#if>  id="customerName" value="<#if personGV?exists && personGV?has_content>${personGV.firstName!}<#else>${parameters.customerName!""}</#if>" class="form-control" placeholder="Enter Customer Name">
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group has-error">
                                                                <label class="control-label mb-10">Email</label><#if readOnly == "true">==readonly="readonly=="</#if>
                                                                <input type="email" id="emailAddress" name="emailAddress" <#if readOnly == "true">readonly="readonly"</#if> value="<#if emailGV?exists && emailGV?has_content>${emailGV.infoString!}<#else>${parameters.emailAddress!""}</#if>" class="form-control" placeholder="Enter Email">
                                                            </div>
                                                        </div>
                                                    <!--/span-->
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label mb-10">Mobile Number</label>
                                                                <input type="tel" id="contactNumber" maxlength="11" <#if readOnly == "true">readonly="readonly"<#else>required</#if> name="contactNumber" value="<#if partyPostalAddress?exists && partyPostalAddress?has_content>${partyPostalAddress.contactNumber!""}<#else>${parameters.contactNumber!""}</#if>"  class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile">
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label mb-10">Address</label>
                                                                <input type="text" id="address" <#if readOnly == "true">readonly="readonly"<#else>required</#if>  name="address" value="<#if partyPostalAddress?exists && partyPostalAddress?has_content>${partyPostalAddress.address1!""}<#else>${parameters.address!""}</#if>"  class="form-control" placeholder="Enter Address">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label mb-10">City</label>
                                                                <input type="text" id="city" <#if readOnly == "true">readonly="readonly"<#else>required</#if>  name="city" value="<#if partyPostalAddress?exists && partyPostalAddress?has_content>${partyPostalAddress.city!""}<#else>${parameters.city!""}</#if>"  class="form-control" placeholder="Enter City">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label mb-10">PostalCode</label>
                                                                <input type="text" id="postalCode" <#if readOnly == "true">readonly="readonly"<#else>required</#if>  name="postalCode" value="<#if partyPostalAddress?exists && partyPostalAddress?has_content>${partyPostalAddress.postalCode!""}<#else>${parameters.postalCode!""}</#if>"  class="form-control" placeholder="Enter Postal Code">
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                </div>
                                        </div>
                                    
                                </div>
                                    <div class="panel panel-default">
                                                <input type="hidden" name="partyId" required id="partyId" value="${parameters.partyId!partyId!""}">
                                                <div class="panel-heading" role="tab" id="heading_11">
                                                    <h4 class="txt-dark capitalize-font"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_2" href="#collapse_11" aria-expanded="false"  ><div class="icon-ac-wrap pr-20"><span class="plus-ac"><i class="ti-plus"></i></span><span class="minus-ac"><i class="ti-minus"></i></span></div>Vehicle Details </a></h4>
                                                </div>
                                                <div id="collapse_11" class="panel-collapse collapse" role="tabpanel">
                                                <div class="panel-body pa-15"> 
                                                            <div class="row">
                                                           		<div class="col-md-4 ">
                                                                    <div class="form-group">
                                                                    	<label class="control-label mb-10">Vehicle Model</label>
                                                                        <select class="form-control" name="vehicleModel" id="vehicleModel">
                                                                            <option>--Select Vehicle Model--</option>
                                                                            <#list vehicleModelNameList as vehicleModelId>
                                                                                <option value="${vehicleModelId!}" <#if parameters.vehicleModel?has_content && (parameters.vehicleModel == vehicleModelId)>selected</#if>>${vehicleModelId!}</option>
                                                                            </#list>
                                                                        </select>
                                                                    </div>  
                                                                </div>
                                                                 <div class="col-md-4 ">
                                                                    <div class="form-group">
                                                                    <label class="control-label mb-10">Vehicle Manufacturer</label>
                                                                    <select class="form-control" name="vehicleManufacturer" id="vehicleManufacturer">
                                                                            <option>--Select Vehicle Manufacturer--</option>
                                                                            <#list vehicleList as vehicleGV>
                                                                            <option value="${vehicleGV.modelName!}" <#if parameters.vehicleManufacturer?has_content && (parameters.vehicleManufacturer == vehicleGV.manufacturerName)>selected</#if>>${vehicleGV.manufacturerName!}</option>
                                                                            </#list>
                                                                        </select>
                                                                    </div>  
                                                                </div>
                                                                <div class="col-md-4 ">
                                                                    <div class="form-group">
	                                                                    <label class="control-label mb-10">Vehicle Id</label>
	                                                                    <select class="form-control" name="vehicleId" id="vehicleId">
                                                                            <option>--Select Vehicle Id--</option>
                                                                            <#list vehicleList as vehicleGV>
                                                                            <option value="${vehicleGV.modelName!}" <#if parameters.vehicleId?has_content && (parameters.vehicleId == vehicleGV.vehicleId)>selected</#if>>${vehicleGV.vehicleId!}</option>
                                                                            </#list>
                                                                        </select>
                                                                    </div>  
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="row">
                                                               <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Fuel Type</label>
                                                                        <select class="form-control" name="vehicleFuelType" id="vehicleFuelType">
                                                                            <option>--Select Vehicle Variant--</option>
                                                                            <#list vehicleList as vehicleGV>
                                                                                <option value="${vehicleGV.modelName!}" <#if parameters.vehicleFuelType?exists && parameters.vehicleFuelType?has_content && (parameters.vehicleFuelType == vehicleGV.fuelType!)>selected</#if>>${vehicleGV.fuelType!}</option>
                                                                            </#list>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                               <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Variant</label>
                                                                        <select class="form-control" name="vehicleVariant" id="vehicleVariant">
                                                                            <option>--Select Vehicle Variant--</option>
                                                                            <#list vehicleList as vehicleGV>
                                                                            	<#if vehicleGV.variant?exists && vehicleGV.variant?has_content && vehicleGV.variant != "">
                                                                                	<option value="${vehicleGV.modelName!}" <#if parameters.vehicleVariant?has_content && (parameters.vehicleVariant == vehicleGV.variant!)>selected</#if>>${vehicleGV.variant!}</option>
                                                                           		</#if>
                                                                            </#list>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Oil Capacity</label>
                                                                        <select class="form-control" name="oilCapacity" id="oilCapacity">
                                                                            <option>--Select oil Capacity--</option>
                                                                            <#list vehicleList as vehicleGV>
                                                                                <option value="${vehicleGV.modelName!}" <#if parameters.oilCapacity?has_content && (parameters.oilCapacity == vehicleGV.oilCapacity!)>selected</#if>>${vehicleGV.oilCapacity!}</option>
                                                                            </#list>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Vehicle Reg. No.</label>
                                                                        <input type="text" id="VehicleRegNo" name="VehicleRegNo" value="${parameters.VehicleRegNo!""}" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Manufacturing Year</label>
                                                                        <select class="form-control" name="VehicleRegYear">
                                                                            <option>--Select Vehicle Manufacturer Year--</option>
                                                                            <#if vehicleYList?exists && vehicleYList?has_content>
                                                                            <#list vehicleYList as vehicleY>
                                                                                <option value="${vehicleY!}" <#if parameters.VehicleRegYear?has_content && (parameters.VehicleRegYear == vehicleY)>selected</#if>>${vehicleY!}</option>
                                                                            </#list>
                                                                            </#if>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Anniv. Date</label>
                                                                        <input type="text" name="AnnivDate" value="${parameters.AnnivDate!}"  placeholder="DD/MM/YYYY" data-mask="99/99/9999"  class="form-control"  id="datepicker15" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <!-- /Row -->
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Last Service Date</label>
                                                                        <input type="text" name="LastServiceDate" value="${parameters.LastServiceDate!}"  placeholder="DD/MM/YYYY" data-mask="99/99/9999"  class="form-control"  id="datepicker16" readonly>
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Service Date</label>
                                                                        <input type="text" name="ServiceDate" value="${parameters.ServiceDate!}"  placeholder="DD/MM/YYYY" data-mask="99/99/9999"  class="form-control"  id="datepicker17" readonly>
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Insurance Company</label>
                                                                        <select class="form-control" name="vehicleInsuranceComp">
                                                                            <option>--Select Company--</option>
                                                                            <option value="ICICI" <#if parameters.vehicleInsuranceComp?has_content && (parameters.vehicleInsuranceComp == "ICICI")>selected</#if>>ICICI</option>
                                                                            <option value="HDFC-Ergo" <#if parameters.vehicleInsuranceComp?has_content && (parameters.vehicleInsuranceComp == "HDFC-Ergo")>selected</#if>>HDFC-Ergo</option>
                                                                            <option value="NationalInsurance" <#if parameters.vehicleInsuranceComp?has_content && (parameters.vehicleInsuranceComp == "NationalInsurance")>selected</#if>>NationalInsurance</option>
                                                                            <option value="OBC" <#if parameters.vehicleInsuranceComp?has_content && (parameters.vehicleInsuranceComp == "OBC")>selected</#if>>OBC</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- /Row -->
                                                            <div class="row">
                                                                
                                                                <!--/span-->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Insurance Date</label>
                                                                        <input type="text" name="InsuranceDate" value="${parameters.InsuranceDate!}"  placeholder="DD/MM/YYYY" data-mask="99/99/9999"  class="form-control"  id="datepicker18" readonly>
                                                                        
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Payment mode</label>
                                                                        <select class="form-control"  name="paymentMode">
                                                                            <option>--Select Payment Type--</option>
                                                                            <option value="CASH" <#if parameters.paymentMode?has_content && parameters.paymentMode?upper_case == "CASH">selected</#if>>Cash</option>
                                                                            <option value="DEBIT-CARD" <#if parameters.paymentMode?has_content && parameters.paymentMode?upper_case == "DEBIT-CARD">selected</#if>>Debit-Card</option>
                                                                            <option value="CREDIT-CARD" <#if parameters.paymentMode?has_content && parameters.paymentMode?upper_case == "CREDIT-CARD">selected</#if>>Credit-Card</option>
                                                                            <option value="PAYTM" <#if parameters.paymentMode?has_content && parameters.paymentMode?upper_case == "PAYTM">selected</#if>>Paytm</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label mb-10">Customer Type</label>
                                                                        <select class="form-control"   name="customerType">
                                                                            <option>--Select Customer Type--</option>
                                                                            <option value="GAADIZO" <#if parameters.customerType?has_content && parameters.customerType?upper_case == "GAADIZO">selected</#if>>Gaadizo</option>
                                                                            <option value="INDEPENDENT"<#if parameters.customerType?has_content && parameters.customerType?upper_case == "INDEPENDENT">selected</#if>>Independent</option>
                                                                            <option value="WALK-IN"<#if parameters.customerType?has_content && parameters.customerType?upper_case == "WALK-IN">selected</#if>>Walk-In</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                </div>
                                                <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <input type="submit" id="submitV"  name="submit" value="Save Customer & Vehicle"  class="form-control" placeholder="Save Vehicle">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    </div>
                    <!-- /Row -->
                    
                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark">Service Detail</h6>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-primary btn-rounded btn-icon left-icon btn-xs" id="addServiceButton"> <i class="fa fa-plus"></i> <span>Add Service</span></a>   
                                </div>
                                <div id="addItemInJobCard" style="display:none;">
                                   ${screens.render("component://gaadizo/widget/GaadizoScreens.xml#AddItemToJobCard")}
                                </div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        <div>
                                            <table class="tablesaw table-hover table" data-tablesaw-mode="columntoggle">
                                                <thead>
                                                    <tr>
                                                      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Service Item ID</th>
                                                      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Qty</th>
                                                      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Item Description</th>
                                                      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Service Cost</th>
                                                      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Discount %</th>
                                                      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Tax</th>
                                                      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Total Amount</th>
                                                      <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <#list shoppingCart.items() as cartLine>
                                                <#assign cartLineIndex = shoppingCart.getItemIndex(cartLine)>
                                                <tr>
                                                  <td class="title"><#if cartLine.getProductId()??>${cartLine.getProductId()}</#if></td>
                                                   <td class="title"><#if cartLine.getQuantity()??>${cartLine.getQuantity()}</#if></td>
                                                  <td>${cartLine.getName()!}</td>
                                                  <td><@ofbizCurrency amount=cartLine.getBasePrice()!"0" isoCode=INR/></td>
                                                  <td><#-- <input type="text" class="form-control input-sml" placeholder="Discount %"> -->
                                                  <@ofbizCurrency amount=cartLine.getOtherAdjustments()!"0" isoCode=INR/>
                                                  </td>
                                                  <td>&#x20B9; 200</td>
                                                  <td><@ofbizCurrency amount=cartLine.getDisplayItemSubTotal()!"0" isoCode=INR/></td>
                                                  <td><a href="#"><i class="zmdi zmdi-delete"></i><span> Delete</span></a></td>
                                                </tr>
                                                </#list>
                                                </tbody>
                                                <tfoot>
                                                <tr class="bg-grey">
                                                  <td class="title"></td>
                                                  <td class="title"></td>
                                                  <td>Total</td>
                                                  <td><@ofbizCurrency amount=shoppingCart.getSubTotal()!"0" isoCode=INR/> </td>
                                                  <td></td>
                                                  <td><@ofbizCurrency amount=shoppingCart.getTotalSalesTax()!"0" isoCode=INR/></td>
                                                  <td><@ofbizCurrency amount=shoppingCart.getGrandTotal()!"0" isoCode=INR/></td>
                                                  
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- /Row -->
                <div class="form-actions mt-10 fright">
                    <button type="button" class="btn btn-default">Cancel</button> 
                    <input type="submit" class="form-control input-sml" value="Create Job Card" placeholder="Create Job Card"> 
                    <button type="submit" 
             class='input_submit buttontextbig' 
             style="margin-right: 15px;" 
             onClick="submitForm(document.customerForm, 'DN', '')">Place Order
     </button>
                </div>
                </form>
            </div>
                
                            
            </div>
            <!-- /Main Content -->
        
</div>
<script language="JavaScript" type="text/javascript">
    function submitForm(form, mode, value) {
        document.getElementById("customerForm").action="<@ofbizUrl>processorder</@ofbizUrl>";
        document.customerForm.submit();
    }
</script>
        <!-- /#wrapper -->
        <script language="JavaScript" type="text/javascript">
jQuery(document).ready(function() {
    jQuery('#addServiceButton').click(function(){
        
        jQuery("#addItemInJobCard").toggle();
    });
});
</script>
<#--charu : automatic selection of all the dependent dropdowns when user select model name-->
<script language="JavaScript" type="text/javascript">
	jQuery('#vehicleModel').on( 'change', function() {
		jQuery('#vehicleManufacturer').val(this.value);
		jQuery('#vehicleId').val(this.value);
		jQuery('#vehicleVariant').val(this.value);
		jQuery('#oilCapacity').val(this.value);
		jQuery('#vehicleFuelType').val(this.value);
	} ).trigger( 'change' );
	

</script>
        <#-- 
        <script>
        w3.includeHTML();
            function customerInfoCreate(shippingMethod) {
            listDocument = '<@ofbizUrl>ViewContentDetail</@ofbizUrl>';
           jQuery.ajax({
            url: listDocument,
            type: 'POST',
            data: {"contentId" : contentId},
            error: function(msg) {
                showErrorAlert("${uiLabelMap.CommonErrorMessage2}","${uiLabelMap.ErrorLoadingContent} : " + msg);
            },
            success: function(msg) {
                jQuery('#Document').html(msg);
            }
        });
    }
        </script -->
        <#-- script>
        jQuery(document).ready(function () 
    {
    
        var autoSuggestionList = [""];
        jQuery(function() 
        {
            jQuery("#CustomerName").autocomplete({source: autoSuggestionList});
        });
        
        jQuery("#CustomerName").keyup(function(e) 
        {
            var keyCode = e.keyCode;
            if(keyCode != 40 && keyCode != 38)
            {
              var searchText = jQuery(this).attr('value');
            
              jQuery("#CustomerName").autocomplete({
                appendTo:"#searchAutoComplete",
                source: function(request, response) {
                jQuery.ajax({
                    url: "<@ofbizUrl secure="${request.isSecure()?string}">findAutoSuggestions?searchText="+searchText+"</@ofbizUrl>",
                    dataType: "json",
                    type: "POST",
                    success: function(data) {
                    alert(data.autoSuggestionList);
                    if(data.autoSuggestionList != null)
                    {
                        response(jQuery.map(data.autoSuggestionList, function(item) 
                        {
                            return {
                                value: item
                            }
                        }))
                    }
                    else
                    {
                        response(function() {
                            return {
                                value: ""
                            }
                        })
                    }
                }
                
            });
          },
          minLength: 1
          });
        }
    });
     });
        </script> -->
        
    

<script type="text/javascript">
    jQuery(document).ready(function() 
    { 
        jQuery("#datepicker13").datetimepicker({format: 'DD-MM-YYYY HH:mm:ss.fff'});
        jQuery("#datepicker14").datetimepicker({format: 'DD-MM-YYYY HH:mm:ss'});
        jQuery("#datepicker15").datetimepicker({format: 'DD-MM-YYYY HH:mm:ss'});
        jQuery("#datepicker16").datetimepicker({format: 'DD-MM-YYYY HH:mm:ss'});
        jQuery("#datepicker17").datetimepicker({format: 'DD-MM-YYYY HH:mm:ss.fff', minDate: new Date()});
        jQuery("#datepicker18").datetimepicker({format: 'DD-MM-YYYY HH:mm:ss'});
        jQuery("#datepicker2").datetimepicker({format: 'DD-MM-YYYY HH:mm:ss'});
        jQuery("#datepicker3").datetimepicker({format: 'DD-MM-YYYY HH:mm:ss'});
        /* if (jQuery('submitCustomerData')) {
        jQuery("#submitCustomerData").click(function(e) 
        {
            new Ajax.Request('getFinAccountTransRunningTotalAndBalances', {
                asynchronous: false,
                onSuccess: function(transport) {
                    var data = transport.responseText.evalJSON(true);
                    $('showFinAccountTransRunningTotal').update(data.finAccountTransRunningTotal);
                    $('finAccountTransRunningTotal').update(data.finAccountTransRunningTotal);
                    $('numberOfFinAccountTransaction').update(data.numberOfTransactions);
                    $('endingBalance').update(data.endingBalance);
                }, parameters: jQuery('customerForm').serialize(), requestHeaders: {Accept: 'application/json'}
            });
         });
        } */
        
    }); 
</script>