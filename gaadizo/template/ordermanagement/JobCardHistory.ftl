<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
						
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="sortableData" class="table  display table-hover mb-30">
												<thead>
													<tr>
														<th>Booking Id</th>
														<th>Order Amount</th>
														<th>Service Date</th>
														<th>Status</th>
													</tr>
												</thead>

												<tbody>
												    <#list jobCardHistoryList as jobCard>
													<tr>
														<td>
														<${jobCard.jobCardId!}
														</td>
														<td><@ofbizCurrency amount=jobCard.grandTotal isoCode="INR"/></td>
														<td>${jobCard.serviceDate!}</td>
														<td>${jobCard.status!}</td>
													</tr>
													</#list>
													
												</tbody>
											</table>
										</div>
									</div>	
								</div>	
							</div>
						</div>
					</div>
				</div>