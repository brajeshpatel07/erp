<div class="title2"><span>Job Card Search</span></div>

<form class="form-horizontal formCenter" method="post" id="findBookingForm" name="findBooking" onsubmit="javascript:submitFormDisableSubmits(this)" action="<@ofbizUrl>findJobcardilist</@ofbizUrl>">
<input type="hidden" name="noConditionFind" value="Y" id="FindInvoices_noConditionFind">
                <input type="hidden" name="hideSearch" value="Y" id="FindInvoices_hideSearch">
<div class="row form-group">
<div class="col-sm-4">
<label>Job Card ID</label>
<input id="jobCardId" type="text" name="jobCardId" class="form-control" value="${parameters.jobCardId!}" placeholder="">
</div>

<div class="col-sm-4">
<label>Booking ID</label>
<input id="bookingId" type="text" size="25" name="bookingId" class="form-control" value="${parameters.bookingId!}" placeholder="">
</div>

<div class="col-sm-4">
<label>Customer Name</label>
<input class="form-control" type="text" id="CustomerName" name="CustomerName" maxlength="40" value="${parameters.CustomerName!""}" placeholder=""/>
</div>

<div class="col-sm-4">
<label>Customer Source</label>
<input type="text" name="" class="form-control" placeholder="">
</div>
 
<div class="col-sm-4">
<label>From Date</label>
<div class="relative"><i class="fa fa-calendar"></i>
<input type="text" name="fromDate" value="${parameters.fromDate!}" class="form-control date"  id="datepicker13" readonly placeholder="">
</div>

</div>

<div class="col-sm-4">
<label>To Date</label>
<div class="relative"><i class="fa fa-calendar"></i><input type="text" name="" class="form-control date" placeholder=""></div>
</div>

</div>
<input type="submit" class="btn btn-danger" value="FIND">
</form>
<script type="text/javascript">
             jQuery(document).ready(function() 
    { 
        jQuery("#datepicker13").datetimepicker({format: 'DD-MM-YYYY HH:mm:ss'});
        jQuery("#datepicker14").datetimepicker({format: 'DD-MM-YYYY HH:mm:ss'});
        jQuery("#datepicker2").datetimepicker({format: 'DD-MM-YYYY HH:mm:ss'});
        jQuery("#datepicker3").datetimepicker({format: 'DD-MM-YYYY HH:mm:ss'});
    
    }); 
        </script>	