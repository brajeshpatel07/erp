<#if (parameters.jobCardId?exists && parameters.jobCardId?has_content)>

<div class="title2"><span>Edit Job Card#${parameters.jobCardId!}</span></div>
<#assign jobCardGV = (delegator.findOne("JobCard", {"jobCardId" : parameters.jobCardId}, true))!>
<div  class="pull-right"><a class="button-blue" href="<@ofbizUrl>clearcartFromJobCard</@ofbizUrl>">Create New Job Card</a></div>
<form action="<@ofbizUrl>selectItemForCard</@ofbizUrl>" name="selectItemForCardForm" id="selectItemForCardForm" method="post">
                                    	<input type="hidden" name="jobCardId" value="${jobCardGV.jobCardId!}" />
                                    	<input type="hidden" name="orderId" value="${jobCardGV.orderId!}" />
                                    	<input type="hidden" name="vehicleId" value="${jobCardGV.vehicleId!}" />
                                    </form>
                                    <div  class="pull-right"><a class="button-blue" href="javascript:document.selectItemForCardForm.submit();">Add spare Part</a></div>

<#else>
<div class="title2"><span>Create Job Card</span></div>
</#if>


<#assign readOnly = "false">

<#if (personGV?exists && personGV?has_content)>
    <#assign readOnly = "true">
</#if>
<#if (parameters.editJobCard?exists && parameters.editJobCard?has_content && parameters.editJobCard == "true")>
    <#assign readOnly = "false">
</#if>
<div class="paddingbott20">
  <form action="<@ofbizUrl>${parameters.jobCardFormAction!}</@ofbizUrl>" name="customerForm" id="customerForm" method="post">
  	<#assign shoppingCart = sessionAttributes.shoppingCart!>
	<#if shoppingCart?has_content>
    	<#assign shoppingCartSize = shoppingCart.size()>
	<#else>
    	<#assign shoppingCartSize = 0>
	</#if>
	
	===${lastJobCardDetail!}===
	<div id="accordion1" class="panel-group accordion">
         <#if (parameters.jobCardId?exists && parameters.jobCardId?has_content) || (shoppingCart.jobCardId?exists && shoppingCart.jobCardId?has_content)>
             <div class="clearfix"><a href="<@ofbizUrl>editJobCard?jobCardId=${shoppingCart.jobCardId!parameters.jobCardId!}</@ofbizUrl>">Edit</a></div>
             <div class="clearfix"><a target="_blank" href="<@ofbizUrl>jobCardPDF?jobCardId=${shoppingCart.jobCardId!jobCard.jobCardId!}&orderId=${(jobCard.orderId)!}</@ofbizUrl>" title="Print Job Card" ><img src="\erp\images\exportToPdfIcon.gif"/></a></div>
         </#if>
  <div class="panel">
    <div class="panel-title"><a data-parent="#accordion1" data-toggle="collapse" href="#accordion11" class="" aria-expanded="true"><h3> Customer Details</h3></a></div>
    <div id="accordion11" class="panel-collapse collapse in" role="tablist" aria-expanded="true">
      <div class="panel-content">
       

<div class="row form-group">
<div class="col-md-3 col-sm-4">
<label>Customer Name</label>
<input type="text" name="customerName" <#if readOnly == "true">readonly="readonly"<#else>required</#if>  id="customerName" value="<#if personGV?exists && personGV?has_content>${personGV.firstName!}<#else>${parameters.customerName!""}</#if>" class="form-control" placeholder="Enter Customer Name">

</div>

<div class="col-md-3 col-sm-4">
<label>Email ID</label>
<#if emailGV?exists && emailGV?has_content && emailGV.contactMechId?has_content><input type="hidden" name="emailAddressContactMechId" value=${emailGV.contactMechId!}" /></#if>
                                                                <input type="email" id="emailAddress" name="emailAddress" <#if readOnly == "true">readonly="readonly"</#if> value="<#if emailGV?exists && emailGV?has_content>${emailGV.infoString!}<#else>${parameters.emailAddress!""}</#if>" class="form-control" placeholder="Enter Email">
</div>

<div class="col-md-3 col-sm-4">
<label>Mobile Number</label>
<#if partyPostalAddress?exists && partyPostalAddress?has_content && partyPostalAddress.contactMechId?has_content><input type="hidden" name="addressContactMechId" value=${partyPostalAddress.contactMechId!}" /></#if>
                                                                <input type="tel" id="contactNumber" maxlength="11" <#if readOnly == "true">readonly="readonly"<#else>required</#if> name="contactNumber" value="<#if partyPostalAddress?exists && partyPostalAddress?has_content>${partyPostalAddress.contactNumber!""}<#else>${parameters.contactNumber!""}</#if>"  class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile">
</div>

<div class="col-md-3 col-sm-4">
<label>Address</label>
<input type="text" id="address" <#if readOnly == "true">readonly="readonly"<#else>required</#if>  name="address" value="<#if partyPostalAddress?exists && partyPostalAddress?has_content>${partyPostalAddress.address1!""}<#else>${parameters.address!""}</#if>"  class="form-control" placeholder="Enter Address">
</div>

<div class="col-md-3 col-sm-4">
<label>State</label>
<#assign SELECTED_STATE = Static["org.apache.ofbiz.base.util.UtilProperties"].getMessage("gaadizo", "SELECTED_STATE", Static["org.apache.ofbiz.base.util.UtilMisc"].toMap(), locale)/>
<select class="form-control"   name="state">
    <#list geoAssocList as state>
        <option value="${state.geoId}" <#if state.geoId?has_content && state.geoId?upper_case == SELECTED_STATE>selected</#if>>${state.geoName?default(state.geoId)}</option>
    </#list>
    </select>
<#-- label>Postal Code</label>
<input type="text" id="postalCode" <#if readOnly == "true">readonly="readonly"<#else>required</#if>  name="postalCode" value="<#if partyPostalAddress?exists && partyPostalAddress?has_content>${partyPostalAddress.postalCode!""}<#else>${parameters.postalCode!""}</#if>"  class="form-control" placeholder="Enter Postal Code" -->
</div>

<div class="col-md-3 col-sm-4">
<label>City</label>
<input type="text" id="city" <#if readOnly == "true">readonly="readonly"<#else>required</#if>  name="city" value="<#if partyPostalAddress?exists && partyPostalAddress?has_content>${partyPostalAddress.city!""}<#else>${parameters.city!""}</#if>"  class="form-control" placeholder="Enter City">
</div>


<input type="hidden" name="partyId" required id="partyId" value="${parameters.partyId!partyId!""}">
<input type="hidden" name="postalCode" required id="postalCode" value="122001">
                                                <input type="hidden" name="vehicleModel" required id="vehicleModelH" value="${parameters.vehicleModel!vehicleModel!""}">
                                                <input type="hidden" name="vehicleId" required id="vehicleIdH" value="${parameters.vehicleId!vehicleId!""}">
                                                <input type="hidden" name="vehicleVariant" required id="vehicleVariantH" value="${parameters.vehicleVariant!vehicleVariant!""}">
                                                <input type="hidden" name="oilCapacity" required id="oilCapacityH" value="${parameters.oilCapacity!oilCapacity!""}">
                                                <input type="hidden" name="vehicleFuelType" required id="vehicleFuelTypeH" value="${parameters.vehicleFuelType!vehicleFuelType!""}">
                                                <input type="hidden" name="vehicleManufacturer" required id="vehicleManufacturerH" value="${parameters.vehicleManufacturer!vehicleManufacturer!""}">
                                                <input type="hidden" name="VehicleRegYear" required id="VehicleRegYearH" value="${parameters.VehicleRegYear!VehicleRegYear!""}">
                                                <input type="hidden" name="jobCardId" required id="jobCardId" value="${parameters.jobCardId!jobCardId!""}">
<div class="col-md-3 col-sm-4">
<label>Customer Source</label>
<select class="form-control"   name="customerType">
                                                                            <option>--Select Customer Type--</option>
                                                                            <option value="GAADIZO" <#if parameters.customerType?has_content && parameters.customerType?upper_case == "GAADIZO">selected</#if>>Gaadizo</option>
                                                                            <option value="INDEPENDENT"<#if parameters.customerType?has_content && parameters.customerType?upper_case == "INDEPENDENT">selected</#if>>Independent</option>
                                                                            <option value="WALK-IN"<#if parameters.customerType?has_content && parameters.customerType?upper_case == "WALK-IN">selected</#if>>Walk-In</option>
                                                                        </select>
</div>

<div class="col-md-3 col-sm-4">
<label>Vehicle Registration</label>
<input type="text" id="VehicleRegNo" name="registationNumber" value="${parameters.vehicleRegNo!""}" class="form-control" placeholder="Enter Vehicle Registration">
</div>

</div>

      </div>
    </div>
  </div>
  
  <div class="panel">
    <div class="panel-title"><a class="collapsed" data-parent="#accordion1" data-toggle="collapse" href="#accordion12" aria-expanded="false"><h3> VEHICLE DETAILS</h3></a></div>
    <div id="accordion12" class="panel-collapse collapse" role="tablist" aria-expanded="false">
      <div class="panel-content">

<div class="row form-group">
<div class="col-md-3 col-sm-4">
<label>Vehicle Model</label>
<select class="form-control" name="vehicleModel1" id="vehicleModel">
                                                                            <option value="NA">--Select Vehicle Model--</option>
                                                                            <#list vehicleList as vehicleModelId>
                                                                                <option value="${vehicleModelId.modelId!}" <#if parameters.vehicleModel?has_content && (parameters.vehicleModel == vehicleModelId.modelId)>selected</#if>>${vehicleModelId.searchName!}</option>
                                                                            </#list>
                                                                        </select>
</div>

<div class="col-md-3 col-sm-4">
<label>Vehicle Manufacturer</label>
<select class="form-control" name="vehicleManufacturer1" id="vehicleManufacturer">
                                                                            <option>--Select Vehicle Manufacturer--</option>
                                                                            <#list vehicleList as vehicleGV>
                                                                            <option value="${vehicleGV.modelId!}" <#if parameters.vehicleManufacturer?has_content && (parameters.vehicleManufacturer == vehicleGV.manufacturerName)>selected</#if>>${vehicleGV.manufacturerName!}</option>
                                                                            </#list>
                                                                        </select>
</div>

<div class="col-md-3 col-sm-4">
<label>Vehicle ID</label>
<select class="form-control" name="vehicleId1" id="vehicleId">
                                                                            <option>--Select Vehicle Id--</option>
                                                                            <#list vehicleList as vehicleGV>
                                                                            <option value="${vehicleGV.modelId!}" <#if parameters.vehicleId?has_content && (parameters.vehicleId == vehicleGV.vehicleId)>selected</#if>>${vehicleGV.vehicleId!}</option>
                                                                            </#list>
                                                                        </select>
</div>

<div class="col-md-3 col-sm-4">
<label>Fuel Type</label>
<select class="form-control" name="vehicleFuelType1" id="vehicleFuelType">
                                                                            <option>--Select Vehicle Variant--</option>
                                                                            <#list vehicleList as vehicleGV>
                                                                                <option value="${vehicleGV.modelId!}" <#if parameters.vehicleFuelType?exists && parameters.vehicleFuelType?has_content && (parameters.vehicleFuelType == vehicleGV.type!)>selected</#if>>${vehicleGV.type!}</option>
                                                                            </#list>
                                                                        </select>
</div>
 
<div class="col-md-3 col-sm-4">
<label>Variant</label>
<select class="form-control" name="vehicleVariant1" id="vehicleVariant">
                                                                            <option>--Select Vehicle Variant--</option>
                                                                            <#list vehicleList as vehicleGV>
                                                                            	<#if vehicleGV.variant?exists && vehicleGV.variant?has_content && vehicleGV.variant != "">
                                                                                	<option value="${vehicleGV.modelId!}" <#if parameters.vehicleVariant?has_content && (parameters.vehicleVariant == vehicleGV.variant!)>selected</#if>>${vehicleGV.variant!}</option>
                                                                           		</#if>
                                                                            </#list>
                                                                        </select>
</div>

<div class="col-md-3 col-sm-4">
<label>Oil Capacity</label>
<select class="form-control" name="oilCapacity1" id="oilCapacity">
                                                                            <option>--Select oil Capacity--</option>
                                                                            <#list vehicleList as vehicleGV>
                                                                                <option value="${vehicleGV.modelId!}" <#if parameters.oilCapacity?has_content && (parameters.oilCapacity == vehicleGV.fuelType!)>selected</#if>>${vehicleGV.fuelType!}</option>
                                                                            </#list>
                                                                        </select>
</div>

<div class="col-md-3 col-sm-4">
<label>Manufacturing Year</label>
<select class="form-control" name="VehicleRegYear1" id="VehicleRegYear">
                                                                            <option>--Select Vehicle Manufacturer Year--</option>
                                                                            <#if vehicleList?exists && vehicleList?has_content>
                                                                            <#list vehicleList as vehicleGV>
                                                                                <option value="${vehicleGV.modelId!}" <#if parameters.VehicleRegYear?has_content && (parameters.VehicleRegYear == vehicleGV.year)>selected</#if>>${vehicleGV.year!}</option>
                                                                            </#list>
                                                                            </#if>
                                                                        </select>
</div>

<div class="col-md-3 col-sm-4">
<label>Service Date</label>
<input class="form-control date" id="serviceDate" name="serviceDate" value="${parameters.serviceDate!}"  placeholder="MM/DD/YYYY" type="text"/>
</div>


<div class="col-md-3 col-sm-4">
<label>Delivery Date</label>
<input type="text" name="deliveryDate" value="${parameters.deliveryDate!}" id="date" class="form-control date" placeholder="Enter Delivery Date">
</div>

<div class="col-md-3 col-sm-4">
<label>Last Service Date</label>
<input type="text" name="lastServiceDate" id="date" value="${parameters.lastServiceDate!}" class="form-control date" placeholder="Enter Last Service Date">
</div>

<div class="col-md-3 col-sm-4">
<label>Insurance Company</label>
<select class="form-control" name="vehicleInsuranceComp">
                                                                            <option>--Select Company--</option>
                                                                            <option value="ICICI" <#if parameters.vehicleInsuranceComp?has_content && (parameters.vehicleInsuranceComp == "ICICI")>selected</#if>>ICICI</option>
                                                                            <option value="HDFC-Ergo" <#if parameters.vehicleInsuranceComp?has_content && (parameters.vehicleInsuranceComp == "HDFC-Ergo")>selected</#if>>HDFC-Ergo</option>
                                                                            <option value="NationalInsurance" <#if parameters.vehicleInsuranceComp?has_content && (parameters.vehicleInsuranceComp == "NationalInsurance")>selected</#if>>NationalInsurance</option>
                                                                            <option value="OBC" <#if parameters.vehicleInsuranceComp?has_content && (parameters.vehicleInsuranceComp == "OBC")>selected</#if>>OBC</option>
                                                                        </select>
</div>

<div class="col-md-3 col-sm-4">
<label>Insurance Expiry Date</label>
<input type="text" name="insuranceExpDate" value="${parameters.insuranceExpDate!}"  class="form-control" placeholder="Enter Insurance Date">
</div>


<div class="col-md-3 col-sm-4">
<label>Payment Mode</label>
<select class="form-control"  name="paymentMode">
                                                                            <option>--Select Payment Type--</option>
                                                                            <option value="CASH" <#if parameters.paymentMode?has_content && parameters.paymentMode?upper_case == "CASH">selected</#if>>Cash</option>
                                                                            <option value="DEBIT-CARD" <#if parameters.paymentMode?has_content && parameters.paymentMode?upper_case == "DEBIT-CARD">selected</#if>>Debit-Card</option>
                                                                            <option value="CREDIT-CARD" <#if parameters.paymentMode?has_content && parameters.paymentMode?upper_case == "CREDIT-CARD">selected</#if>>Credit-Card</option>
                                                                            <option value="PAYTM" <#if parameters.paymentMode?has_content && parameters.paymentMode?upper_case == "PAYTM">selected</#if>>Paytm</option>
                                                                        </select>
</div>
<div class="col-md-3 col-sm-4">
<label>Vehicle Color</label>

<#assign VEHICLE_COLOR = Static["org.apache.ofbiz.base.util.UtilProperties"].getMessage("gaadizo", "VEHICLE_COLOR", Static["org.apache.ofbiz.base.util.UtilMisc"].toMap(), locale)/>
<#assign colorList = Static["org.apache.ofbiz.base.util.StringUtil"].split(VEHICLE_COLOR,",") />
<select class="form-control"  name="color">
     <option>--Select Color--</option>
     <#if colorList?has_content>
         <#list colorList as color>
         	<option value="${color!}" <#if parameters.color?has_content && parameters.color == color>selected</#if>>${color!}</option>
         </#list>
     </#if>
 </select>
</div>
 

</div>


      </div>
    </div>
  </div>
</div>

<input type="submit" class="btn btn-danger" value="SAVE CUSTOMER  & VEHICLE">
</form>
</div>
<script>
$(document).ready(function(){
var date_input=$('.date'); //our date input has the name "date"
var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
date_input.datepicker({
	format: 'mm/dd/yyyy',
	container: container,
	todayHighlight: true,
	autoclose: true,
})
})
</script>
<script>
jQuery(document).ready(function(){
var date_input=jQuery(".date"); //our date input has the name "date"
var serviceDate_input=jQuery("#serviceDate"); //our date input has the name "date"

var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
var dateToday = new Date(); 
serviceDate_input.datetimepicker({
	'DD-MM-YYYY HH:mm:ss.fff',
	container: container,
	todayHighlight: true,
	autoclose: false,
	minDate: new Date()
})
date_input.datepicker({
	format: 'mm/dd/yyyy',
	container: container,
	todayHighlight: true,
	autoclose: false,
})
})
</script>
<script>
jQuery(document).ready(function(){

jQuery("#datepicker15").datetimepicker({format: 'DD-MM-YYYY HH:mm:ss'});
        jQuery("#datepicker16").datetimepicker({format: 'DD-MM-YYYY HH:mm:ss',maxDate: new Date()});
        jQuery("#datepicker17").datetimepicker({format: 'DD-MM-YYYY HH:mm:ss.fff', minDate: new Date()});
        jQuery("#datepicker18").datetimepicker({format: 'DD-MM-YYYY HH:mm:ss'});


})
</script>
<script language="JavaScript" type="text/javascript">
	jQuery('#vehicleModel').on( 'change', function() {
		if(this.value != null && this.value != "NA")
		{
			jQuery('#oilCapacity option[value!=null]').css('display','true');
			jQuery('#vehicleManufacturer option[value!=null]').css('display','true');
			jQuery('#vehicleId option[value!=null]').css('display','true');
			jQuery('#vehicleVariant option[value!=null]').css('display','true');
			jQuery('#vehicleFuelType option[value!=null]').css('display','true');
			jQuery('#vehicleManufacturer').val(this.value);
			jQuery('#VehicleRegYear').val(this.value);
			jQuery('#vehicleId').val(this.value);
			jQuery('#vehicleVariant').val(this.value);
			jQuery('#oilCapacity').val(this.value);
			jQuery('#vehicleFuelType').val(this.value);
			jQuery('#oilCapacity option[value!="' + this.value +  '"]').css('display','none');
			jQuery('#vehicleManufacturer option[value!="' + this.value +  '"]').css('display','none');
			jQuery('#vehicleId option[value!="' + this.value +  '"]').css('display','none');
			jQuery('#vehicleVariant option[value!="' + this.value +  '"]').css('display','none');
			jQuery('#vehicleFuelType option[value!="' + this.value +  '"]').css('display','none');
			jQuery('#oilCapacityH').val(jQuery('#oilCapacity').find(":selected").text());
			jQuery('#vehicleManufacturerH').val(jQuery('#vehicleManufacturer').find(":selected").text());
			jQuery('#vehicleVariantH').val(jQuery('#vehicleVariant').find(":selected").text());
			jQuery('#vehicleFuelTypeH').val(jQuery('#vehicleFuelType').find(":selected").text());
			jQuery('#vehicleIdH').val(this.value);
			jQuery('#vehicleModelH').val(this.value);
			
		}
	} ).trigger( 'change' );
	

</script>
