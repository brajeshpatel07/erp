			<!-- Main Content -->
	    <div class="page-wrapper">
			<div class="container-fluid">
					<!-- Title -->
					<div class="row heading-bg">
						<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
							<h5 class="txt-dark">Job Card Creation</h5>
						</div>
					
						<!-- Breadcrumb -->
						<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
							<ol class="breadcrumb">
								<li><a href="<@ofbizUrl>main</@ofbizUrl>">Dashboard</a></li>
								<#-- li><a href="#"><span>form</span></a></li>
								<li class="active"><span>form-layout</span></li -->
							</ol>
						</div>
						<!-- /Breadcrumb -->
					
					</div>
					<!-- /Title -->
					
					<!-- Row -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h4 class="txt-dark">Job Card Number : <code>#${orderHeaderAndjobCardGV.jobCardId!}</code></h4>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
									<form name="updateJobCard" method="post" action=""> 
										<div class="panel-group accordion-struct accordion-style-1" id="accordion_2" role="tablist" aria-multiselectable="true">
											<div class="panel panel-default">
												<div class="panel-heading activestate" role="tab" id="heading_10">
													<h4 class="txt-dark capitalize-font"><a role="button" data-toggle="collapse" data-parent="#accordion_2" href="#collapse_10" aria-expanded="true" ><div class="icon-ac-wrap pr-20"><span class="plus-ac"><i class="ti-plus"></i></span><span class="minus-ac"><i class="ti-minus"></i></span></div>Job Card Details</a></h4>
												</div>
												<div id="collapse_10" class="panel-collapse collapse in" role="tabpanel">
													<div class="panel-body pa-15">
													<div class="row">
													<div class="col-md-6">
						<table style="clear: both" class="table table-bordered table-striped" id="user_2">
							<tbody>
								<tr>
									<td style="width:35%">Customer Name</td>
									
									 
									<td style="width:65%"><a href="#" id="CustomerName" data-type="text" data-pk="1" data-title="Customer Name">${(orderHeaderAndjobCardGV.firstName)!}</a></td>
								</tr>
								<tr>
									<td>Mobile Number</td>
									<td><a href="#" id="MobileNumber" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Mobile Number"></a></td>
								</tr>
								<tr>
									<td>Advisor</td>
									<td><a href="#" id="Advisor" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Advisor Name">Advisor Name</a></td>
								</tr>
							</tbody>
						</table>	
					</div>
					<!--/span-->
					<div class="col-md-6">
						<table style="clear: both" class="table table-bordered table-striped" id="user_2">
							<tbody>
								<tr>
									<td style="width:35%">Vehicle Manufacturer</td>
									<td style="width:65%">${(vehicleDetail.manufacturerName)!}</td>
								</tr>
								<tr>
									<td>Vehicle Model</td>
									<td>${(vehicleDetail.modelId)!}</td>
								</tr>
								<tr>
									<td>Services Availed</td>
									<td><a href="#">Car Wash</a>, <a href="#">${(vehicleDetail.searchName!)!}</a></td>
								</tr>
								</tbody>
							</table>
						</div>
				</div>
														
															
															
															</div>
												</div>
											</div>
										</div>
										</form> 
										<!-- add_new_service_div ends-->
										</div>
										
									</div>
								</div>
							</div>
					</div>
					</div>
					<!-- /Row -->
			<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">Service Detail</h6>
								</div>
								
								<#if orderHeaderAndjobCardGV.status?has_content && ("PERFORM_INVOICE_GENE" != orderHeaderAndjobCardGV.status!)>
									<div class="pull-right">
										<button class="btn btn-primary btn-rounded btn-icon left-icon btn-xs" id="addServiceButton"> <i class="fa fa-plus"></i> <span>Add Service</span></button>	
										<button class="btn btn-primary btn-rounded btn-icon left-icon btn-xs" id="promotionButton"> <i class="fa fa-plus"></i> <span>Add Discount</span></button>
									</div>
									<div id="addItemInJobCard" style="display:none;">
									   ${screens.render("component://gaadizo/widget/GaadizoScreens.xml#PerformInvoiceAddItem")}
									</div>
									<div id="promotionDiscount" style="display:none;">
									   ${screens.render("component://gaadizo/widget/GaadizoScreens.xml#AddPromotionInJobCard")}
									</div>
								<#else>
								  <#if picklistBinList?exists && picklistBinList?has_content>
										<div class="pull-right">
											<a  target='_blank' class="dt-button buttons-pdf buttons-html5" tabindex="0" aria-controls="example" href="<@ofbizUrl>createPerformInvoicePDF?jobCardId=${orderHeaderAndjobCardGV.jobCardId!}&orderId=${orderHeaderAndjobCardGV.orderId!}</@ofbizUrl>"><span>PDF</span></a>
										</div>
									</#if>
								</#if>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="datable_1" class="table  display table-hover mb-30">
												<thead>
													<tr>
														<th>Service Item Id</th>
														<th>Item Description</th>
														<th>Service Cost</th>
														<th>Discount %</th>
														<th>Tax</th>
														<th>Total Amount</th>
														<th>Action</th>
													</tr>
												</thead>

												<tbody>
												<#assign allitemSubTotal = 0>
												<#assign allitemTaxTotal = 0>
												<#assign allitemDiscountTotal = 0>
												<#assign allitemTotal = 0>
												<#if jobCardDetails?exists && jobCardDetails?has_content && (orderHeaderAndjobCardGV.orderId?exists && orderHeaderAndjobCardGV.orderId?has_content)>
												<#list jobCardDetails as jobCardDetail>
												    
													<tr>
													<#assign itemTotalOnLine = 0>
														<td>
														
														<#-- <a class="buttontext" href="<@ofbizUrl>GlAccountNavigate?productId=${jobCardDetail.orderItemSeqId!}</@ofbizUrl>" title="Code"> </a> -->
     													 ${jobCardDetail.productId!}
														
														</td>
														<td>${jobCardDetail.itemDescription!}</td>
														<td>
														    <#if jobCardDetail.unitPrice?has_content>
														        <#assign itemTotalOnLine = jobCardDetail.unitPrice.multiply(jobCardDetail.quantity) />
														    <#else>
														        <#assign itemTotalOnLine = 0 />
														    </#if>
														    <#assign allitemSubTotal = allitemSubTotal + itemTotalOnLine >
															<span class="label label-danger"><@ofbizCurrency amount=itemTotalOnLine isoCode="INR"/></span>
														</td>
														<td>
														<#assign totaldiscountOnLine = 0>
														<#list orderAdjustmentList as orderAdjustment>
															<#if "PROMOTION_ADJUSTMENT" == orderAdjustment.orderAdjustmentTypeId! && orderAdjustment.orderItemSeqId == jobCardDetail.orderItemSeqId>
															    <#assign totaldiscountOnLine = totaldiscountOnLine + orderAdjustment.amount>
																<#assign allitemDiscountTotal = allitemDiscountTotal + orderAdjustment.amount>
															</#if>
														</#list>
														<#assign itemTotalOnLine = itemTotalOnLine + totaldiscountOnLine>
														<span class="label label-danger"><@ofbizCurrency amount=totaldiscountOnLine isoCode="INR"/>
														</td>
														<td>
														<#assign totalTaxOnLine = 0>
														<#list orderAdjustmentList as orderAdjustment>
															<#if "SALES_TAX" == orderAdjustment.orderAdjustmentTypeId! && orderAdjustment.orderItemSeqId == jobCardDetail.orderItemSeqId>
																<#assign totalTaxOnLine = totalTaxOnLine + orderAdjustment.amount>
																<#assign allitemTaxTotal = allitemTaxTotal + orderAdjustment.amount>
															</#if>
														</#list>
														<#assign itemTotalOnLine = itemTotalOnLine + totalTaxOnLine>
														<#assign allitemTotal = itemTotalOnLine + allitemTotal>
														<span class="label label-danger"><@ofbizCurrency amount=totalTaxOnLine isoCode="INR"/></span>
														</td>
														<td>
															<@ofbizCurrency amount=itemTotalOnLine isoCode="INR"/>	
														</td>
														<td>
														<form name="updateItemInfo_00001_${jobCardDetail.orderItemSeqId!}" method="post" action="<@ofbizUrl>cancelOrderItemFromPerFormInvoice</@ofbizUrl>">
															<input type="hidden" name="orderId" value="${orderHeaderAndjobCardGV.orderId!}"/>
															<input type="hidden" name="jobCardId" value="${orderHeaderAndjobCardGV.jobCardId!}"/>
															<input type="hidden" name="orderItemSeqId" value="${jobCardDetail.orderItemSeqId!}"/>
            												<input type="hidden" name="shipGroupSeqId" value="00001"/>
            												<input type="hidden" name="purposeFrom" value="cancelItemFromPerformInvoice"/>
                                                             <a href="javascript:document.updateItemInfo_00001_${jobCardDetail.orderItemSeqId!}.submit();" class="btn btn-primary btn-icon-anim btn-circle fa fa-minus">-</a>
														</form>
														
														</td>
														
													</tr>
													
												</#list>
												</#if>
												
													
												</tbody>
												<#if jobCardDetails?exists && jobCardDetails?has_content && (orderHeaderAndjobCardGV.orderId?exists && orderHeaderAndjobCardGV.orderId?has_content)>
												<tfoot>
												<tr class="bg-grey">
                                                  <td class="title"></td>
                                                  <td>User Entered Discount:</td>
                                                  <td></td>
                                                  <td></td> 
                                                  <td></td>
                                                  <td><@ofbizCurrency amount=orh.getOrderAdjustmentsTotal() isoCode="INR"/></td>
                                                  <td></td>
                                                </tr>
												<tr class="bg-grey">
												  <td class="title"></td>
												  <td>Total</td>
												  <td><@ofbizCurrency amount=allitemSubTotal isoCode="INR"/></td>
												  <td><@ofbizCurrency amount=allitemDiscountTotal isoCode="INR"/></td> 
												  <td><@ofbizCurrency amount=allitemTaxTotal isoCode="INR"/></td>
												  <td><@ofbizCurrency amount=orh.getOrderGrandTotal() isoCode="INR"/></td>
												  <td></td>
												</tr>
												
												</tfoot>
												</#if>
											</table>
										</div>
									</div>	
								</div>	
							</div>
						</div>
					</div>
				</div>
				</div>
								</div>
				<!-- /Row -->
				<!-- /Row -->
				<#-- if orderHeaderAndjobCardGV.status?has_content && ("PERFORM_INVOICE_GENE" != orderHeaderAndjobCardGV.status!) -->
				<div class="form-actions mt-10 fright">
                  <form method="post" action="<@ofbizUrl>generatePerFormInvoiceForJobCard</@ofbizUrl>">
                  <input type="hidden" name="facilityId" value="${orderHeaderAndjobCardGV.serviceProviderId!}"/>
                  <input type="hidden" name="orderIdList" value="${orderHeaderAndjobCardGV.orderId!}"/>
                  <input type="hidden" name="jobCardId" value="${orderHeaderAndjobCardGV.jobCardId!}"/>
                    <input type="hidden" size="4" name="maxNumberOfOrders" value="20"/>
                  <input  class="btn btn-success btn-lable-wrap left-label" type="submit" value="Generate Perform Invoice"/>
                </form> 
				</div>
				<#-- </#if> -->

<script language="JavaScript" type="text/javascript">
jQuery(document).ready(function() {
	jQuery('#addServiceButton').click(function(){
		
		jQuery("#addItemInJobCard").toggle();
	});
    jQuery('#promotionButton').click(function(){
		
		jQuery("#promotionDiscount").toggle();
	});
	
});
</script>