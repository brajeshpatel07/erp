		    <div class="page-wrapper">
			<div class="container-fluid pt-25">
			<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
						
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="sortableData" class="table tabledetails display table-hover mb-30">
												<thead>
													<tr>
														<th>Product Id</th>
														<th>Product Type</th>
														<th>Product Category</th>
														<th>Product Type</th>
														<th>Brand Name</th>
														<th>Product Name</th>
														<th>Description</th>
														<th>Qty Uom Id</th>
														<th>Product Weight</th>
														<th>Product Width</th>
														<th>Product Height</th>
														<th>Bar Code</th>
														<th>HSN/SCA Code</th>
														<th>Sales UOM</th>
														<th>Purchase UOM</th>
														<th>Packing Type</th>
														<th>Supplier Id</th>
														<th>Oil Capacity</th>
														<th>Pack Size</th>
													</tr>
												</thead>

												<tbody>
												    <#list productList as product>
													<tr>
														<td>
															<a class="buttontext" href="<@ofbizUrl>modifyProduct?productId=${product.productId!}&isUpdate=Y</@ofbizUrl>" title="Code">
	                                                        	${product.productId!}
	                                                        </a>
														</td>
														<td>${product.productTypeId!}</td>
														<td>${product.primaryProductCategoryId!}</td>
														<td>${product.productType!}</td>
														<td>${product.brandName!}</td>
														<td>${product.productName!}</i></td>
														<td>${product.longDescription!}</td>
														<td>${product.quantityUomId!}</td>
														<td>${product.productWeight!}</td>
														<td>${product.productWidth!}</td>
														<td>${product.productHeight!}</td>
														<td>${product.barcode!}</td>
														<td>${product.hsnScaCode!}</td>
														<td>${product.salesUOM!}</td>
														<td>${product.purchaseUOM!}</td>
														<td>${product.packingType!}</td>
														<td>${product.supplierId!}</td>
														<td>${product.oilCapacity!}</td>
														<td>${product.packSize!}</td>
													</tr>
													</#list>
													
												</tbody>
											</table>
										</div>
									</div>	
								</div>	
							</div>
						</div>
					</div>
				</div>
				</div>
								</div>
				<!-- /Row -->

