<div role="tabpanel" class="box box-primary" id="invoices">
<div class="">
    <div>
      <div class="box-header with-border">
           <h3 class="box-title">Find Supplier</h3>
       </div>      
        <div class="box-body">
            <form class="form-horizontal formCenter" method="post" id="findSupplierForm" name="findSupplier" onsubmit="javascript:submitFormDisableSubmits(this)" action="<@ofbizUrl>findSupplierList</@ofbizUrl>">
                
                
                <div class="row no-margin">
                  
		     
		        <div id="collapseOne" class="search-cntr">
				 
				    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Supplier Id</label>
                            <div class="col-sm-8 input-group">
                                <input id="supplierId" type="text" size="25" name="supplierId" class="form-control getid-val" value="${parameters.supplierId!}">
                            </div>
                        </div>
                    </div>
		            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		                <div class="form-group">
		                    <label for="inputPassword3" class="col-sm-4 control-label">Supplier Name</label>
		                    <div class="col-sm-8 input-group">
		                        <input class="form-control getid-valu" type="text" id="supplierName" name="supplierName" maxlength="40" value="${parameters.supplierName!""}"/>
		                    </div>
		                </div>
		            </div>		            
		        </div>
		        </br>
		        <div class="col-lg- col-md-6 col-sm-6 col-xs-12 pull-right">
                  <input type="submit" class="btn btn-success pull-right" name="searchButton" value="${uiLabelMap.Find}">
               </div>
		        
    		</form>
           
        </div>
    </div>
</div>
</div>
