		    <div class="page-wrapper">
			<div class="container-fluid pt-25">
			<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
						
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="sortableData" class="table tabledetails display table-hover mb-30">
												<thead>
													<tr>
														<th>Company Id</th>
														<th>Company Name</th>
														<th>Company Image Url</th>
													</tr>
												</thead>

												<tbody>
												    <#list customersList as company>
													<tr>
														<td>
															<a class="buttontext" href="<@ofbizUrl>companyDetail?companyId=${company.partyId!}&isUpdate=Y</@ofbizUrl>" title="Code">
	                                                        	${company.partyId!}
	                                                        </a>
	                                                        <a class="buttontext" href="<@ofbizUrl>customerDetail?companyId=${company.partyId!}&isUpdate=Y</@ofbizUrl>" title="Code">
	                                                        	${company.partyId!}
	                                                        </a>
														</td>
														
														<td>${company.groupName!}</td>
														<td>${company.partyId!}</td>
													</tr>
													</#list>
													
												</tbody>
											</table>
										</div>
									</div>	
								</div>	
							</div>
						</div>
					</div>
				</div>
				</div>
								</div>
				<!-- /Row -->

