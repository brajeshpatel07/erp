		    <div class="page-wrapper">
			<div class="container-fluid pt-25">
			<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
						
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="sortableData" class="table tabledetails display table-hover mb-30">
												<thead>
													<tr>
														<th>Product Id</th>
														<th>Vehicle Model</th>
														<th>Product Description</th>
														<th>From Date</th>
														<th>Thru Date</th>
														<th>Product Store Id</th>
														<th>Facility</th>
														<th>Price</th>
														<th>Discount</th>
														<th>Workshop Price</th>
														<th>Tax Percentage</th>
													</tr>
												</thead>

												<tbody>
												    <#list pvPriceList as pvPrice>
													<tr>
														<td>
															<a class="buttontext" href="<@ofbizUrl>modifyPVPrice?productId=${pvPrice.productId!}&isUpdate=Y</@ofbizUrl>" title="Code">
	                                                        	${pvPrice.productId!}
	                                                        </a>
														</td>
														<td>${pvPrice.vehicleId!}</td>
														<td>${pvPrice.description!}</td>
														<td>${pvPrice.fromDate!}</td>
														<td>${pvPrice.thruDate!}</td>
														<td>${pvPrice.productStoreId!}</td>
														<td>${pvPrice.facilityId!}</td>
														<td><@ofbizCurrency amount=pvPrice.price isoCode="INR"/></td>
														<td><@ofbizCurrency amount=pvPrice.discount isoCode="INR"/></td>
														<td><@ofbizCurrency amount=pvPrice.workshopPrice isoCode="INR"/></td>
														<td><@ofbizCurrency amount=pvPrice.taxPercentage isoCode="INR"/></td>
													</tr>
													</#list>
													
												</tbody>
											</table>
										</div>
									</div>	
								</div>	
							</div>
						</div>
					</div>
				</div>
				</div>
								</div>
				<!-- /Row -->

