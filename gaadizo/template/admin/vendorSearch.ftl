<div role="tabpanel" class="box box-primary" id="invoices">
<div class="">
    <div>
      <div class="box-header with-border">
           <h3 class="box-title">Find Vendor</h3>
       </div>      
        <div class="box-body">
            <form class="form-horizontal formCenter" method="post" id="findVendorForm" name="findVendor" onsubmit="javascript:submitFormDisableSubmits(this)" action="<@ofbizUrl>findVendorList</@ofbizUrl>">
                
                
                <div class="row no-margin">
                  
		     
		        <div id="collapseOne" class="search-cntr">
				 
				    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Vendor Id</label>
                            <div class="col-sm-8 input-group">
                                <input id="vendorId" type="text" size="25" name="vendorId" class="form-control getid-val" value="${parameters.vendorId!}">
                            </div>
                        </div>
                    </div>
		            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		                <div class="form-group">
		                    <label for="inputPassword3" class="col-sm-4 control-label">Vendor Name</label>
		                    <div class="col-sm-8 input-group">
		                        <input class="form-control getid-valu" type="text" id="vendorName" name="vendorName" maxlength="40" value="${parameters.vendorName!""}"/>
		                    </div>
		                </div>
		            </div>		            
		        </div>
		        </br>
		        <div class="col-lg- col-md-6 col-sm-6 col-xs-12 pull-right">
                  <input type="submit" class="btn btn-success pull-right" name="searchButton" value="${uiLabelMap.Find}">
               </div>
		        
    		</form>
           
        </div>
    </div>
</div>
</div>
