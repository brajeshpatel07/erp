			<!-- Main Content -->
	    <div class="page-wrapper">
			<div class="container-fluid">
					<!-- Title -->
					<div class="row heading-bg">
						<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
							<h5 class="txt-dark">Customer Details</h5>
						</div>
					
						<!-- Breadcrumb -->
						<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
							<ol class="breadcrumb">
								<li><a href="<@ofbizUrl>main</@ofbizUrl>">Dashboard</a></li>
								<#-- li><a href="#"><span>form</span></a></li>
								<li class="active"><span>form-layout</span></li -->
							</ol>
						</div>
						<!-- /Breadcrumb -->
					
					</div>
					<!-- /Title -->
					
					<!-- Row -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h4 class="txt-dark">Customer Number : <code>#<#if partyAndPersonGV?exists && partyAndPersonGV?has_content && partyAndPersonGV.userLoginId?has_content>${partyAndPersonGV.userLoginId!}</#if></code></h4>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<div class="panel-group accordion-struct accordion-style-1" id="accordion_2" role="tablist" aria-multiselectable="true">
											<div class="panel panel-default">
												<div class="panel-heading activestate" role="tab" id="heading_10">
													<h3 class="txt-dark capitalize-font">Customer Details</h3>
												</div>
												<div id="collapse_10" class="panel-collapse collapse in" role="tabpanel">
													<div class="panel-body pa-15">
														<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		<label class="control-label mb-10">Customer Id</label>
																		<input type="text" id="CustomerName" class="form-control" placeholder="Enter Name" value="${(partyAndPersonGV.firstName)!} ${(partyAndPersonGV.lastName)!}">
																		<span class="help-block"> This is inline help </span> 
																	</div>
																</div>
																<!--/span-->
																<div class="col-md-6">
																	<div class="form-group has-error">
																		<label class="control-label mb-10">Email</label>
																		<input type="email" id="Email" class="form-control" placeholder="Enter Email" value="<#if emailPCDPGV?has_content>${emailPCDPGV.infoString!}</#if>">
																		<span class="help-block"> This field has error. </span> 
																	</div>
																</div>
																<!--/span-->
														</div>
														<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		<label class="control-label mb-10">Customer Name</label>
																		<input type="text" id="CustomerName" class="form-control" placeholder="Enter Name" value="<#if partyAndPersonGV?has_content>${(partyAndPersonGV.firstName)!} ${(partyAndPersonGV.lastName)!}</#if>">
																		<span class="help-block"> This is inline help </span> 
																	</div>
																</div>
																<!--/span-->
																<div class="col-md-6">
																	<div class="form-group">
																		<label class="control-label mb-10">Created Date & Time</label>
																		<input type="createdDateTime" id="createdDateTime" class="form-control" placeholder="Created Date & Time" value="<#if orderHeaderAndJobCardGV?has_content>${orderHeaderAndJobCardGV.serviceDate!}</#if>">
																		<span class="help-block"> This field has error. </span> 
																	</div>
																</div>
																<!--/span-->
														</div>
														
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label mb-10">Mobile Number</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if phonePCDPGV?has_content>${phonePCDPGV.infoString!}</#if>">
																</div>
															</div>
															<!--/span-->
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label mb-10">Customer Group</label>
																	<input type="text" id="customerGroup" class="form-control" placeholder="Enter Customer Group" <#if partyAddress?has_content>value="${partyAddress!}"</#if>> 
																</div>
															</div>
															<!--/span-->
														</div>
														
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label mb-10">Vehicle Model</label>
																	<input type="text" name="vehicleModel" id="vehicleModel" class="form-control" data-mask="999-999-9999" placeholder="Enter Vehicle Model" value="<#if orderHeaderAndJobCardGV?has_content>${orderHeaderAndJobCardGV.vehicleModel!}</#if>">
																</div>
															</div>
															<!--/span-->
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label mb-10">Reg.\Vehilce no.</label>
																	<#--<input type="text" id="Address" class="form-control" placeholder="Enter Address" <#if partyAndPostalAddressGV.address1?has_content>value="${partyAndPostalAddressGV.address1!}"</#if>>-->
																	<input type="text" name="vehilceRegistationNumber" id="vehilceRegistationNumber" class="form-control" placeholder="Enter Vehilce Registation Number" value="<#if orderHeaderAndJobCardGV?has_content>${orderHeaderAndJobCardGV.registationNumber!}</#if>"> 
																</div>
															</div>
															<!--/span-->
														</div>
														
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label mb-10">DOB</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if orderHeaderAndJobCardGV?has_content>${orderHeaderAndJobCardGV.serviceDate!}</#if>">
																</div>
															</div>
															<!--/span-->
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label mb-10">Variant</label>
																	<input type="text" id="customerGroup" class="form-control" placeholder="Enter Customer Group" value="<#if lastVehicleGV?has_content>${lastVehicleGV.variant!}</#if>"> 
																</div>
															</div>
															<!--/span-->
														</div>
														
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label mb-10">Annv. Date</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if orderHeaderAndJobCardGV?has_content>${orderHeaderAndJobCardGV.serviceDate!}</#if>">
																</div>
															</div>
															<!--/span-->
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label mb-10">Manufacturing Year</label>
																	<input type="text" id="customerGroup" class="form-control" placeholder="Enter Customer Group" value="<#if lastVehicleGV?has_content>${lastVehicleGV.year!}</#if>"> 
																</div>
															</div>
															<!--/span-->
														</div>
													</div>
												</div>
											</div>
											
											<#-- Addrsss Details Starts-->
											<div class="panel panel-default">
												<div class="panel-heading activestate" role="tab" id="heading_11">
													<h3 class="txt-dark capitalize-font">Addrsss Details</h3>
												</div>
												<div id="collapse_12" class="panel-collapse collapse in" role="tabpanel">
													<#-- Bill TO Address Section Starts-->
													<h4><label class="control-label mb-10">Bill To Address</label></h4>
													<div class="panel-body pa-15"> 
															<div class="row">
																<div class="col-md-4 ">
																	<div class="form-group">
																	<label class="control-label mb-10">Builiding No.</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if billingPCDPGV?has_content>${billingPCDPGV.address1!}</#if>">
																	</div>	
																</div>
																<div class="col-md-4 ">
																	<div class="form-group">
																	<label class="control-label mb-10">Locality</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if billingPCDPGV?has_content>${billingPCDPGV.address2!}</#if>">
																	</div>	
																</div>
																<div class="col-md-4 ">
																	<div class="form-group">
																	<label class="control-label mb-10">Landmark</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if billingPCDPGV?has_content>${billingPCDPGV.directions!}</#if>">
																	</div>	
																</div>
															</div>
														<!-- /Row -->
															<div class="row">
																<div class="col-md-4 ">
																	<div class="form-group">
																	<label class="control-label mb-10">Address Type</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if billingPCDPGV?has_content && billingPCDPGV.contactMechPurposeTypeId?has_content && billingPCDPGV.contactMechPurposeTypeId.equals("BILLING_LOCATION")>${"Home"!}</#if>">
																	</div>	
																</div>
																<div class="col-md-4 ">
																	<div class="form-group">
																	<label class="control-label mb-10">City</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if billingPCDPGV?has_content>${billingPCDPGV.city!}</#if>">
																	</div>	
																</div>
																<div class="col-md-4 ">
																	<div class="form-group">
																	<label class="control-label mb-10">State</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if billingPCDPGV?has_content>${billingPCDPGV.stateGeoName!}</#if>">
																	</div>	
																</div>
															</div>
														<!-- /Row -->
															<div class="row">
																<div class="col-md-4 ">
																	<div class="form-group">
																	<label class="control-label mb-10">Country</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if billingPCDPGV?has_content>${billingPCDPGV.countryGeoName!}</#if>">
																	</div>	
																</div>
																<div class="col-md-4 ">
																	<div class="form-group">
																	</div>	
																</div>
																<div class="col-md-4 ">
																	<div class="form-group">
																	</div>	
																</div>
															</div>
														<!-- /Row -->
													</div>
												</div>
												<#-- Ship To Address Section Ends-->
												<#-- Ship To Address Section Starts-->
												<div class="panel-heading activestate" role="tab" id="heading_11">
													<h3 class="txt-dark capitalize-font"></h3>
												</div>
												<div id="collapse_12" class="panel-collapse collapse in" role="tabpanel">
													<#-- Bill TO Address Section Starts-->
													<h4><label class="control-label mb-10">Ship To Address</label></h4>
													<div class="panel-body pa-15"> 
															<div class="row">
																<div class="col-md-4 ">
																	<div class="form-group">
																	<label class="control-label mb-10">Builiding No.</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if shippingPCDPGV?has_content>${shippingPCDPGV.address1!}</#if>">
																	</div>	
																</div>
																<div class="col-md-4 ">
																	<div class="form-group">
																	<label class="control-label mb-10">Locality</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if shippingPCDPGV?has_content>${shippingPCDPGV.address2!}</#if>">
																	</div>	
																</div>
																<div class="col-md-4 ">
																	<div class="form-group">
																	<label class="control-label mb-10">Landmark</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if shippingPCDPGV?has_content>${shippingPCDPGV.directions!}</#if>">
																	</div>	
																</div>
															</div>
														<!-- /Row -->
															<div class="row">
																<div class="col-md-4 ">
																	<div class="form-group">
																	<label class="control-label mb-10">Address Type</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if shippingPCDPGV?has_content && shippingPCDPGV.contactMechPurposeTypeId?has_content && shippingPCDPGV.contactMechPurposeTypeId.equals("SHIPPING_LOCATION")>${"Home"!}</#if>">
																	</div>	
																</div>
																<div class="col-md-4 ">
																	<div class="form-group">
																	<label class="control-label mb-10">City</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if shippingPCDPGV?has_content>${shippingPCDPGV.city!}</#if>">
																	</div>	
																</div>
																<div class="col-md-4 ">
																	<div class="form-group">
																	<label class="control-label mb-10">State</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if shippingPCDPGV?has_content>${shippingPCDPGV.stateGeoName!}</#if>">
																	</div>	
																</div>
															</div>
														<!-- /Row -->
															<div class="row">
																<div class="col-md-4 ">
																	<div class="form-group">
																	<label class="control-label mb-10">Country</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if shippingPCDPGV?has_content>${shippingPCDPGV.countryGeoName!}</#if>">
																	</div>	
																</div>
																<div class="col-md-4 ">
																	<div class="form-group">
																		<label class="control-label mb-10">Insurance Company</label>
																		<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="${insuranceCompany!""}">
																	</div>	
																</div>
																<div class="col-md-4 ">
																	<div class="form-group">
																		<label class="control-label mb-10">Payment mode</label>
																		<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="${paymentMode!""}">
																	</div>	
																</div>
															</div>
														<!-- /Row -->
														<div class="row">
																<div class="col-md-4 ">
																	<div class="form-group">
																	<label class="control-label mb-10">Previous Service Date</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if orderHeaderAndJobCardGV?has_content>${orderHeaderAndJobCardGV.serviceDate!}</#if>">
																	</div>	
																</div>
																<div class="col-md-4 ">
																	<div class="form-group">
																		<label class="control-label mb-10">Insurance Expiry Date</label>
																		<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if orderHeaderAndJobCardGV?has_content>${orderHeaderAndJobCardGV.serviceDate!}</#if>">
																	</div>	
																</div>
																<div class="col-md-4 ">
																	<div class="form-group">
																		<label class="control-label mb-10">Customer Type</label>
																		<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="${customerType!}">
																	</div>	
																</div>
															</div>
														<!-- /Row -->
														<div class="row">
																<div class="col-md-4 ">
																	<div class="form-group">
																	<label class="control-label mb-10">Registered By</label>
																	<input type="tel" id="MobileNumber" class="form-control" data-mask="999-999-9999" placeholder="Enter Mobile" value="<#if partyAndPersonGV?has_content>${partyAndPersonGV.createdByUserLogin!}</#if>">
																	</div>	
																</div>
																<div class="col-md-4 ">
																	<div class="form-group">
																	</div>	
																</div>
																<div class="col-md-4 ">
																	<div class="form-group">
																	</div>	
																</div>
															</div>
														<!-- /Row -->
													</div>
												</div>
												<#-- Ship To Address Section Ends-->
											</div> 
											<#-- Address Details Section Ends-->	
										</div>
									</div>
								</div>
							</div>
				<!-- /Row -->
				
					</div>
					</div>
					<!-- /Row -->
			<!-- Row -->
			
				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">Vehicle Details</h6>
								</div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="datable_1" class="table  display table-hover mb-30">
												<thead>
													<tr>
														<th>Sr No.</th>
														<th>Vehicle Model</th>
														<th>Last Service Date</th>
														<th>Last Service</th>
														<th>Invoice Amount</th>
														<th>Recommendations</th>
														<th>Mileage</th>
														<th>Service Due Date</th>
														<th>Manufacturing Year</th>
														<th>Invoice</th>
													</tr>
												</thead>

												<tbody>
												<#list vehicleList as vehicle>
													<tr>
														<#assign srCount = 0>
														<td>${srCount!}</td>
														<#assign srCount = srCount+1>
														<td>${vehicle.modelId!}</td>
														<td></td>
														<td></td>
														<td></td>
														<td>${vehicle.searchName!}</td>
														<td>${vehicle.power!}</td>
														<td></td>
														<td>${vehicle.year!}</td>
														<td>
														<form name="updateItemInfo_00001_${vehicle.modelId!}" method="post" action="<@ofbizUrl>updateVehicle</@ofbizUrl>">
															<input type="hidden" name="vehicleModelId" value="${vehicle.modelId!}"/>
                                                            <a href="javascript:document.updateItemInfo_00001_${vehicle.modelId!}.submit();" class="btn btn-primary btn-icon-anim btn-circle fa fa-minus">-</a>
														</form>
														</td>
														
													</tr>
													
												</#list>
												
													
												</tbody>
												<tfoot>
												</tfoot>
											</table>
										</div>
									</div>	
								</div>	
							</div>
						</div>
					</div>
				</div>
				</div>
								</div>
				<!-- /Row -->
				<!-- /Row -->
				<div class="form-actions mt-10 fright">
					<button type="button" class="btn btn-default">Cancel</button> 
					<#if orderHeaderAndjobCardGV?exists && orderHeaderAndjobCardGV.orderId?has_content>
					    <button class="btn btn-success btn-lable-wrap left-label"> <span class="btn-label"><i class="fa fa-check"></i> </span><span class="btn-text">Update Customer</span></button>
					<#else>
					<button class="btn btn-success btn-lable-wrap left-label"> <span class="btn-label"><i class="fa fa-check"></i> </span><span class="btn-text">Create Customer</span></button>
				</#if>	  	
				</div>

<script language="JavaScript" type="text/javascript">
jQuery(document).ready(function() {
	jQuery('#addServiceButton').click(function(){
		
		jQuery("#addItemInJobCard").toggle();
	});
});
</script>