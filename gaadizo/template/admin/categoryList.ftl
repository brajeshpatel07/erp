		    <div class="page-wrapper">
			<div class="container-fluid pt-25">
			<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
						
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="sortableData" class="table tabledetails  display table-hover mb-30">
												<thead>
													<tr>
														<th>Category Id</th>
														<th>Category Type</th>
														<th>Category Name</th>
														<th>Parent Category Id</th>
														<th>Description</th>
														<th>Category Image Url</th>
													</tr>
												</thead>

												<tbody>
												    <#list categoryList as category>
													<tr>
														<td>
															<a class="buttontext" href="<@ofbizUrl>modifyCategory?productCategoryId=${category.productCategoryId!}&isUpdate=Y</@ofbizUrl>" title="Code">
	                                                        	${category.productCategoryId!}
	                                                        </a>
														</td>
														<td>${category.productCategoryTypeId!}</td>
														<td>${category.categoryName!}</td>
														<td>${category.primaryParentCategoryId!}</td>
														<td>${category.longDescription!}</td>
														<td>${category.categoryImageUrl!}</td>
													</tr>
													</#list>
													
												</tbody>
											</table>
										</div>
									</div>	
								</div>	
							</div>
						</div>
					</div>
				</div>
				</div>
								</div>
				<!-- /Row -->

