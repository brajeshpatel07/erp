<div role="tabpanel" class="box box-primary" id="invoices">
<div class="">
    <div>
      <div class="box-header with-border">
           <h3 class="box-title">Find Category</h3>
       </div>      
        <div class="box-body">
            <form class="form-horizontal formCenter" method="post" id="findCategoryForm" name="findCategory" onsubmit="javascript:submitFormDisableSubmits(this)" action="<@ofbizUrl>findCategoryList</@ofbizUrl>">
                
                
                <div class="row no-margin">
                  
		     
		        <div id="collapseOne" class="search-cntr">
				 
				    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Category Id</label>
                            <div class="col-sm-8 input-group">
                                <input id="categoryId" type="text" size="25" name="categoryId" class="form-control getid-val" value="${parameters.categoryId!}">
                            </div>
                        </div>
                    </div>
		            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		                <div class="form-group">
		                    <label for="inputPassword3" class="col-sm-4 control-label">Category Name</label>
		                    <div class="col-sm-8 input-group">
		                        <input class="form-control getid-valu" type="text" id="categoryName" name="categoryName" maxlength="40" value="${parameters.categoryName!""}"/>
		                    </div>
		                </div>
		            </div>		            
		        </div>
		        </br>
		        <div class="col-lg- col-md-6 col-sm-6 col-xs-12 pull-right">
                  <input type="submit" class="btn btn-success pull-right" name="searchButton" value="${uiLabelMap.Find}">
               </div>
		        
    		</form>
           
        </div>
    </div>
</div>
</div>

		

	<script type="text/javascript">
             jQuery(document).ready(function() 
    { 
        jQuery("#datepicker13").datepicker({ dateFormat: 'yy-mm-dd' });
        jQuery("#datepicker14").datepicker({ dateFormat: 'yy-mm-dd' });
        jQuery("#datepicker2").datepicker({ dateFormat: 'yy-mm-dd' });
        jQuery("#datepicker3").datepicker({ dateFormat: 'yy-mm-dd' });
    
    }); 
        </script>		
