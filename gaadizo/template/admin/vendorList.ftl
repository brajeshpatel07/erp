		    <div class="page-wrapper">
			<div class="container-fluid pt-25">
			<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
						
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="sortableData" class="table tabledetails display table-hover mb-30">
												<thead>
													<tr>
														<th>Vendor Id</th>
														<th>Vendor Name</th>
														<th>Social Security Number</th>
														<th>Vendor Email</th>
													</tr>
												</thead>

												<tbody>
												    <#list partyAndPersons as partyAndPerson>
													<tr>
														<td>
															<a class="buttontext" href="<@ofbizUrl>modifyVendor?partyId=${partyAndPerson.partyId!}&isUpdate=Y</@ofbizUrl>" title="Code">
	                                                        	${partyAndPerson.partyId!}
	                                                        </a>
														</td>
														<td>${partyAndPerson.firstName!} ${partyAndPerson.lastName!}</td>
														<td>${partyAndPerson.socialSecurityNumber!}</td>
														<td></td>
													</tr>
													</#list>
													
												</tbody>
											</table>
										</div>
									</div>	
								</div>	
							</div>
						</div>
					</div>
				</div>
				</div>
								</div>
				<!-- /Row -->

