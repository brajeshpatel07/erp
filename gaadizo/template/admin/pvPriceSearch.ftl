<div role="tabpanel" class="box box-primary" id="invoices">
<div class="">
    <div>
      <div class="box-header with-border">
           <h3 class="box-title">Find Product Vehicle Price</h3>
       </div>      
        <div class="box-body">
            <form class="form-horizontal formCenter" method="post" id="findPVPriceForm" name="findPVPrice" onsubmit="javascript:submitFormDisableSubmits(this)" action="<@ofbizUrl>findPVPriceList</@ofbizUrl>">
                
                
                <input type="hidden" name="noConditionFind" value="Y" id="FindInvoices_noConditionFind">
                <input type="hidden" name="hideSearch" value="Y" id="FindInvoices_hideSearch">
                <div class="row no-margin">
                  
		     
		        <div id="collapseOne" class="search-cntr">
				 
				    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Product Id</label>
                            <div class="col-sm-8 input-group">
                                <input id="productId" type="text" size="25" name="productId" class="form-control getid-val" value="${parameters.productId!}">
                            </div>
                        </div>
                    </div>
		            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		                <div class="form-group">
		                    <label for="inputPassword3" class="col-sm-4 control-label">Product Description</label>
		                    <div class="col-sm-8 input-group">
		                        <input class="form-control getid-valu" type="text" id="productName" name="productName" maxlength="40" value="${parameters.productName!""}"/>
		                    </div>
		                </div>
		            </div>		            
		        </div>
		        </br>
		        <div class="col-lg- col-md-6 col-sm-6 col-xs-12 pull-right">
                  <input type="submit" class="btn btn-success pull-right" name="searchButton" value="${uiLabelMap.Find}">
               </div>
		        
    		</form>
           
        </div>
    </div>
</div>
</div>

		

	<script type="text/javascript">
             jQuery(document).ready(function() 
    { 
        jQuery("#datepicker13").datepicker({ dateFormat: 'yy-mm-dd' });
        jQuery("#datepicker14").datepicker({ dateFormat: 'yy-mm-dd' });
        jQuery("#datepicker2").datepicker({ dateFormat: 'yy-mm-dd' });
        jQuery("#datepicker3").datepicker({ dateFormat: 'yy-mm-dd' });
    
    }); 
        </script>		
