		    <div class="page-wrapper">
			<div class="container-fluid pt-25">
			<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
						
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="sortableData" class="table tabledetails display table-hover mb-30">
												<thead>
													<tr>
														<th>Vehicle Id</th>
														<th>Model Id</th>
														<th>Manufacturer Id</th>
														<th>Manufacturer Code</th>
														<th>Category Id</th>
														<th>Model Name</th>
														<th>Manufacturer Name</th>
														<th>Search Name</th>
														<th>Year</th>
														<th>Variant</th>
														<th>Power</th>
														<th>Type</th>
														<th>Fuel Type</th>
														<th>Oil Capacity</th>
													</tr>
												</thead>

												<tbody>
												    <#list vehicleList as vehicle>
													<tr>
														<td>
															<a class="buttontext" href="<@ofbizUrl>modifyVehicle?vehicleId=${vehicle.vehicleId!}&isUpdate=Y</@ofbizUrl>" title="Code">
	                                                        	${vehicle.vehicleId!}
	                                                        </a>
														</td>
														<td>${vehicle.modelId!}</td>
														<td>${vehicle.manufacturerId!}</td>
														<td>${vehicle.manufacturerCode!}</td>
														<td>${vehicle.categoryId!}</td>
														<td>${vehicle.modelName!}</td>
														<td>${vehicle.manufacturerName!}</td>
														<td>${vehicle.searchName!}</td>
														<td>${vehicle.year!}</i></td>
														<td>${vehicle.variant!}</td>
														<td>${vehicle.power!}</td>
														<td>${vehicle.type!}</td>
														<td>${vehicle.fuelType!}</td>
														<td>${vehicle.oilCapacity!}</td>
													</tr>
													</#list>
													
												</tbody>
											</table>
										</div>
									</div>	
								</div>	
							</div>
						</div>
					</div>
				</div>
				</div>
								</div>
				<!-- /Row -->

