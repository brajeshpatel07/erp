<div class="col-md-9 col-sm-8">
<div class="rightform">
<div class="title2"><span>Customer Master</span></div>

<div class="paddingbott20">
<div id="accordion1" class="panel-group accordion">
  <div class="panel">
    <div class="panel-title"><a data-parent="#accordion1" data-toggle="collapse" href="#accordion11" class="" aria-expanded="true"><h3>Customer Registration</h3></a></div>
    <div class="paddingbott10">
    <div id="accordion11" class="panel-collapse collapse in" role="tablist" aria-expanded="true">
      <div class="panel-content">
        <form action="" method="get">

<div class="row form-group">
<div class="col-sm-4">
<label>Customer Id</label>
<input type="text" name="customerId" value="${partyAndPersonGV.partyId!}" class="form-control" placeholder="" readonly>
</div>

<div class="col-sm-4">
<label>Created Date & Time</label>
<div class="row">
<div class="col-sm-8 col-xs-8 paddrightnone">
<div class="input-group">
<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
<input class="form-control" id="date" name="date" value="${partyAndPersonGV.createdDate!}" placeholder="MM/DD/YYYY" type="text" readonly>
</div>
</div>


</div>
</div>

<div class="col-sm-4">
<label>Customer Group</label>
	<select class="form-control"><option>Gold</option></select>
	</div>

<div class="col-sm-4">
<label>Customer Name</label>
<input type="text" name=""  readonly value="<#if partyAndPersonGV.partyTypeId?has_content && ("PERSON" == partyAndPersonGV.partyTypeId)>${partyAndPersonGV.firstName!} ${partyAndPersonGV.lastName!}<#else>${partyAndPersonGV.groupName!}</#if>" class="form-control" placeholder="">
</div>
 
<div class="col-sm-4">
<label>Mobile No.</label>      
<input type="text" name=""  readonly class="form-control" value="${shippingPCDPGV.contactNumber!""}" placeholder="">
</div>
<#-- 
<div class="col-sm-4">
<label>Vehicle Model</label>      
<input type="text" name=""  class="form-control" placeholder="">
</div>

<div class="col-sm-4">
<label>Reg.\Vehilce no.</label>
<input type="text" name="" class="form-control" placeholder="">
</div> -->
 
<div class="col-sm-4">
<label>Email</label>      
<input type="text" name=""  readonly class="form-control" value="${(emailPCDPGV.infoString)!""}"   placeholder="">
</div>
<#--
<div class="col-sm-4">
<label>Variant</label>      
<input type="text" name="" class="form-control" placeholder="">
</div>
-->
<#if partyAndPersonGV.partyTypeId?has_content && ("PERSON" == partyAndPersonGV.partyTypeId)>
<div class="col-sm-4">
<label>DOB</label>
<div class="input-group">
<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
<input class="form-control" id="date" name="date"  readonly placeholder="MM/DD/YYYY" type="text"/>
</div>
</div>

 
<div class="col-sm-4">
<label>Annv. Date</label>      
<div class="input-group">
<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
<input class="form-control" id="date" name="date"  readonly placeholder="MM/DD/YYYY" type="text"/>
</div>
</div>
</#if>
<#--
<div class="col-sm-4">
<label>Manufacturing Year</label>      
<select class="form-control"><option>Manufacturing Year</option></select>
</div>
-->
<div class="col-sm-8">
<label>Address</label>      
<input class="form-control" placeholder=""  readonly value="${shippingPCDPGV.address1!""} ${shippingPCDPGV.city!""}" type="text"/>
</div>

</div>

</form>
      </div>
    </div>
    </div>
  </div>
  
  <div class="panel">
    <div class="panel-title"><a class="collapsed" data-parent="#accordion1" data-toggle="collapse" href="#accordion12" aria-expanded="false"><h3> Bill to Address</h3></a></div>
    <div class="paddingbott10">
    <div id="accordion12" class="panel-collapse collapse" role="tablist" aria-expanded="false">
      <div class="panel-content">
        <form action="" method="get">

<div class="row form-group">
<div class="col-md-3 col-sm-4">
<label>Builiding No.</label>
<input type="text" name="" value="<#if addressMap.address1?exists && addressMap.address1?has_content>${addressMap.address1!}</#if>" class="form-control" placeholder="E-995">
</div>

<div class="col-md-3 col-sm-4">
<label>Locality</label>
<input type="text" name="" class="form-control" value="<#if addressMap.address2?exists && addressMap.address2?has_content>${addressMap.address2!}</#if>" placeholder="Balaji Apartmentrs">
</div>

<div class="col-md-3 col-sm-4">
<label>Landmark</label>
<input type="text" name="" class="form-control" value="<#if addressMap.address3?exists && addressMap.address3?has_content>${addressMap.address3!}</#if>" placeholder="Opposite Country Inn">
</div>

<div class="col-md-3 col-sm-4">
<label>Address Type</label>
<input type="text" name="" class="form-control" value="<#if addressMap.address4?exists && addressMap.address4?has_content>${addressMap.address4!}</#if>" placeholder="Home">
</div>
 
<div class="col-md-3 col-sm-4">
<label>City</label>
<input type="text" name="" class="form-control" value="<#if addressMap.address5?exists && addressMap.address5?has_content>${addressMap.address5!}</#if>" placeholder="Gurugram">
</div>

<div class="col-md-3 col-sm-4">
<label>State</label>
<input type="text" name="" class="form-control" value="<#if addressMap.address1?exists && addressMap.address1?has_content>${shippingPCDPGV.city!}</#if>" placeholder="Haryana">
</div>
 

</div>

</form>
      </div>
    </div>
    </div>
  </div>
  
  <#-- div class="panel">
    <div class="panel-title"><a class="collapsed" data-parent="#accordion1" data-toggle="collapse" href="#accordion13" aria-expanded="false"><h3> Ship to Address</h3></a></div>
    <div id="accordion13" class="panel-collapse collapse" role="tablist" aria-expanded="false">
      <div class="panel-content">
        <form action="" method="get">

<div class="row form-group">
<div class="col-md-3 col-sm-4">
<label>Builiding No.</label>
<input type="text" name="" class="form-control" placeholder="E-995">
</div>

<div class="col-md-3 col-sm-4">
<label>Locality</label>
<input type="text" name="" class="form-control" placeholder="Balaji Apartmentrs">
</div>

<div class="col-md-3 col-sm-4">
<label>Landmark</label>
<input type="text" name="" class="form-control" placeholder="Opposite Country Inn">
</div>

<div class="col-md-3 col-sm-4">
<label>Address Type</label>
<input type="text" name="" class="form-control" placeholder="Home">
</div>
 
<div class="col-md-3 col-sm-4">
<label>City</label>
<input type="text" name="" class="form-control" placeholder="Gurugram">
</div>

<div class="col-md-3 col-sm-4">
<label>State</label>
<input type="text" name="" class="form-control" placeholder="Haryana">
</div>

<div class="col-md-3 col-sm-4">
<label>Insurance Company</label>
<select class="form-control"><option>Insurance Company Name</option></select>
</div>

<div class="col-md-3 col-sm-4">
<label>Payment Mode</label>
<select class="form-control"><option>Payment Mode</option></select>
</div>
 
<div class="col-md-3 col-sm-4">
<label>Previous Service Date</label>
<div class="input-group">
<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
<input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/>
</div>
</div>

<div class="col-md-3 col-sm-4">
<label>Insurance Expiry Date</label>
<div class="input-group">
<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
<input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/>
</div>
</div>

<div class="col-md-3 col-sm-4">
<label>Customer Type</label>
<select class="form-control"><option>Insurance Company Name</option></select>
</div>

<div class="col-md-3 col-sm-4">
<label>Registered By</label>
<select class="form-control"><option>Payment Mode</option></select>
</div>

 

</div>

</form>
      </div>
    </div>
  </div -->
  
</div>


</div>

 

 

 


</div>
</div>



</div>
<div class="col-md-9 col-sm-8">
<div class="rightform">
<div class="title2"><span>Job Cards History</span></div>

<div class="table-responsive margintop20">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabledetails">
  <tr>
    <th>Srl No.</th>
    <th>Vehicle Model</th>
    <th>Last Service Date</th>
    <th>Last Service</th>
    <th>Invoice Amount</th>
    <th>Recommendations</th>
    <th>Mileage</th>
    <th>Service Due Date</th>
    <th>Manufacturing Year</th>
    <th>Invoice</th>
  </tr>
  <tr>
    <td>1</td>
    <td>Amaze</td>
    <td>1/1/2018</td>
    <td>Standard Service, Clutch Repair</td>
    <td>5500</td>
    <td>Suspension Work need to be done</td>
    <td>22000</td>
    <td>5/1/2018</td>
    <td>2016</td>
    <td class="action"><a href="#"><i class="fa fa-eye"></i></a><a href="#"><i class="fa fa-edit"></i></a></td>
  </tr>
  <tr>
    <td>2</td>
    <td>Hyundai Verna</td>
    <td>43160</td>
    <td>Brake Pads Replacement</td>
    <td>4500</td>
    <td>Engine Ovehauling Required</td>
    <td>66000</td>
    <td>5/1/2018</td>
    <td>2012</td>
    <td class="action"><a href="#"><i class="fa fa-eye"></i></a><a href="#"><i class="fa fa-edit"></i></a></td>
  </tr>
  <tr>
    <td>3</td>
    <td>Amaze</td>
    <td>1/1/2018</td>
    <td>Standard Service, Clutch Repair</td>
    <td>5500</td>
    <td>Suspension Work need to be done</td>
    <td>22000</td>
    <td>5/1/2018</td>
    <td>2010</td>
    <td class="action"><a href="#"><i class="fa fa-eye"></i></a><a href="#"><i class="fa fa-edit"></i></a></td>
  </tr>
  <tr>
    <td>2</td>
    <td>Hyundai Verna</td>
    <td>43160</td>
    <td>Brake Pads Replacement</td>
    <td>4500</td>
    <td>Engine Ovehauling Required</td>
    <td>66000</td>
    <td>5/1/2018</td>
    <td>2012</td>
    <td class="action"><a href="#"><i class="fa fa-eye"></i></a><a href="#"><i class="fa fa-edit"></i></a></td>
  </tr>
</table>
</div>
 
<div class="button-tab"><button type="submit" name="" class="btn create">Add Vehicle</button></div>
 <div class="button-tab">
<button type="submit" name="" class="btn cancel">Register</button> 
<button type="submit" name="" class="btn create">Clear</button>
<button type="submit" name="" class="btn order">Close</button>
</div>
 

</div>
</div>


</div>
</section>

  
 
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
<script>
$(document).ready(function(){
var date_input=$('input[name="date"]'); //our date input has the name "date"
var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
date_input.datepicker({
	format: 'mm/dd/yyyy',
	container: container,
	todayHighlight: true,
	autoclose: true,
})
})
</script>
