<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>Gaadizo ERP - Dashboard</title>
	<meta name="description" content="Gaadizo ERP is a Dashboard & Admin Site for managing Car Service Workshop" />
	<meta name="keywords" content="Gaadizo ERP" />
	
	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">
	<!-- Data table CSS -->
	<link href="vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
	<link href="vendors/bower_components/datatables.net-responsive/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css"/>
	
	<!-- Custom CSS -->
	<link href="dist/css/style.css" rel="stylesheet" type="text/css">
	
	<!--  w3 include html -->
		<script src="https://www.w3schools.com/lib/w3.js"></script>
</head>

<body>
	<!--Preloader-->
	<div class="preloader-it">
		<div class="la-anim-1"></div>
	</div>
	<!--/Preloader-->
    <div class="wrapper theme-1-active pimary-color-blue slide-nav-toggle">

			<!-- Top Menu Items -->
			
			<#--<div w3-include-html="#"><#include "nav-1.ftl"/></div>--> 
			
			<!-- Left Sidebar Menu -->
			<#--<div w3-include-html="#"><#include "left.ftl"/></div>--> 
		
			<!-- Right Sidebar Menu -->
			<#--<div w3-include-html="#"><#include "right.ftl"/></div>--> 

        <!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid pt-25">
			<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h5 class="txt-dark">GaadiZo Data Import</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					  <ol class="breadcrumb">
						<li><a href="<@ofbizUrl>main</@ofbizUrl>">Dashboard</a></li>
						<li><a href="<@ofbizUrl>bookinglist</@ofbizUrl>"><span>GaadiZo Data Import</span></a></li>
						
					  </ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->
			<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
						
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="datable_1" class="table  display table-hover mb-30" border="0" style="text-align:left;">
											  <thead>
												<tr>
													<th style="width:50%;" >Purpose of Import</th>
													<th style="width:50%;" >Action</th>
												</tr>
											 </thead>
											 <tbody>
											 <tr>
													<td >
													    <label>Import Company Infomation</label>
														</td>
														<td >
														 <form name="importaddrmap123" method="post" enctype="multipart/form-data" action="<@ofbizUrl>importCompanyInERPFromCsv</@ofbizUrl>">
            												<input type="file" name="uploadedFile" size="14"/>
          												    <input type="submit" value="${uiLabelMap.CommonUpload}"/>
            											 </form>
														</td>
												</tr>
												<tr>
													<td >
													    <label>Import Company Chart of Account</label>
														</td>
														<td >
														 <form name="importChartOfAccount" method="post" enctype="multipart/form-data" action="<@ofbizUrl>importChartOfAccountInERPFromCsv</@ofbizUrl>">
            												<input type="file" name="uploadedFile" size="14"/>
          												    <input type="submit" value="${uiLabelMap.CommonUpload}"/>
            											 </form>
														</td>
												</tr>
												<tr>
													<td >
													    <label>Import Product and Vehicle Category Details</label>
														</td>
														<td >
														<form name="importCategoryInERPFromCsv" method="post" enctype="multipart/form-data" action="<@ofbizUrl>importCategoryInERPFromCsv</@ofbizUrl>">
            												<input type="file" name="uploadedFile" size="14"/>
          												    <input type="submit" value="${uiLabelMap.CommonUpload}"/>
            											 </form>
														<a href="<@ofbizUrl>importCategoryInERPFromCsv</@ofbizUrl>">Import</a>
														</td>
												</tr>
												<tr>
													<td >
													    <label>Import Vehicle Details</label>
														</td>
														<td>
                                                         <form name="importVehicleInERPFromCsv" method="post" enctype="multipart/form-data" action="<@ofbizUrl>importVehicleInERPFromCsv</@ofbizUrl>">
            												<input type="file" name="uploadedFile" size="14"/>
          												    <input type="submit" value="${uiLabelMap.CommonUpload}"/>
            											 </form>
														<a href="<@ofbizUrl>importVehicleInERPFromCsv</@ofbizUrl>">Import</a>
														</td>
												</tr>
												<tr>
													<td >
													    <label>Import Products (Services/Spare Parts) Details</label>
														</td>
														<td >
														<form name="importProductInERPFromCsv" method="post" enctype="multipart/form-data" action="<@ofbizUrl>importProductInERPFromCsv</@ofbizUrl>">
            												<input type="file" name="uploadedFile" size="14"/>
          												    <input type="submit" value="${uiLabelMap.CommonUpload}"/>
            											 </form>
														<a href="<@ofbizUrl>importProductInERPFromCsv</@ofbizUrl>">Import</a>
														</td>
												</tr>
												<tr>
													<td >
													    <label>Import Product Price Per Vehicle Details</label>
														</td>
														<td>
														<form name="importPVPriceInERPFromCsv" method="post" enctype="multipart/form-data" action="<@ofbizUrl>importPVPriceInERPFromCsv</@ofbizUrl>">
            												<input type="file" name="uploadedFile" size="14"/>
          												    <input type="submit" value="${uiLabelMap.CommonUpload}"/>
            											 </form>
														<a href="<@ofbizUrl>importPVPriceInERPFromCsv</@ofbizUrl>">Import</a>
														</td>
												</tr>
												<tr>
													<td >
													    <label>Import Product and Item Association Details</label>
														</td>
														<td>
														<form name="importProductItemAssocFromCsv" method="post" enctype="multipart/form-data" action="<@ofbizUrl>importProductItemAssocFromCsv</@ofbizUrl>">
            												<input type="file" name="uploadedFile" size="14"/>
          												    <input type="submit" value="${uiLabelMap.CommonUpload}"/>
            											 </form>
														<a href="<@ofbizUrl>importProductItemAssocFromCsv</@ofbizUrl>">Import</a>
														</td>
												</tr>
												<tr>
													<td >
													    <label>Import Product Inventory Details</label>
														</td>
														<td>
														<form name="importProductInventoryFromCsv" method="post" enctype="multipart/form-data" action="<@ofbizUrl>importProductInventoryFromCsv</@ofbizUrl>">
            												<input type="file" name="uploadedFile" size="14"/>
          												    <input type="submit" value="${uiLabelMap.CommonUpload}"/>
            											 </form>
														<a href="<@ofbizUrl>importProductInventoryFromCsv</@ofbizUrl>">Import</a>
														</td>
												</tr>
												<tr>
													<td >
													    <label>Import Sample Orders</label>
														</td>
														<td>
														<form name="importVehicleInERPFromCsv" method="post" enctype="multipart/form-data" action="<@ofbizUrl>importOrderInERPService</@ofbizUrl>">
            												<input type="file" name="uploadedFile" size="14"/>
          												    <input type="submit" value="${uiLabelMap.CommonUpload}"/>
            											 </form>
														<a href="<@ofbizUrl>importOrderInERPService</@ofbizUrl>">Import</a>
														</td>
												</tr>
												</tbody>
											</table>
										</div>
									</div>	
								</div>	
							</div>
						</div>
					</div>
				</div>
				</div>
								</div>
				<!-- /Row -->

		</div>
			
		<!-- Footer -->
		<footer class="footer container-fluid pl-30 pr-30">
			<div class="row">
				<div class="col-sm-12">
					<p>2018 &copy; Gaadizo</p>
				</div>
			</div>
		</footer>
		<!-- /Footer -->
			
		</div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
	
	<!-- JavaScript -->
		<script>
		w3.includeHTML();
		</script>
	
    <!-- jQuery -->
    <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    
	<!-- Data table JavaScript -->
	<script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
	<script src="vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	<script src="vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	<script src="dist/js/responsive-datatable-data.js"></script>
	
	<!-- Slimscroll JavaScript -->
	<script src="dist/js/jquery.slimscroll.js"></script>
	
	<!-- Fancy Dropdown JS -->
	<script src="dist/js/dropdown-bootstrap-extended.js"></script>
	
	<!-- Owl JavaScript -->
	<script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>
	
	<!-- Switchery JavaScript -->
	<script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>
		
	<!-- Init JavaScript -->
	<script src="dist/js/init.js"></script>
	