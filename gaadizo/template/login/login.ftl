<section class="login-page">

<div class="logo"><img src="/erpTheme/images/erp-logo.png" alt=""></div>
<div class="login-box">
<h3>Sign In</h3>

<form method="post" action="<@ofbizUrl>login</@ofbizUrl>" name="loginform">
<div class="form-group">
<label>Username</label>
<input type="text" name="USERNAME" class="form-control" value="${username!}" placeholder="Enter username">
</div>

<div class="form-group">
<label>Password</label>
<input type="password" name="PASSWORD" class="form-control" placeholder="Enter Password">
</div>

<div class="form-group">
<div class="row">
<div class="col-sm-6 col-xs-6"><input type="checkbox" name=""> Remember me</div>
<div class="col-sm-6 col-xs-6 text-right"><a href="#model">Forgot pwd?</a></div>
</div>
</div>

<input type="submit" name="" class="btn btn-danger" value="Login">
</form>

</div>
</section>
<#-- 
<div class="or"><span>EASILY LOGIN</span></div>

 div class="social">
<div class="row">
<div class="col-sm-6 col-xs-6"><a href="#" class="facebook"><i class="fa fa-facebook-f"></i> Login With Facebook</a></div>
<div class="col-sm-6 col-xs-6"><a href="#" class="google"><i class="fa fa-google-plus"></i>  Login With Google</a></div>
</div>
</div -->


<#-- 
		<div class="wrapper pa-0" style="margin-top:-40px;">
			<header class="sp-header">
				<div class="sp-logo-wrap pull-left">
					<a href="index.html">
						<img class="brand-img mr-10" src="/erp/css/dist/img/logo.png" alt="brand"/>
						<span class="brand-text">Gaadizo</span>
					</a>
				</div>
				<#-- div class="form-group mb-0 pull-right">
					<span class="inline-block pr-10">Don't have an account?</span>
					<a class="inline-block btn btn-info btn-rounded btn-outline" href="signup.html">Sign Up</a>
				</div -->
				<div class="clearfix"></div>
			</header>
			
			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0 auth-page">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<h3 class="text-center txt-dark mb-10">Sign in to Gaadizo</h3>
											<h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
										</div>	
										<div class="form-wrap">
											<form method="post" action="<@ofbizUrl>login</@ofbizUrl>" name="loginform">
												<div class="form-group">
													<label class="control-label mb-10" for="exampleInputEmail_2">Email address</label>
													<input type="text" class="form-control" name="USERNAME" value="${username!}" required="" id="exampleInputEmail_2" placeholder="Enter email">
												</div>
												<div class="form-group">
													<label class="pull-left control-label mb-10" for="exampleInputpwd_2">Password</label>
													<#-- a class="capitalize-font txt-primary block mb-10 pull-right font-12" href="forgot-password.html">forgot password ?</a -->
													<div class="clearfix"></div>
													<input type="password" name="PASSWORD" class="form-control" required="" id="exampleInputpwd_2" placeholder="Enter pwd">
												</div>
												
												<#-- div class="form-group">
													<div class="checkbox checkbox-primary pr-10 pull-left">
														<input id="checkbox_2" required="" type="checkbox">
														<label for="checkbox_2"> Keep me logged in</label>
													</div>
													<div class="clearfix"></div>
												</div -->
												<div class="form-group text-center">
													<button type="submit" class="btn btn-info btn-rounded">sign in</button>
												</div>
											</form>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->	
				</div>
				
			</div>
			<!-- /Main Content -->
		
		</div>
		<!-- /#wrapper -->
-->