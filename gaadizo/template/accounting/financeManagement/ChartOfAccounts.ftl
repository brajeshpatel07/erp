		    <div class="page-wrapper">
			<div class="container-fluid pt-25">
			<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
						
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="datable_1" class="table  display table-hover mb-30">
												<thead>
													<tr>
														<th>#Code</th>
														<th>Account Name</th>
														<th>Parent Gl Account ID</th>
														<th>Gl Account Type ID</th>
														<th>Gl Account Class ID</th>
														<th>Gl Resource Type ID</th>
														<th>Description</th>
														<th>Product ID</th>
														<th>External ID</th>
													</tr>
												</thead>

												<tbody>
												    <#list glAccounts as glAccount>
													<tr>
														<td>
														<a class="buttontext" href="<@ofbizUrl>GlAccountNavigate?glAccountId=${glAccount.accountCode!}</@ofbizUrl>" title="Code">
      ${glAccount.accountCode!}</a>
														
														</td>
														<td>${glAccount.accountName!}</td>
														<td>${glAccount.parentGlAccountId!}</td>
														<td>
															<span class="label label-danger">${glAccount.glAccountTypeId!}</span>
														</td>
														<td>${glAccount.glAccountClassId!}</td>
														<td>${glAccount.glResourceTypeId!}</td>
														<td>
															<a href="#" data-tooltip="Invoice" data-position="top" class="top icon-btn"><i class="fa fa-file-text">${glAccount.description!}</i></a>	
														</td>
														<td>${glAccount.productId!}</td>
														<td>${glAccount.externalId!}</td>
													</tr>
													</#list>
													
												</tbody>
											</table>
										</div>
									</div>	
								</div>	
							</div>
						</div>
					</div>
				</div>
				</div>
								</div>
				<!-- /Row -->

