<#escape x as x?xml>
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format"
    <#-- inheritance -->
    <#if defaultFontFamily?has_content>font-family="${defaultFontFamily}"</#if>
>
    <fo:layout-master-set>
        <fo:simple-page-master master-name="main-page"
              page-width="8.267in" page-height="11.692in"
              margin-top="0.2in" margin-bottom="0.2in"
              margin-left="0.3in" margin-right="0.3in">
            <#-- main body -->
            <fo:region-body margin-top="3.3in" margin-bottom="1.5in"/>
            <#-- the header -->
            <fo:region-before extent="0.1in"/>
            <#-- the footer -->
            <fo:region-after extent="1.1in"/>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="main-page-landscape"
              page-width="11in" page-height="8.5in"
              margin-top="0.4in" margin-bottom="0.4in"
              margin-left="0.6in" margin-right="0.4in">
            <#-- main body -->
            <fo:region-body margin-top="1.2in" margin-bottom="0.4in"/>
            <#-- the header -->
            <fo:region-before extent="1.2in"/>
            <#-- the footer -->
            <fo:region-after extent="0.4in"/>
        </fo:simple-page-master>
    </fo:layout-master-set>
    <fo:page-sequence master-reference="${pageLayoutName?default("main-page")}">


        <#assign bodyFontSize = "8pt" />
        <#assign headerFontSize = "10pt" />
        <#assign headerTypeFontSize = "14pt" />
        <#assign footerFontSize = "9pt" />
        <#assign valueColor = "#A50000" />
        <#assign columnPadding = "2pt" />
        <#assign fontBold = "Y" />

        <#-- Company name-->
        <#assign partyGroup =   delegator.findByPrimaryKey("PartyGroup",Static["org.ofbiz.base.util.UtilMisc"].toMap("partyId",companyId?if_exists))?if_exists/>
        <#if partyGroup?has_content>
          <#assign companyName = partyGroup.groupName>
        </#if>
         <#assign companyEmail = "naresh@gaadizo.com"/>
         <#assign companyPhone = "91-124-11111111 / 222222222"/>
		<#assign imageUrlPrefix = Static["org.ofbiz.base.util.UtilProperties"].getPropertyValue("gaadizo.properties", "image_url")>
        <#-- Header -->
        <fo:static-content flow-name="xsl-region-before">
            <fo:table table-layout="fixed" width="100%" >
                <fo:table-column column-number="1" column-width="proportional-column-width(40)"/>
                <fo:table-column column-number="2" column-width="proportional-column-width(20)"/>
                <fo:table-column column-number="3" column-width="proportional-column-width(40)"/>
                <fo:table-body>
                <fo:table-row>
                        <fo:table-cell>
                            <fo:block  text-align="left">
                                    <fo:external-graphic src="<@ofbizContentUrl>http://192.168.0.110:7085/oms_theme/images/invoice_logo.png</@ofbizContentUrl>" height="40px" content-width="2348px" content-height="scale-to-fit"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block  text-align="left">
                                    <fo:external-graphic src="<@ofbizContentUrl>http://192.168.0.110:7085/oms_theme/images/invoice_logo.png</@ofbizContentUrl>" height="71px" content-width="2348px" content-height="scale-to-fit"/>
                            </fo:block>
                        </fo:table-cell>
                </fo:table-row>
                
                
                <fo:table-row>

                <#assign facilityId = orderFacility.facilityId?if_exists/>
                <#assign facilityContactMechPurposes = delegator.findByAnd("FacilityContactMechPurpose", {"facilityId" : facilityId?if_exists})?if_exists/>
                <#assign facilityContactMechPurposes = Static["org.ofbiz.entity.util.EntityUtil"].filterByDate(facilityContactMechPurposes?if_exists)?if_exists/>
        
                <#-- facility Address -->
                <#assign facilityAddresses = Static["org.ofbiz.entity.util.EntityUtil"].filterByAnd(facilityContactMechPurposes?if_exists, {"contactMechPurposeTypeId" : "SHIP_ORIG_LOCATION"})?if_exists />
                <#if facilityAddresses?has_content>
                    <#assign facilityAddress = Static["org.ofbiz.entity.util.EntityUtil"].getFirst(facilityAddresses)/>
                    <#assign facilityAddress = facilityAddress.getRelatedOne("ContactMech")/>
                    <#assign facilityAddress = facilityAddress.getRelatedOne("PostalAddress")/>
                </#if>
                
                <#assign formattedCellPhone = ''/>
	             <#if billingParty?has_content>
	                <#assign partyContactMechPurpose = billingParty.getRelated("PartyContactMechPurpose")/>
	                <#assign partyContactMechPurpose = Static["org.ofbiz.entity.util.EntityUtil"].filterByDate(partyContactMechPurpose,true)/>
	                <#assign partyPurposeMobilePhones = Static["org.ofbiz.entity.util.EntityUtil"].filterByAnd(partyContactMechPurpose, Static["org.ofbiz.base.util.UtilMisc"].toMap("contactMechPurposeTypeId", "PHONE_MOBILE"))/>
	                <#assign partyPurposeMobilePhones = Static["org.ofbiz.entity.util.EntityUtil"].getRelated("PartyContactMech", partyPurposeMobilePhones)/>
	                <#assign partyPurposeMobilePhones = Static["org.ofbiz.entity.util.EntityUtil"].filterByDate(partyPurposeMobilePhones,true)/>
	                <#assign partyPurposeMobilePhones = Static["org.ofbiz.entity.util.EntityUtil"].orderBy(partyPurposeMobilePhones, Static["org.ofbiz.base.util.UtilMisc"].toList("fromDate DESC"))/>
	                <#if partyPurposeMobilePhones?has_content> 
	                    <#assign partyPurposePhone = Static["org.ofbiz.entity.util.EntityUtil"].getFirst(partyPurposeMobilePhones)/>
	                    <#assign telecomNumber = partyPurposePhone.getRelatedOne("TelecomNumber")/>
	                    <#assign phoneMobileTelecomNumber =telecomNumber/>
	                    <#assign formattedCellPhone = Static["com.osafe.util.OsafeAdminUtil"].formatTelephone(phoneMobileTelecomNumber.areaCode?if_exists, phoneMobileTelecomNumber.contactNumber?if_exists, globalContext.FORMAT_TELEPHONE_NO!)/>
	                </#if>
	             </#if>
                    <fo:table-cell padding="2pt">
                    	<fo:block font-size="8pt" font-weight="bold" text-align="left">BILLING ADDRESS</fo:block>
						<fo:block font-size="8pt">
								<#if personInvoice?exists && personInvoice?has_content>
								    ${personInvoice.firstName!}
								</#if>
								</fo:block>
                    	<#if billingAddress?exists>
                                    <#if billingAddress?has_content>
                                        <#if billingAddress.address1?has_content>
                                            <fo:block  font-size="${bodyFontSize!}">
                                                ${billingAddress.address1?if_exists}
                                            </fo:block>
                                        </#if>
                                        <#if billingAddress.address2?has_content>
                                            <fo:block  font-size="${bodyFontSize!}" >
                                                ${billingAddress.address2?if_exists}
                                            </fo:block>
                                        </#if>
                                        <#if billingAddress.address3?has_content>
                                            <fo:block  font-size="${bodyFontSize!}" >
                                                ${billingAddress.address3?if_exists}
                                            </fo:block>
                                        </#if>
                                        <fo:block  font-size="${bodyFontSize!}" >
                                            <#if billingAddress.city?has_content>
                                                ${billingAddress.city?if_exists}
                                            </#if> 
                                            
                                            <#if billingAddress.postalCode?has_content>
                                                ${billingAddress.postalCode!""}
                                            </#if>
                                        </fo:block>
                                    </#if>
                                </#if>
                                <fo:block font-size="8pt" color="#FFFFFF" >BLANK</fo:block>
                     			<fo:block font-size="8pt" font-weight="bold" text-align="left">SHIPPING ADDRESS</fo:block>
								<fo:block font-size="8pt">
								<#if personInvoice?exists && personInvoice?has_content>
								    ${personInvoice.firstName!}
								</#if>
								</fo:block>
                                <#if shippingAddress?exists>
                                    <#if shippingAddress?has_content>
                                        <#if shippingAddress.address1?has_content>
                                            <fo:block  font-size="${bodyFontSize!}">
                                                ${shippingAddress.address1?if_exists}
                                            </fo:block>
                                        </#if>
                                        <#if shippingAddress.address2?has_content>
                                            <fo:block  font-size="${bodyFontSize!}" >
                                                ${shippingAddress.address2?if_exists}
                                            </fo:block>
                                        </#if>
                                        <#if shippingAddress.address3?has_content>
                                            <fo:block  font-size="${bodyFontSize!}" >
                                                ${shippingAddress.address3?if_exists}
                                            </fo:block>
                                        </#if>
                                        <fo:block  font-size="${bodyFontSize!}" >
                                            <#if shippingAddress.city?has_content>
                                                ${shippingAddress.city?if_exists}
                                            </#if> 
                                            <#if shippingAddress.stateProvinceGeoId?has_content>
                                                <#assign stateProvince = delegator.findByPrimaryKeyCache("Geo", {"geoId" : shippingAddress.stateProvinceGeoId})?if_exists>
                                                , ${stateProvince.geoName?cap_first!stateProvince.geoId!""}
                                            </#if>
                                            <#if shippingAddress.postalCode?has_content>
                                                ${shippingAddress.postalCode!""}
                                            </#if>
                                        </fo:block>
                                    </#if>
                                </#if>
                              <fo:block font-size="8pt" color="#FFFFFF" >BLANK</fo:block>
                     			<fo:block font-size="8pt" font-weight="bold" text-align="left">Payment Detail:</fo:block>
								<fo:block font-size="8pt">
									<#if payment?exists && payment?has_content && payment.paymentMethodTypeId == "EXT_CITRUS">
										Mode : Citrus
									<#elseif payment?exists && payment?has_content && payment.paymentMethodTypeId == "EXT_PAYPAL">
										Mode : PayPal
									<#elseif payment?exists && payment?has_content && payment.paymentMethodTypeId == "EXT_PAYU">
								        Mode : PayU
									</#if>
								</fo:block>
								<fo:block font-size="8pt"><#if payment?exists && payment?has_content>TransactionId : ${transactionId!}</#if></fo:block>
                         </fo:table-cell>
                        <fo:table-cell>
                        </fo:table-cell>
                        <fo:table-cell>
	                        <fo:table table-layout="fixed" width="100%">
						        <fo:table-column column-number="1" column-width="proportional-column-width(45)"/>
						        <fo:table-column column-number="2" column-width="proportional-column-width(55)"/>
						        <fo:table-body>
						          <fo:table-row>
						            <fo:table-cell>
						            	<fo:block font-size="8pt" font-weight="bold" >Invoice Number</fo:block>
	                        		</fo:table-cell>
	                        		<fo:table-cell>
						            	<fo:block font-size="${bodyFontSize!}" > : ${invoice.invoiceId} </fo:block>
	                        		</fo:table-cell>
	                        	  </fo:table-row>
	                        	  <fo:table-row>
						            <fo:table-cell>
						            	<fo:block font-size="${bodyFontSize!}"  >Invoice Date </fo:block>
	                        		</fo:table-cell>
	                        		<fo:table-cell>
						            	<fo:block font-size="${bodyFontSize!}"  > : ${invoiceDate?if_exists}</fo:block>
	                        		</fo:table-cell>
	                        	  </fo:table-row>
	                        	  <fo:table-row>
						            <fo:table-cell>
						            	<fo:block font-size="${bodyFontSize!}" >Page </fo:block>
	                        		</fo:table-cell>
	                        		<fo:table-cell>
						            	<fo:block font-size="${bodyFontSize!}" > : <fo:page-number/> ${uiLabelMap.CommonOf} <fo:page-number-citation ref-id="theEnd"/> </fo:block>
						            	<fo:block font-size="8pt" color="#FFFFFF" >BLANK</fo:block>
	                        		</fo:table-cell>
	                        	  </fo:table-row>
	                        	  <fo:table-row >
		                        	  <fo:table-cell number-columns-spanned="2"  >
		                                <fo:block font-size="8pt" text-align="left" font-weight="bold">${companyName!}</fo:block>
		                                <#if facilityAddress?exists>
		                                    <#if facilityAddress?has_content>
	                                        <#if facilityAddress.address1?has_content>
	                                            <fo:block font-size="8pt" text-align="left">
	                                                ${facilityAddress.address1?if_exists}
	                                            </fo:block>
	                                        </#if>
	                                        <#if facilityAddress.address2?has_content>
	                                            <fo:block font-size="8pt" text-align="left">
	                                                ${facilityAddress.address2?if_exists}
	                                            </fo:block>
	                                        </#if>
	                                        <#if facilityAddress.address3?has_content>
	                                            <fo:block font-size="8pt" text-align="left">
	                                                ${facilityAddress.address3?if_exists}
	                                            </fo:block>
	                                        </#if>
	                                        <fo:block font-size="8pt" text-align="left">
	                                            <#if facilityAddress.city?has_content>
	                                                ${facilityAddress.city?if_exists}
	                                            </#if> 
	                                            <#if facilityAddress.stateProvinceGeoId?has_content>
	                                                <#assign stateProvince = delegator.findByPrimaryKeyCache("Geo", {"geoId" : facilityAddress.stateProvinceGeoId})?if_exists>
	                                                , ${stateProvince.geoName?cap_first!stateProvince.geoId!""}
	                                            </#if>
	                                            <#if facilityAddress.postalCode?has_content>
	                                                ${facilityAddress.postalCode!""}
	                                            </#if>
	                                    	</#if>
			                              <#else>
			                                    <fo:block font-size="8pt" text-align="left">${uiLabelMap.NoPostalAddressInfo}</fo:block>
			                                    <fo:block font-size="8pt" text-align="left">${uiLabelMap.ForCaption} ${companyName!}</fo:block>
			                              </#if>
			                            </fo:block>
		                        	   </fo:table-cell>
	                        	  </fo:table-row>
	                        	   <fo:table-row>
						            <fo:table-cell>
						            	<fo:block font-size="8pt" color="#FFFFFF" >BLANK</fo:block>
						            	<fo:block font-size="8pt" >GSTIN  </fo:block>
	                        		</fo:table-cell>
	                        		<fo:table-cell>
	                        			<fo:block font-size="8pt" color="#FFFFFF" >BLANK</fo:block>
						            	<fo:block font-size="8pt" > : 29AADCH6537K1Z5</fo:block>
	                        		</fo:table-cell>
	                        	  </fo:table-row>
	                        	  <fo:table-row>
						            <fo:table-cell>
						            	<fo:block font-size="8pt" >Email </fo:block>
	                        		</fo:table-cell>
	                        		<fo:table-cell>
						            	 <#if companyEmail?exists>
		                                        <fo:block font-size="8pt" >: ${companyEmail?if_exists}</fo:block>
		                                 </#if>
	                        		</fo:table-cell>
	                        	  </fo:table-row>
	                        	  <fo:table-row>
						            <fo:table-cell>
						            	<fo:block font-size="8pt" >Tel No </fo:block>
	                        		</fo:table-cell>
	                        		<fo:table-cell>
						            	<#if companyPhone?exists>
		                                        <fo:block font-size="8pt" > : ${companyPhone?if_exists}</fo:block>
		                                </#if>	
	                        		</fo:table-cell>
	                        	  </fo:table-row>
	                        	  <fo:table-row >
						            <fo:table-cell number-columns-spanned="2">
	                        	  		<fo:block font-size="8pt" color="#FFFFFF" >BLANK</fo:block>
	                        	  	</fo:table-cell>
	                        	  </fo:table-row>
	                        	  <fo:table-row font-weight="bold">
						            <fo:table-cell>
						            	<fo:block font-size="8pt" >Order No </fo:block>
	                        		</fo:table-cell>
                                    
	                        		<fo:table-cell>
						            	 <fo:block font-size="8pt" font-weight="bold"><#list orders as order><#assign orderId=order?if_exists > : ${order}</#list>-${releaseNumberForRef!"01"}</fo:block>
	                        		</fo:table-cell>
	                        	  </fo:table-row>
	                        	</fo:table-body>
						     </fo:table>
                        </fo:table-cell>
                    </fo:table-row>
                  </fo:table-body>
            </fo:table>
			<fo:table table-layout="fixed" width="100%">
				<fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(30)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(17)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(25)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(18)"/>
				<fo:table-header font-size="${bodyFontSize!}" font-weight="bold">
					<fo:table-row border-bottom-style="solid">
						<fo:table-cell  >
							<fo:block text-indent="0.05in" font-size="${bodyFontSize!}" text-align="center">Product Image</fo:block>
							<fo:block text-indent="0.05in" font-size="${bodyFontSize!}" text-align="center">SKU No</fo:block>
						</fo:table-cell>											
						
						 <fo:table-cell >
							<fo:block text-indent="0.05in" font-size="${bodyFontSize!}" text-align="center">Product Description</fo:block>
						</fo:table-cell>
						<fo:table-cell >
							<fo:block text-indent="0.05in" font-size="${bodyFontSize!}" text-align="center">Unit Price</fo:block>
						</fo:table-cell>
						<fo:table-cell >
							<fo:block text-indent="0.05in" font-size="${bodyFontSize!}" text-align="center">Units</fo:block>
						</fo:table-cell>
						<fo:table-cell >
							<fo:block text-indent="0.05in" font-size="${bodyFontSize!}"  text-align="center">Amount</fo:block>
						</fo:table-cell>
					  </fo:table-row>
				</fo:table-header>
				<fo:table-body font-size="1pt">
					<fo:table-row>
						<fo:table-cell number-columns-spanned="5">
							<fo:block color="#fffff">BLANK</fo:block>
						</fo:table-cell>
					 </fo:table-row>
				</fo:table-body>
				
            </fo:table>
        </fo:static-content>
        
        <#-- the footer -->
        <fo:static-content flow-name="xsl-region-after">
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
                <fo:table-column column-number="2" column-width="proportional-column-width(30)"/>
                <fo:table-column column-number="3" column-width="proportional-column-width(40)"/>
                <fo:table-body>																				
					 <fo:table-row border-bottom-style="solid">
                        <fo:table-cell number-columns-spanned="3">
                        <fo:block <#if fontBold?has_content && fontBold == "Y">font-weight="bold"</#if> font-size="${footerFontSize!}" text-align="center">
                        This is a computer generated Invoice and hence does not require a signature note.
                        </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                        <fo:table-cell number-columns-spanned="3">
                        	<fo:block <#if fontBold?has_content && fontBold == "Y">font-weight="bold"</#if> font-size="${footerFontSize!}" text-align="center">
	                        	www.quancious.com
	                        </fo:block>
                            <fo:block text-align="center">
                                <fo:instream-foreign-object>
                                    <qr:qr-code xmlns:qr="http://hobbut.ru/fop/qr-code/" width="2.5cm" message="${StringUtil.wrapString('www.quancious.com')}" correction="l"/>
                                </fo:instream-foreign-object>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                  </fo:table-body>
            </fo:table>
        </fo:static-content>

        <#-- the body -->
        <fo:flow flow-name="xsl-region-body">
        <fo:table table-layout="fixed" width="100%" >
        <fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
        <fo:table-body>
            <fo:table-row>
            <fo:table-cell>
                <fo:table border-bottom-style="solid" >
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell>
                                <fo:table table-layout="fixed" width="100%">
                                    <fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
                                    <fo:table-column column-number="2" column-width="proportional-column-width(30)"/>
                                    <fo:table-column column-number="3" column-width="proportional-column-width(20)"/>
                                    <fo:table-column column-number="4" column-width="proportional-column-width(20)"/>
                                    <fo:table-column column-number="5" column-width="proportional-column-width(20)"/>
                                    <fo:table-body font-size="10pt">
									<fo:table-row>
								<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if>>
									<fo:block font-size="8pt" color="#FFFFFF" >BLANK</fo:block>
								</fo:table-cell>
								</fo:table-row>
                                        <#assign currentShipmentId = "">
                                        <#assign newShipmentId = "">
                                        <#assign invoiceItem_sequence = 0 />
										<#assign HSCODE = ""/>
                                        <#-- if the item has a description, then use its description.  Otherwise, use the description of the invoiceItemType -->
                                        <#assign invoiceItemIndexForDisplay = 1 />
                                        <#assign orderSubtotal = 0>
                                        <#assign orderDiscount = 0>
                                        <#assign tax = 0>
                                        <#list invoiceItems as invoiceItem>
                                            <#assign itemType = invoiceItem.getRelatedOne("InvoiceItemType")>
                                            <#assign isItemAdjustment = Static["org.ofbiz.common.CommonWorkers"].hasParentType(delegator, "InvoiceItemType", "invoiceItemTypeId", itemType.getString("invoiceItemTypeId"), "parentTypeId", "INVOICE_ADJ")/>
                                            <#assign taxRate = invoiceItem.getRelatedOne("TaxAuthorityRateProduct")?if_exists>
                                            <#assign itemBillings = invoiceItem.getRelated("OrderItemBilling")?if_exists>
                                            <#if itemBillings?has_content>
                                                <#assign itemBilling = Static["org.ofbiz.entity.util.EntityUtil"].getFirst(itemBillings)>
                                                <#if itemBilling?has_content>
                                                    <#assign orderItem = Static["org.ofbiz.entity.util.EntityUtil"].getFirst(delegator.findByAnd("OrderItem", {"orderId" : itemBilling.orderId, "orderItemSeqId" :itemBilling.orderItemSeqId}))/>
                                                    
                                                    <#assign orderItemAttributeList = delegator.findByAnd("OrderItemAttribute", {"orderId" : itemBilling.orderId, "orderItemSeqId" :itemBilling.orderItemSeqId})/>
                                                    <#assign imagePath = ""/>
                                                    <#assign garmentSize = ""/>
                                                    <#list orderItemAttributeList as orderItemAttribute>
                                                        <#if orderItemAttribute.attrName == "SIZE_TYPE">
                                                            <#assign orderItemAttributeSizeChart = orderItemAttribute /> 
                                                        </#if>
                                                        <#if orderItem?has_content && orderItem.productTypeTypeId == "FASHION">
	                                                        <#if orderItemAttribute.attrName?has_content && orderItemAttribute.attrName == "PRODUCT_IMAGE">
	                                                            <#assign imagePath = orderItemAttribute.attrValue!"">
	                                                        </#if>
                                                        <#else>
	                                                        <#if orderItemAttribute.attrName?has_content && orderItemAttribute.attrName == "GS1_DESIGN_IMAGE">
	                                                            <#assign imagePath = orderItemAttribute.attrValue!"">
	                                                        </#if>
                                                        </#if>
														<#if orderItemAttribute.attrName?has_content && orderItemAttribute.attrName == "HSCODE">
	                                                            <#assign HSCODE = orderItemAttribute.attrValue!"">
	                                                        </#if>
                                                        
                                                        <#if orderItemAttribute.attrName?has_content && orderItemAttribute.attrName == "SIZE">
                                                            <#assign sizeChartList = Static["org.ofbiz.base.util.StringUtil"].split(orderItemAttribute.attrValue,"|") />
											                <#if (sizeChartList.size() >= 2)>
											                    <#assign sizeChartListvalue = sizeChartList.get(sizeChartList.size()-2) />
											                    <#if ((sizeChartListvalue.length() >= 1))>
											                     	<#if (sizeChartListvalue.contains(";"))>
											                        	<#assign sizeChartListvalue = sizeChartListvalue.replaceAll(";",",") />
											                    	</#if>
											                    	 <#assign garmentSize = sizeChartListvalue/>
											                    </#if>
											                </#if>
                                                        </#if>
                                                    </#list>
                                                    
                                                    <#assign itemIssuance = itemBilling.getRelatedOne("ItemIssuance")?if_exists>
                                                    <#if itemIssuance?has_content>
                                                        <#assign newShipmentId = itemIssuance.shipmentId>
                                                        <#assign issuedDateTime = itemIssuance.issuedDateTime/>
                                                    </#if>
                                                </#if>
                                            </#if>
                                            <#if invoiceItem.description?has_content>
                                                <#assign description=invoiceItem.description>
                                            <#elseif taxRate?has_content & taxRate.get("description",locale)?has_content>
                                                <#assign description=taxRate.get("description",locale)>
                                            <#elseif itemType.get("description",locale)?has_content>
                                                <#assign description=itemType.get("description",locale)>
                                            </#if>

            
             
                                        <#if !isItemAdjustment>
                                        <fo:table-row >
                                        	<fo:table-cell>
							                    <fo:block text-align="center">
							                            <fo:external-graphic src="${StringUtil.wrapString(imageUrlPrefix)}${imagePath!""}" overflow="hidden" height="4.5cm" content-width="4.5cm" content-height="scale-to-fit"/>
							                    </fo:block>
												<fo:block text-align="center">${orderItem.productId!}</fo:block>
							                </fo:table-cell>
                                            <fo:table-cell <#if columnPadding?has_content>padding="${columnPadding!}"</#if>>
											
											<#if orderItem?has_content && orderItem.productTypeTypeId == "FASHION">
											    <fo:block font-weight="bold" font-size="${bodyFontSize!}" > Garment</fo:block>
										    <#else>
											    <fo:block font-weight="bold" font-size="${bodyFontSize!}" > ${orderItem.productTypeTypeId?if_exists}</fo:block>
											</#if>
											
											 <fo:block font-size="8pt" color="#FFFFFF" >BLANK</fo:block>
											
                                                <fo:block font-size="${bodyFontSize!}" >${description?if_exists}</fo:block>
                                                <#if garmentSize?exists && garmentSize?has_content>
                                                    <fo:block font-size="${bodyFontSize!}" >Size : ${garmentSize?if_exists}</fo:block>
                                                </#if>
                                                
												<fo:block font-size="8pt" color="#FFFFFF" >BLANK</fo:block>
                                                <fo:block font-size="${bodyFontSize!}">HS Code : ${HSCODE!}</fo:block>
                                            </fo:table-cell>
											
                                            <#assign orderedQuantity = ""/>
                                            <#if orderItem?exists && orderItem?has_content>
	                                            <#assign picklistItemRows = orderItem.getRelated("PicklistItem")!""/>
							                    <#assign picklistItemRow = Static["org.ofbiz.entity.util.EntityUtil"].getFirst(picklistItemRows)!""/>
							                    <#assign orderedItemSequence = "" />
							                    
							                    <#if picklistItemRow?exists && picklistItemRow?has_content>
							                    	<#assign orderedItemSequence = picklistItemRow.orderItemSeqId />
								                    <#if ((orderedItemSequence).contains("_"))>
								                    	<#assign orderedQuantity = "" />
								                    	<#else>
														<#assign orderedQuantity = picklistItemRow.quantity?if_exists/>
													</#if>
												</#if>
											</#if>
											<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if>>
                                                <fo:block text-indent="0.1in" font-size="${bodyFontSize!}" text-align="center"><#if invoiceItem.quantity?exists><@ofbizCurrency amount=invoiceItem.amount?if_exists isoCode=invoice.currencyUomId?if_exists/></#if> </fo:block>
                                            </fo:table-cell>
											<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if>>
                                                <fo:block text-indent="0.1in" font-size="${bodyFontSize!}" text-align="center"><#if orderedQuantity?has_content>${orderedQuantity?string.number}<#else>-</#if></fo:block>
												<#if orderItemAttributeSizeChart?exists && orderItemAttributeSizeChart?has_content>
													<fo:block text-indent="0.1in" font-size="${bodyFontSize!}" text-align="center"><#if "Standard" == orderItemAttributeSizeChart.attrValue!>Pcs<#else>${orderItemAttributeSizeChart.attrValue!}</#if></fo:block>
											    </#if>	
												
                                            </fo:table-cell>
                                            
                                            <#assign adj=invoiceItemsPromotionMap.get(invoiceItem.invoiceItemSeqId)?if_exists>
											
                                             <#if subtotalMapWithItemSeqId.containsKey(invoiceItem.invoiceItemSeqId)>
                                             <#assign offerPrice = 0.0 >
                                              <#else>
                                            <#assign offerPrice = invoiceItem.amount + adj >
                                            </#if>
                                            <fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if>>
                                                <fo:block text-indent="0.1in" font-size="${bodyFontSize!}" text-align="center">
                                                <#assign itemSubtotal= (Static["org.ofbiz.accounting.invoice.InvoiceWorker"].getInvoiceItemTotal(invoiceItem))>
                                                <@ofbizCurrency amount=itemSubtotal isoCode=invoice.currencyUomId?if_exists/>
                                             </fo:block>
                                            </fo:table-cell>
                                          </fo:table-row>
                                          <#assign orderSubtotal= itemSubtotal+orderSubtotal>
                                          </#if>
                                          <#if (invoiceItem.invoiceItemTypeId.equals("INV_FPROD_ITEM"))>
                                          <#assign invoiceItemIndexForDisplay = invoiceItemIndexForDisplay + 1 />
                                          </#if>
                                          </#list>
                                          <#--  For cancel -->
                                          </fo:table-body>
                                 </fo:table>
                             </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
					
                </fo:table>
    
									
                <fo:block space-after="0.1in"/>
    
                <fo:table table-layout="fixed" width="100%"  keep-together="always">
						<fo:table-column column-number="1" column-width="proportional-column-width(70)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(30)"/>
						<fo:table-body>
							
							<fo:table-row>
								<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if>>
									<fo:block <#if fontBold?has_content && fontBold == "Y">font-weight="bold"</#if> font-size="${bodyFontSize!}" text-indent="0.2in" text-align="right">Sub Total</fo:block>
								</fo:table-cell>
								<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if>>
									<fo:block <#if fontBold?has_content && fontBold == "Y">font-weight="bold"</#if> font-size="${bodyFontSize!}"  text-align="right"><@ofbizCurrency amount=orderSubtotal rounding="2" isoCode=invoice.currencyUomId?if_exists/></fo:block>
								</fo:table-cell>
								
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if> padding-after="8pt">
									<fo:block <#if fontBold?has_content && fontBold == "Y">font-weight="bold"</#if> font-size="${bodyFontSize!}" text-align="right">Discounts</fo:block>
								</fo:table-cell> 
								<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if> padding-after="8pt">
									<fo:block <#if fontBold?has_content && fontBold == "Y">font-weight="bold"</#if> font-size="${bodyFontSize!}"  text-align="right">
									<fo:inline color="red"><@ofbizCurrency amount=totalPromoValue rounding="2" isoCode=invoice.currencyUomId?if_exists/></fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if> padding-after="8pt">
									<fo:block <#if fontBold?has_content && fontBold == "Y">font-weight="bold"</#if> font-size="${bodyFontSize!}" text-indent="0.2in" text-align="right">Net Amount</fo:block>
								</fo:table-cell>
								<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if> padding-after="8pt">
									<fo:block <#if fontBold?has_content && fontBold == "Y">font-weight="bold"</#if> font-size="${bodyFontSize!}"  text-align="right">
										<@ofbizCurrency amount=(orderSubtotal+totalPromoValue) rounding="2" isoCode=invoice.currencyUomId?if_exists/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if>>
									<fo:block <#if fontBold?has_content && fontBold == "Y">font-weight="bold"</#if> font-size="${bodyFontSize!}" text-indent="0.2in" text-align="right">Shipping</fo:block>
								</fo:table-cell>
								<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if>>
									<fo:block <#if fontBold?has_content && fontBold == "Y">font-weight="bold"</#if> font-size="${bodyFontSize!}"  text-align="right"><@ofbizCurrency amount=amtShippingCharge rounding="2" isoCode=invoice.currencyUomId?if_exists/></fo:block>
								</fo:table-cell>
							</fo:table-row>
                            <#if invoice.currencyUomId?has_content && "INR" == invoice.currencyUomId>
							   
								<fo:table-row>
									<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if>>
										<fo:block <#if fontBold?has_content && fontBold == "Y">font-weight="bold"</#if> font-size="${bodyFontSize!}" text-indent="0.2in" text-align="right">Duties/<#if shippingAddress.address1.contains("Karnataka")>SGST<#else>IGST</#if></fo:block>
									</fo:table-cell>
									<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if>>
										<fo:block <#if fontBold?has_content && fontBold == "Y">font-weight="bold"</#if> font-size="${bodyFontSize!}"  text-align="right"><@ofbizCurrency amount="${sgstBD!}" rounding="2" isoCode=invoice.currencyUomId?if_exists/></fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if>>
										<fo:block <#if fontBold?has_content && fontBold == "Y">font-weight="bold"</#if> font-size="${bodyFontSize!}" text-indent="0.2in" text-align="right">Duties/CGST</fo:block>
									</fo:table-cell>
									<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if>>
										<fo:block <#if fontBold?has_content && fontBold == "Y">font-weight="bold"</#if> font-size="${bodyFontSize!}"  text-align="right"><@ofbizCurrency amount="${cgstBD!}" rounding="2" isoCode=invoice.currencyUomId?if_exists/></fo:block>
									</fo:table-cell>
								</fo:table-row>
                            <#else>							
							<fo:table-row>
								<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if>>
									<fo:block <#if fontBold?has_content && fontBold == "Y">font-weight="bold"</#if> font-size="${bodyFontSize!}" text-indent="0.2in" text-align="right">Duties/GST</fo:block>
								</fo:table-cell>
								<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if>>
									<fo:block <#if fontBold?has_content && fontBold == "Y">font-weight="bold"</#if> font-size="${bodyFontSize!}"  text-align="right"><@ofbizCurrency amount=amtSalesCharge rounding="2" isoCode=invoice.currencyUomId?if_exists/></fo:block>
								</fo:table-cell>
							</fo:table-row>	
							</#if>
							<fo:table-row>
								<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if> background-color="#D3D3D3">
									<fo:block <#if fontBold?has_content && fontBold == "Y">font-weight="bold"</#if> font-size="${bodyFontSize!}" text-indent="0.2in" text-align="right">Final Amount</fo:block>
								</fo:table-cell>
								<fo:table-cell  <#if columnPadding?has_content>padding="${columnPadding!}"</#if>  background-color="#D3D3D3">
									<fo:block <#if fontBold?has_content && fontBold == "Y">font-weight="bold"</#if> font-size="${bodyFontSize!}"  text-align="right"><@ofbizCurrency amount=(orderSubtotal+amtShippingCharge+amtSalesCharge)+totalPromoValue rounding="2" isoCode=invoice.currencyUomId?if_exists/></fo:block>
								</fo:table-cell>
							</fo:table-row>
						  </fo:table-body>
					</fo:table>
				
                <fo:block space-after="0.1in"/>
				

            </fo:table-cell>
            
            
            </fo:table-row>
        </fo:table-body>
        </fo:table>

            <fo:block id="theEnd"/>  <#-- marks the end of the pages and used to identify page-number at the end -->
        </fo:flow>

    </fo:page-sequence>
</fo:root>
</#escape>