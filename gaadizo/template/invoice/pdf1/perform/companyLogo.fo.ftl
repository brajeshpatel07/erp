<#escape x as x?xml>

<fo:block text-align="left">
    <fo:external-graphic src="<@ofbizContentUrl>http://localhost:8085/erp/images/logo/companylogo.png</@ofbizContentUrl>" height="40px" content-width="100px"/>
</fo:block>

<fo:table table-layout="fixed" text-align="center" width="100%">
    <fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
    <fo:table-body>
        <fo:table-row>
            <fo:table-cell>
                <fo:block font-size="10pt">
                    <fo:block>${companyName!}</fo:block>
                    <#if postalAddress?exists>
                        <#if postalAddress?has_content>
                            ${setContextField("postalAddress", postalAddress)}
                            ${screens.render("component://party/widget/partymgr/PartyScreens.xml#postalAddressPdfFormatter")}
                        </#if>
                    <#else>
                        <fo:block>${uiLabelMap.CommonNoPostalAddress}</fo:block>
                        <fo:block>${uiLabelMap.CommonFor}: ${companyName}</fo:block>
                    </#if>
                     <fo:block>${phone.contactNumber?if_exists}</fo:block>
                
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
            
        
         <fo:table-row>
                      <fo:table-cell>
                         <fo:block font-size="8pt"> </fo:block>
                      </fo:table-cell>
        </fo:table-row>
		</fo:table-body>
</fo:table>

</#escape>