<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
<#escape x as x?xml>
<#if orderContactMechValueMaps?exists && orderContactMechValueMaps?has_content>
	<#list orderContactMechValueMaps as orderContactMechValueMap>
	    <#assign contactMech = orderContactMechValueMap.contactMech>
	    <#assign contactMechPurpose = orderContactMechValueMap.contactMechPurposeType>
	    <#if (contactMech.contactMechTypeId == "POSTAL_ADDRESS") && (contactMechPurpose.contactMechPurposeTypeId == "BILLING_LOCATION")>
	        <#assign billingPostalAddress = orderContactMechValueMap.postalAddress>
	    </#if>
	    <#-- if (contactMech.contactMechTypeId == "POSTAL_ADDRESS") && (contactMechPurpose.contactMechPurposeTypeId == "SHIPPING_LOCATION")>
	        <#assign shippingPostalAddress = orderContactMechValueMap.postalAddress>
	    </#if -->
	</#list>
</#if>
<#if invoice?exists && invoice?has_content>
	<#if invoice.invoiceTypeId?has_content && invoice.invoiceTypeId.equals("MARGIN_INVOICE")>
		<#assign billingPartyContactMechPurposes = delegator.findByAnd("PartyContactMechPurpose", {"partyId", invoice.partyIdFrom, "contactMechPurposeTypeId", "GENERAL_LOCATION"})>
		<#if billingPartyContactMechPurposes?has_content>
			<#assign billingPartyContactMechPurpose = Static["org.ofbiz.entity.util.EntityUtil"].getFirst(billingPartyContactMechPurposes)>
			<#assign billingPostalAddress = (delegator.findOne("PostalAddress", {"contactMechId", billingPartyContactMechPurpose.contactMechId?if_exists}, false))?if_exists />
		</#if>
		<#assign shippingPartyContactMechPurposes = delegator.findByAnd("PartyContactMechPurpose", {"partyId", invoice.partyId, "contactMechPurposeTypeId", "GENERAL_LOCATION"})>
		<#if shippingPartyContactMechPurposes?has_content>
			<#assign shippingPartyContactMechPurpose = Static["org.ofbiz.entity.util.EntityUtil"].getFirst(shippingPartyContactMechPurposes)>
			<#assign shippingPostalAddress = (delegator.findOne("PostalAddress", {"contactMechId", shippingPartyContactMechPurpose.contactMechId?if_exists}, false))?if_exists />
		</#if>
	</#if>	
</#if>
    <#assign localeGeo = Static["org.ofbiz.base.util.UtilMisc"].parseLocale("fr")/>
     <#if shippingPostalAddress.stateProvinceGeoId?exists && shippingPostalAddress.stateProvinceGeoId?has_content && !shippingPostalAddress.stateProvinceGeoId.equals("QC")  >
       <#assign localeGeo = Static["org.ofbiz.base.util.UtilMisc"].parseLocale("en")/>
    </#if>
    <#assign uiLabelMap = Static["org.ofbiz.base.util.UtilProperties"].getResourceBundleMap("scadminUiLabels", localeGeo)/>

<#assign bodyheaderFontSize="10pt">
<#-- bill to/ship to -->
 <fo:table table-layout="fixed" width="100%">
    <fo:table-column column-number="1" column-width="proportional-column-width(58.26)"/>
    <fo:table-column column-number="2" column-width="proportional-column-width(41.74)"/>
    <fo:table-body>
	    
		<fo:table-row padding-top="10pt">
	  <fo:table-cell padding="1mm" text-align="left" >
		 <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminSoldTo} </fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="1mm" text-align="left">
		 <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminShipTo} </fo:block>
	  </fo:table-cell>
		</fo:table-row>
	    
	    <#if parameters.transitId?exists && parameters.transitId?has_content>
	    	<fo:table-row padding-top="10pt" border="solid thin black">
		      <fo:table-cell padding="1mm" text-align="left">
		        <fo:block font-size="8pt">
		            <#if fromName?has_content>
		                <fo:block>${fromName?if_exists}</fo:block>
		            </#if>
		            <#if fromAddress1?has_content>
		                <fo:block>${fromAddress1?if_exists}</fo:block>
		            </#if>
		            <#if fromAddress2?has_content>
		                <fo:block>${fromAddress2?if_exists}</fo:block>
		            </#if>
		            <#if fromStateProvinceGeoId?has_content>
		                <fo:block>${fromStateProvinceGeoId?if_exists}</fo:block>
		            </#if>
		            <#if fromPostalCode?has_content>
		                <fo:block>${fromPostalCode?if_exists}</fo:block>
		            </#if>
		        </fo:block>
		      </fo:table-cell>
		      
		      <fo:table-cell padding="1mm" text-align="left"  border="solid thin black" >
		        <fo:block font-size="8pt">
		            <#if destinationName?has_content>
		                <fo:block>${destinationName?if_exists}</fo:block>
		            </#if>
		            <#if destinationAddress1?has_content>
		                <fo:block>${destinationAddress1?if_exists}</fo:block>
		            </#if>
		            <#if destinationAddress2?has_content>
		                <fo:block>${destinationAddress2?if_exists}</fo:block>
		            </#if>
		            <#if destinationPostalCode?has_content>
		                <fo:block>${destinationPostalCode?if_exists}</fo:block>
		            </#if>
		            <#if destinationStateProvinceGeoId?has_content>
		                <fo:block>${destinationStateProvinceGeoId?if_exists}</fo:block>
		            </#if>
		        </fo:block>
		      </fo:table-cell>
		    </fo:table-row>
	    <#else>
	    	<fo:table-row padding-top="10pt" border="solid thin black">
		      <fo:table-cell padding="1mm" text-align="left" height="75px">
		        <fo:block font-size="${bodyheaderFontSize}">
		            <#if billingPostalAddress?has_content>
		            
		            	<#assign billingFormattedContactPhone = ""/>
	            		<#assign billingFormattedCellNumber = ""/>
	            		<#if billingPostalAddress.contactPhone?exists && billingPostalAddress.contactPhone?has_content>
	            		<#assign billingFormattedContactPhone = Static["org.ofbiz.scadmin.util.SCMAdminUtil"].getPhoneNumberInFormat(billingPostalAddress.contactPhone)/>
	            		</#if>
	            		<#if billingPostalAddress.cellNumber?exists && billingPostalAddress.cellNumber?has_content>
	            		<#assign billingFormattedCellNumber = Static["org.ofbiz.scadmin.util.SCMAdminUtil"].getPhoneNumberInFormat(billingPostalAddress.cellNumber)/>
	            		</#if>
		            	
		            	<#if invoice?exists && invoice?has_content>
							<#if invoice.invoiceTypeId?has_content && invoice.invoiceTypeId.equals("MARGIN_INVOICE")>
								<#assign partyGroup = (delegator.findOne("PartyGroup", {"partyId", partyIdFrom?if_exists}, false))?if_exists />
								<#if partyGroup.groupName?has_content>
									<#assign toName = partyGroup.groupName>
								</#if>
								<#if toName?has_content><fo:block>${toName?if_exists}</fo:block></#if>
							<#else>
								<#if billingPostalAddress.toName?has_content><fo:block>${billingPostalAddress.toName?if_exists}</fo:block></#if>	
							</#if>
						</#if>
		               
		                <fo:block>${billingPostalAddress.address1?if_exists}</fo:block>
		                <#if billingPostalAddress.address2?has_content><fo:block>${billingPostalAddress.address2?if_exists}</fo:block></#if>
		                <#if billingPostalAddress.address3?has_content><fo:block>${billingPostalAddress.address3?if_exists}</fo:block></#if>
		                <#if billingPostalAddress.address4?has_content><fo:block>${billingPostalAddress.address4?if_exists}</fo:block></#if>
		                <fo:block>
		                    <#assign stateGeo = (delegator.findOne("Geo", {"geoId", billingPostalAddress.stateProvinceGeoId?if_exists}, false))?if_exists />
		                    ${billingPostalAddress.city?if_exists}<#if stateGeo?has_content>, ${stateGeo.geoName?if_exists}</#if><#if billingPostalAddress.postalCode?has_content>, ${billingPostalAddress.postalCode?if_exists}</#if>
		                </fo:block>
		                <fo:block>
		                    <#assign countryGeo = (delegator.findOne("Geo", {"geoId", billingPostalAddress.countryGeoId?if_exists}, false))?if_exists />
		                    <#if countryGeo?has_content>${countryGeo.geoName?if_exists}</#if>
		                </fo:block>
		                <#if billingPostalAddress.contactPhone?has_content><fo:block >${billingFormattedContactPhone?if_exists}</fo:block></#if>
		             	<#if billingPostalAddress.cellNumber?has_content><fo:block >${billingFormattedCellNumber?if_exists}</fo:block></#if>
		            </#if>
		        </fo:block>
		      </fo:table-cell>
		      
		      <fo:table-cell padding="1mm" text-align="left"  border="solid thin black">
		        <fo:block font-size="${bodyheaderFontSize}">
		            <#if shippingPostalAddress?has_content>
		            
		            	<#assign shippingFormattedContactPhone = ""/>
	            		<#assign shippingFormattedCellNumber = ""/>
	            		<#if shippingPostalAddress.contactPhone?exists && shippingPostalAddress.contactPhone?has_content>
	            		<#assign shippingFormattedContactPhone = Static["org.ofbiz.scadmin.util.SCMAdminUtil"].getPhoneNumberInFormat(shippingPostalAddress.contactPhone)/>
	            		</#if>
	            		<#if shippingPostalAddress.cellNumber?exists && shippingPostalAddress.cellNumber?has_content>
	            		<#assign shippingFormattedCellNumber = Static["org.ofbiz.scadmin.util.SCMAdminUtil"].getPhoneNumberInFormat(shippingPostalAddress.cellNumber)/>
	            		</#if>
		            
		                <#if shippingPostalAddress.toName?has_content><fo:block>${shippingPostalAddress.toName?if_exists}</fo:block></#if>
		                <#if shippingPostalAddress.attnName?has_content><fo:block>${shippingPostalAddress.attnName?if_exists}</fo:block></#if>
		                <fo:block>${shippingPostalAddress.address1?if_exists}</fo:block>
		                <#if shippingPostalAddress.address2?has_content><fo:block>${shippingPostalAddress.address2?if_exists}</fo:block></#if>
		                <#if shippingPostalAddress.address3?has_content><fo:block>${shippingPostalAddress.address3?if_exists}</fo:block></#if>
		                <#if shippingPostalAddress.address4?has_content><fo:block>${shippingPostalAddress.address4?if_exists}</fo:block></#if>
		                <fo:block>
		                    <#assign stateGeo = (delegator.findOne("Geo", {"geoId", shippingPostalAddress.stateProvinceGeoId?if_exists}, false))?if_exists />
		                    ${shippingPostalAddress.city?if_exists}<#if stateGeo?has_content>, ${stateGeo.geoName?if_exists}</#if><#if shippingPostalAddress.postalCode?has_content>, ${shippingPostalAddress.postalCode?if_exists}</#if>
		                </fo:block>
		                <fo:block>
		                    <#assign countryGeo = (delegator.findOne("Geo", {"geoId", shippingPostalAddress.countryGeoId?if_exists}, false))?if_exists />
		                    <#if countryGeo?has_content>${countryGeo.geoName?if_exists}</#if>
		                </fo:block>
		                <#if shippingPostalAddress.contactPhone?has_content><fo:block >${shippingFormattedContactPhone?if_exists}</fo:block></#if>
		             	<#if shippingPostalAddress.cellNumber?has_content><fo:block >${shippingFormattedCellNumber?if_exists}</fo:block></#if>
		            </#if>
		        </fo:block>
		      </fo:table-cell>
		    </fo:table-row>
	    </#if>
	    
    </fo:table-body>
</fo:table>
<#-- /bill to/ship to -->
</#escape>
