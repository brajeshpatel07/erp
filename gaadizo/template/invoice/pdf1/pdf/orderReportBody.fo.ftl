<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->

<#assign invoiceItemSize = 0/>
<#escape x as x?xml>
<#-- table -->
	<#--if orderContactMechValueMaps?exists && orderContactMechValueMaps?has_content>
	    <#list orderContactMechValueMaps as orderContactMechValueMap>
	        <#assign contactMech = orderContactMechValueMap.contactMech>
	        <#assign contactMechPurpose = orderContactMechValueMap.contactMechPurposeType>
	        <#if (contactMech.contactMechTypeId == "POSTAL_ADDRESS") && (contactMechPurpose.contactMechPurposeTypeId == "SHIPPING_LOCATION")>
	            <#assign shippingPostalAddress = orderContactMechValueMap.postalAddress>
	        </#if>
	    </#list>
    </#if -->
    <#assign localeGeo = Static["org.ofbiz.base.util.UtilMisc"].parseLocale("fr")/>
    <#if shippingPostalAddress.stateProvinceGeoId?exists && shippingPostalAddress.stateProvinceGeoId?has_content && !shippingPostalAddress.stateProvinceGeoId.equals("QC")  >
       <#assign localeGeo = Static["org.ofbiz.base.util.UtilMisc"].parseLocale("en")/>
    </#if>
    <#assign uiLabelMap = Static["org.ofbiz.base.util.UtilProperties"].getResourceBundleMap("scadminUiLabels", localeGeo)/>
 <fo:table table-layout="fixed" width="100%" margin-top="2.5mm">
    <fo:table-column column-number="1" column-width="proportional-column-width(11)"/>
    <fo:table-column column-number="2" column-width="proportional-column-width(11)"/>
    <fo:table-column column-number="3" column-width="proportional-column-width(10)"/>
    <fo:table-column column-number="4" column-width="proportional-column-width(15)"/>
    <fo:table-column column-number="5" column-width="proportional-column-width(20)"/>
    <fo:table-column column-number="6" column-width="proportional-column-width(6)"/>
    <fo:table-column column-number="7" column-width="proportional-column-width(12)"/>
    <fo:table-column column-number="8" column-width="proportional-column-width(7)"/>
    <fo:table-column column-number="9" column-width="proportional-column-width(12)"/>
    <fo:table-header>
	    <#-- header row -->
	    <fo:table-row background-color="#DFDFDF" padding-top="10pt">
    		<fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="2"  >
        	 <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminDate}</fo:block>
      		</fo:table-cell>

	      	<fo:table-cell padding="1mm" border="solid thin black" text-align="center"  >
	        	 <fo:block font-size="8pt" font-weight="bold">PAYMENT TERM</fo:block>
	      	</fo:table-cell>
			<fo:table-cell padding="1mm" border="solid thin black" text-align="center" >
	        	 <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminTaxExcNo}</fo:block>
	      	</fo:table-cell>
	      	<fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="2" >
	        	 <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminBillNo}</fo:block>
	      	</fo:table-cell>
	      	<fo:table-cell padding="1mm" border="solid thin black" text-align="center" >
	        	 <fo:block font-size="8pt" font-weight="bold">Load #</fo:block>
	      	</fo:table-cell>
	      	<fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="2" >
	         	<fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminPoNo} </fo:block>
	      	</fo:table-cell>
	    </fo:table-row>
	    <#-- /header row -->
	    <#-- header info row -->
	    <fo:table-row>
    		<fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="2" >
	        	<fo:block font-size="8pt">
		        	<#assign dateFormat = Static["java.text.DateFormat"].MEDIUM>
					<#assign invoiceDateFormatted = Static["java.text.DateFormat"].getDateInstance(dateFormat,locale).format(invoiceDate)>
					${invoiceDateFormatted!}
				</fo:block>
      		</fo:table-cell>
			<fo:table-cell padding="1mm" border="solid thin black" text-align="center" >
	         	<fo:block font-size="8pt">
	         	<#if orderTerms?exists && orderTerms?has_content && orderTerms.size() gt 0>
				    <#assign orderTerm= orderTerms.get(0) />
				        <fo:block font-size="8pt">
		            		${orderTerm.getRelatedOne("TermType").get("description",locale)} <#--${orderTerm.termDays?default("")} -->
		        		</fo:block>
		    		
		    	</#if>
	         	</fo:block>
	      	</fo:table-cell>
	      	<fo:table-cell padding="1mm" border="solid thin black" text-align="center" >
	         	<fo:block font-size="8pt">${taxExclNumber!}</fo:block>
	      	</fo:table-cell>
	      	
	      	<fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="2" >
	        	 <fo:block font-size="8pt"><#if shipmentIds?exists && shipmentIds?has_content>${shipmentIds!}</#if></fo:block>
	      	</fo:table-cell>
	      	<fo:table-cell padding="1mm" border="solid thin black" text-align="center">
	         	<fo:block font-size="8pt">${shipmentLoadNumber?default('NA')}</fo:block>
	      	</fo:table-cell>
	      	<fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="2" >
	         	<fo:block font-size="8pt"><#if invoice?exists && invoice.poNumber?has_content>${invoice.poNumber!}</#if></fo:block>
	      	</fo:table-cell>
	    </fo:table-row>
       	<fo:table-row background-color="#DFDFDF" padding-top="10pt">
	        <#if parameters.transitId?exists && parameters.transitId?has_content>
	    		<fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="1">
	        	 <fo:block font-size="8pt" font-weight="bold">Transit no</fo:block>
	      		</fo:table-cell>
	    	<#else>
	    		<fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="1">
	        	 <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminOrdeNo}</fo:block>
	      		</fo:table-cell>
	    	</#if>
	      	<fo:table-cell padding="1mm" border="solid thin black" text-align="center"  >
	        	 <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminFOB}</fo:block>
	      	</fo:table-cell>
	      	<fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="1">
	        	 <fo:block font-size="8pt" font-weight="bold">SHIPMENT TERM</fo:block>
	      	</fo:table-cell>
			<fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="2">
	        	 <fo:block font-size="8pt" font-weight="bold">JOB NAME</fo:block>
	      	</fo:table-cell>
			<fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="4">
	        	 <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminShipVia}</fo:block>
	      	</fo:table-cell>


	    </fo:table-row>
	    <fo:table-row padding-top="10pt">
	      <#if parameters.transitId?exists && parameters.transitId?has_content>
	    		<fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="1">
	        	<fo:block font-size="8pt">${parameters.transitId!}</fo:block>
	      		</fo:table-cell>
	    	<#else>
	    		<fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="1" >
	        	<fo:block font-size="8pt">${orderId!}</fo:block>
	      		</fo:table-cell>
	    	</#if>
	      <#if parameters.transitId?exists && parameters.transitId?has_content>
	      	<fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="3" >
				<fo:block font-size="8pt"></fo:block>
	      	</fo:table-cell>
	      <#else>
	      	<fo:table-cell padding="1mm" border="solid thin black" text-align="center" >
	         <fo:block font-size="8pt">
			 <#if invoiceShipmentMethodType?exists && invoiceShipmentMethodType?has_content && "CUST_PICKUP" == invoiceShipmentMethodType>
				${uiLabelMap.scmadminOurYard}
			 <#else>
				${uiLabelMap.scmadminJobSite}
			 </#if>
			 </fo:block>
	      </fo:table-cell>
		  <fo:table-cell padding="1mm" border="solid thin black" text-align="center"   number-columns-spanned="1" >
	        	 <fo:block font-size="8pt">
		        	<#if shippingTerm == "Collect">
						${uiLabelMap.BOL_COLLECT}
					<#elseif (shippingTerm == "Prepaid")>
						${uiLabelMap.BOL_PREPAID}
					<#elseif shippingTerm == "Prepaid and Charge">
						${uiLabelMap.BOL_PREPAID_CHARGE}
					<#else>
						${""}
					</#if>
			  </fo:block>
	      	</fo:table-cell>
	      	<fo:table-cell padding="1mm" border="solid thin black" text-align="center"   number-columns-spanned="2" >
	        	 <fo:block font-size="8pt">${shipmentJobName!""}</fo:block>
	      	</fo:table-cell>
			<fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="4">
	         <fo:block font-size="8pt">${carrierName?if_exists}</fo:block>
	      	</fo:table-cell>
	      </#if>
	      	
	      
	    </fo:table-row>
	
    <#-- /header row -->
	    <#-- header info row -->
	    
	    <#-- /header info row -->
	    <#-- items header row -->
	    <fo:table-row background-color="#DFDFDF" padding-top="10pt">
	      <fo:table-cell padding="1mm" border="solid thin black" text-align="center" >
	         <fo:block font-size="8pt" font-weight="bold">QTY</fo:block>
	      </fo:table-cell>
	     <fo:table-cell padding="1mm" border="solid thin black" text-align="center" >
	         <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminQtyShip}</fo:block>
	      </fo:table-cell>
	      <fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="3" >
	         <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminDescription}</fo:block>
	      </fo:table-cell>
	      <fo:table-cell padding="1mm" border="solid thin black" text-align="center" >
	         <fo:block font-size="8pt" font-weight="bold">Order Item#</fo:block>
	      </fo:table-cell>
	      <fo:table-cell padding="1mm" border="solid thin black" text-align="center">
	         <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminUnitPrice}</fo:block>
	      </fo:table-cell>
	      <fo:table-cell padding="1mm" border="solid thin black" text-align="center">
	         <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminUnit}</fo:block>
	      </fo:table-cell>
	      <fo:table-cell padding="1mm" border="solid thin black" text-align="center">
	         <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminTotal}</fo:block>
	      </fo:table-cell>
	    </fo:table-row>
	</fo:table-header>
	<fo:table-body>
    <#-- /items header row -->
	    <#-- items row -->
	    <#if invoiceItems?exists && invoiceItems?has_content>
	        <#list invoiceItems as invoiceItem>
			    <fo:table-row>
			      <fo:table-cell padding="1mm" border="solid thin black" text-align="right">
			         <fo:block font-size="8pt">${itemPcsMap.get(invoiceItem.invoiceItemSeqId)}</fo:block>
			      </fo:table-cell>
			     <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
			         <fo:block font-size="8pt">${itemQtyPriceUOMMap.get(invoiceItem.invoiceItemSeqId)}</fo:block>
			      </fo:table-cell>
			      <fo:table-cell padding="1mm" border="solid thin black" text-align="left"  number-columns-spanned="3" >
			         <#assign itemDescription = invoiceItem.description?if_exists/>
			         <#if shipToStateProvinceGeoId?exists && shipToStateProvinceGeoId?has_content && shipToStateProvinceGeoId == "QC"  >
			             <#assign invoiceItemProduct = invoiceItem.getRelatedOne("Product")?if_exists>
			             <#--SK (trimed the descriptions to compare) -->
			             <#if invoiceItemProduct?has_content && invoiceItemProduct.altDescription?has_content 
						 && itemDescription.trim() == invoiceItemProduct.internalName.trim()>
			                 <#assign itemDescription = invoiceItemProduct.altDescription?if_exists/>
			             </#if>
			         </#if> 
			         <fo:block font-size="8pt">${itemDescription?if_exists}</fo:block>
					 <#if itemDescription?exists && itemDescription?has_content && (itemDescription.trim().length() gt 65)>
						<#assign invoiceItemSize = invoiceItemSize + 1 />
					</#if> 
			         <#if itemDescription?has_content >
						 <#assign invoiceItemSize = invoiceItemSize+1/>
					 </#if>
					 <#if invoiceItem.comments?has_content>
						 <#assign invoiceItemSize = invoiceItemSize+1/>
					 	<fo:block font-size="8pt">
					 	<#--  SV comments in french for freight(Shipping charge) and discount-->
					 	<#if itemOrderSeqMap.get(invoiceItem.invoiceItemSeqId).toString() == "-" >
					 		<#if invoiceItem.comments?exists && invoiceItem.comments?has_content>
						 		<#if invoiceItem.comments == "Shipping Charge">
						 			${uiLabelMap.shippingCharge}
						 		<#elseif invoiceItem.comments == "Discount" >
						 			${uiLabelMap.ShippingCharge}
						 		<#else>
						 			${invoiceItem.comments!}
						 		</#if>
					 		</#if>
					 	<#else>
					 		${invoiceItem.comments!}
					 	</#if>
					 	</fo:block>
					 	<!-- this block is added to match space of border -->
					 	<fo:block font-size="0.3pt" color="#FFFFFF">BLANK</fo:block>
				 	</#if>
			      </fo:table-cell>
			      <fo:table-cell padding="1mm" border="solid thin black" text-align="center">
			         <fo:block font-size="8pt">${itemOrderSeqMap.get(invoiceItem.invoiceItemSeqId)}</fo:block>
			      </fo:table-cell>
			      <fo:table-cell padding="1mm" border="solid thin black" text-align="right">
					<#assign price = Static["org.ofbiz.scadmin.util.SCMAdminUtil"].getPriceInDecimalFormat(invoiceItem.amount)/>
			         <fo:block font-size="8pt">$ ${price} </fo:block>
			      </fo:table-cell>
			      <fo:table-cell padding="1mm" border="solid thin black" text-align="right">
			         <fo:block font-size="8pt">${invoiceItem.productUomId!}<#if parameters.transitId?exists && parameters.transitId?has_content>PCE</#if></fo:block>
			      </fo:table-cell>
			      <fo:table-cell padding="1mm" border="solid thin black" text-align="right">
			         <fo:block font-size="8pt">
	                      $ ${itemTotalMap.get(invoiceItem.invoiceItemSeqId)} 
			         </fo:block>
			      </fo:table-cell>
			    </fo:table-row>
			</#list>
		</#if>
		
		<fo:table-row >
		      <fo:table-cell padding="1mm" border="solid thin black" border-bottom-width="0" border-top-width="0">
		      	<fo:block font-size="8pt" color="#FFFFFF">BLANK</fo:block>
		      </fo:table-cell>
		      <fo:table-cell padding="1mm" border="solid thin black" border-bottom-width="0" border-top-width="0">
		      </fo:table-cell>
		      <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" number-columns-spanned="3"  >
		      </fo:table-cell>
		      <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" >
		      </fo:table-cell>
		      <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" >
		      </fo:table-cell>
		      <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" >
		      </fo:table-cell>
		      <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" >
		      </fo:table-cell>
		</fo:table-row>

			<fo:table-row text-align="center">
				<fo:table-cell border="solid thin black" border-bottom-width="0" border-top-width="0" number-columns-spanned="1">
				</fo:table-cell>
				<fo:table-cell border="solid thin black" border-bottom-width="0" border-top-width="0" number-columns-spanned="1">
				</fo:table-cell>
				<fo:table-cell border="solid thin black" border-bottom-width="0" border-top-width="0" number-columns-spanned="3">
					<#if shippingPostalAddress.countryGeoId?exists && shippingPostalAddress.countryGeoId?has_content && shippingPostalAddress.countryGeoId.equals("USA")  >
						<fo:block font-size="10pt"  >
							${uiLabelMap.scadminUSACustomerAgreementMessage}
						</fo:block>
					</#if>
				<fo:block font-size="10pt"  >
				   
				 </fo:block>
				 
					<#if currencyUomId?exists && currencyUomId == "USD">
						<fo:block font-size="10pt">
							${uiLabelMap.scadminUSAFund}
						</fo:block>
					</#if>
				 
			   </fo:table-cell>
			  <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" >
			  </fo:table-cell>
			  <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" >
			  </fo:table-cell>
			  <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" >
			  </fo:table-cell>
			  <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" >
			  </fo:table-cell>
			 </fo:table-row>
		
			  
		<#assign lineSize = 17>
		<#if shippingPostalAddress.countryGeoId?exists && shippingPostalAddress.countryGeoId?has_content && shippingPostalAddress.countryGeoId.equals("USA")  >
				<#assign lineSize = 15>
		</#if>		
		<#if invoiceItemSize?exists>
			<#if (invoiceItemSize < lineSize)>
				<#assign numberOfItems = invoiceItemSize />
				<#assign blankLinesToDraw = (lineSize-numberOfItems) />
		    </#if>
	    <#else>
	    	<#if (invoiceItems?size < lineSize)>
				<#assign numberOfItems = invoiceItems?size />
				<#assign blankLinesToDraw = (lineSize-numberOfItems) />
		    </#if>
	    </#if>
		<#-- to draw blank lines to fill the page -->
	    <#if blankLinesToDraw?exists>
		    <#list 1..blankLinesToDraw as index>
			<#if (index == blankLinesToDraw -1) >
				<fo:table-row>
			<#else>
				<fo:table-row >
			</#if>
		      <fo:table-cell padding="1mm" border="solid thin black" border-bottom-width="0" border-top-width="0">
		      	<fo:block font-size="8pt" color="#FFFFFF">BLANK</fo:block>
		      </fo:table-cell>
		      <fo:table-cell padding="1mm" border="solid thin black" border-bottom-width="0" border-top-width="0">
		      </fo:table-cell>
		      <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" number-columns-spanned="3"  >
		      </fo:table-cell>
		      <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" >
		      </fo:table-cell>
		      <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" >
		      </fo:table-cell>
		      <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" >
		      </fo:table-cell>
		      <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" >
		      </fo:table-cell>
		    
		    </fo:table-row>
		    </#list>
	    </#if>
	    <#-- footer row -->
	    <fo:table-row>
	      <fo:table-cell text-align="center" number-columns-spanned="5"  border="solid thin black" height="69px">
	         <fo:table height="95px" >
			    <fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			    <fo:table-body>
			     	<fo:table-row>
				      <fo:table-cell padding="1mm" text-align="left">
				         <fo:block font-size="8pt">${uiLabelMap.scadminComments} </fo:block>
				      </fo:table-cell>
				   </fo:table-row>
				   <fo:table-row>
				   <#if shippingPostalAddress.stateProvinceGeoId?exists && shippingPostalAddress.stateProvinceGeoId?has_content && !shippingPostalAddress.stateProvinceGeoId.equals("QC")  >
				      <fo:table-cell padding="1mm" height="70px"  text-align="left">
					  <#else>
					  <fo:table-cell padding="1mm" height="50px"  text-align="left">
					  </#if>
						<fo:block font-size="8pt"  linefeed-treatment="preserve">
							<#if invoice?exists && invoice.comments?has_content>${invoice.comments!}</#if>
						</fo:block>
						<fo:block font-size="8pt"  linefeed-treatment="preserve">
							<#if shipmentIdForComments?exists && shipmentIdForComments?has_content> Shipments: ${shipmentIdForComments!}</#if>
						</fo:block>
				      </fo:table-cell>
				    </fo:table-row>
				    <fo:table-row>
				      <fo:table-cell padding="1mm" text-align="left">
				         <fo:block font-size="6.5pt" linefeed-treatment="preserve">${uiLabelMap.scadminConditionOfSale}
						</fo:block>
				      </fo:table-cell>
				    </fo:table-row>
				   
			    </fo:table-body>
		    </fo:table>
	      </fo:table-cell>
	      <fo:table-cell text-align="center" number-columns-spanned="4" keep-together="always"  border="solid thin black" height="69px">
	         <fo:table table-layout="fixed" width="100%">
			    <fo:table-column column-number="1" column-width="proportional-column-width(65)"/>
			    <fo:table-column column-number="2" column-width="proportional-column-width(35)"/>
			    <fo:table-body>
				    <fo:table-row>
				      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
				         <fo:block font-weight="bold" font-size="8pt">${uiLabelMap.scadminSubTotal}</fo:block>
				      </fo:table-cell>
				      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
				         <fo:block font-size="8pt">$ ${shippingSubTotal!}</fo:block>
				      </fo:table-cell>
				    </fo:table-row>
                    <fo:table-row>
                      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
                         <fo:block font-weight="bold" font-size="8pt">${uiLabelMap.scadminShipAndHandle}</fo:block>
                      </fo:table-cell>
                      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
                         <fo:block font-size="8pt">$ ${shippingAndHandling?default('0.00')}</fo:block>
                      </fo:table-cell>
                    </fo:table-row>
				    <fo:table-row>
				      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
				         <fo:block font-weight="bold" font-size="8pt">${uiLabelMap.scadminTpsGst}</fo:block>
				      </fo:table-cell>
				      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
				         <fo:block font-size="8pt">$ ${gstTax?default('0.00')}</fo:block>
				      </fo:table-cell>
				    </fo:table-row>
				    <fo:table-row>
				      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
				         <fo:block font-weight="bold" font-size="8pt">${uiLabelMap.scadminTvqQst}</fo:block>
				      </fo:table-cell>
				      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
				         <fo:block font-size="8pt">$ ${pstTax?default('0.00')}</fo:block>
				      </fo:table-cell>
				    </fo:table-row>
				    <fo:table-row>
				      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
				         <fo:block font-weight="bold" font-size="8pt">${uiLabelMap.scadminGrandTotal}</fo:block>
				      </fo:table-cell>
				      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
				         <fo:block font-size="8pt">$ ${orderGrandTotal?if_exists}</fo:block>
				      </fo:table-cell>
				    </fo:table-row>
				    <fo:table-row>
				      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" border-bottom="0pt">
				         <fo:block font-weight="bold" font-size="8pt">${uiLabelMap.scadminAmountDue}</fo:block>
				      </fo:table-cell>
				      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" border-bottom="0pt">
				         <fo:block font-size="8pt">$ ${balanceDue?default('0.00')}</fo:block>
				      </fo:table-cell>
				    </fo:table-row>
			    </fo:table-body>
		    </fo:table>
	      </fo:table-cell>
	    </fo:table-row>
	    <#-- /footer row -->
    </fo:table-body>
  </fo:table>
<#-- /table -->
</#escape>  
