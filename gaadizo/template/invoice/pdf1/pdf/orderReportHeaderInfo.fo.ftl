<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
<#escape x as x?xml>
<#-- if orderContactMechValueMaps?exists && orderContactMechValueMaps?has_content>
	    <#list orderContactMechValueMaps as orderContactMechValueMap>
	        <#assign contactMech = orderContactMechValueMap.contactMech>
	        <#assign contactMechPurpose = orderContactMechValueMap.contactMechPurposeType>
	        <#if (contactMech.contactMechTypeId == "POSTAL_ADDRESS") && (contactMechPurpose.contactMechPurposeTypeId == "SHIPPING_LOCATION")>
	            <#assign shippingPostalAddress = orderContactMechValueMap.postalAddress>
	        </#if>
	    </#list>
</#if -->
<#assign localeGeo = Static["org.ofbiz.base.util.UtilMisc"].parseLocale("fr")/>
    <#if shippingPostalAddress.stateProvinceGeoId?exists && shippingPostalAddress.stateProvinceGeoId?has_content && !shippingPostalAddress.stateProvinceGeoId.equals("QC")  >
       <#assign localeGeo = Static["org.ofbiz.base.util.UtilMisc"].parseLocale("en")/>
    </#if>
    <#assign uiLabelMap = Static["org.ofbiz.base.util.UtilProperties"].getResourceBundleMap("scadminUiLabels", localeGeo)/>
  <fo:table table-layout="fixed" width="100%">
    <fo:table-column column-width="proportional-column-width(40)"/>
    <fo:table-column column-width="proportional-column-width(60)"/>
    <fo:table-body>
    <fo:table-row>
       <fo:table-cell text-align="right" number-columns-spanned="2">
			<fo:block text-align="right">
				<fo:external-graphic src="/scadmin/static/images/logo.bmp" overflow="hidden" text-align="right" height="65px" content-height="scale-to-fit" content-width="scale-to-fit"/>
			</fo:block>
		</fo:table-cell>
    </fo:table-row>
	
	<fo:table-row border-style="solid" padding-top="10pt">
	 <fo:table-cell font-size="9pt" padding="1mm" border="solid 0.1mm black" text-align="center">
	 <fo:block>
	 	<#--  SV WOEPP-261 -->
	 	<#if parameters.previewInvoiceFlag?exists && parameters.previewInvoiceFlag=="true">
	 		PREVIEW INVOICE
	 	<#else>
	 		INVOICE
	 	</#if>
	 </fo:block>
	 </fo:table-cell>
	 <fo:table-cell font-size="9pt" padding="1mm" border="solid 0.1mm black" text-align="center">
	 <fo:block>
	 	<#--  SV WOEPP-261 -->
	 	<#if parameters.previewInvoiceFlag?exists && parameters.previewInvoiceFlag=="true">
	 		XXXXXX
	 	<#else>
	 		${invoiceId!}
	 	</#if>
	 </fo:block>
	 </fo:table-cell>
    </fo:table-row>
	
	<#if invoice.invoiceTypeId?has_content && invoice.invoiceTypeId.equals("MARGIN_INVOICE")>
	<fo:table-row>
		<fo:table-cell font-size="8pt" padding="1mm" border="solid 0.1mm black" text-align="center" number-columns-spanned="1"><fo:block>Ref. I/C Invoice #</fo:block></fo:table-cell>
		<fo:table-cell font-size="9pt" padding="1mm" border="solid 0.1mm black" text-align="center" number-columns-spanned="1"><fo:block>${invoice.referenceInvoiceId!""}</fo:block></fo:table-cell>
	</fo:table-row>
	</#if>
	
    </fo:table-body>
  </fo:table>
</#escape>
