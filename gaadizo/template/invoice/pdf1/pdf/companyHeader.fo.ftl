<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
<#escape x as x?xml>
	<#-- if orderContactMechValueMaps?exists && orderContactMechValueMaps?has_content>
	    <#list orderContactMechValueMaps as orderContactMechValueMap>
	        <#assign contactMech = orderContactMechValueMap.contactMech>
	        <#assign contactMechPurpose = orderContactMechValueMap.contactMechPurposeType>
	        <#if (contactMech.contactMechTypeId == "POSTAL_ADDRESS") && (contactMechPurpose.contactMechPurposeTypeId == "SHIPPING_LOCATION")>
	            <#assign shippingPostalAddress = orderContactMechValueMap.postalAddress>
	        </#if>
	    </#list>
	</#if -->
    <#assign localeGeo = Static["org.ofbiz.base.util.UtilMisc"].parseLocale("fr")/>
     <#if shippingPostalAddress.stateProvinceGeoId?exists && shippingPostalAddress.stateProvinceGeoId?has_content && !shippingPostalAddress.stateProvinceGeoId.equals("QC")  >
       <#assign localeGeo = Static["org.ofbiz.base.util.UtilMisc"].parseLocale("en")/>
    </#if>
    <#assign uiLabelMap = Static["org.ofbiz.base.util.UtilProperties"].getResourceBundleMap("scadminUiLabels", localeGeo)/>
    <#assign uiLabelMap = Static["org.ofbiz.base.util.UtilProperties"].getResourceBundleMap("scadminUiLabels", localeGeo)/>

<fo:table table-layout="fixed" width="100%">
    <fo:table-column column-number="1" column-width="proportional-column-width(90)"/>
    <fo:table-column column-number="2" column-width="proportional-column-width(10)"/>
    <fo:table-body>
        <fo:table-row>
            <fo:table-cell>
                <fo:block font-size="11pt" >
                    <#-- <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminCompanyName}</fo:block> -->
                    <fo:block>${companyName}</fo:block>
                    <#if postalAddress?exists>
                        <#if postalAddress?has_content>
                            ${setContextField("postalAddress", postalAddress)}
                            ${screens.render("component://party/widget/partymgr/PartyScreens.xml#postalAddressPdfFormatter")}
                        </#if>
                    <#else>
                        <fo:block>${uiLabelMap.CommonNoPostalAddress}</fo:block>
                        <fo:block>${uiLabelMap.CommonFor}: ${companyName}</fo:block>
                    </#if>
                     <fo:block>${phone.contactNumber?if_exists}</fo:block>
                
                </fo:block>
            </fo:table-cell>

        </fo:table-row>

        <#if partyGroup?exists && partyGroup.gstHstInfo?has_content>
			<fo:table-row padding-top="50pt">
			  <fo:table-cell padding-top="5px">
				 <fo:block font-size="9pt">${uiLabelMap.scadminTpsGst} : ${partyGroup.gstHstInfo }</fo:block>
			  </fo:table-cell>
			   </fo:table-row>
		</#if>
		<#if partyGroup?exists && partyGroup.pstQstInfo?has_content>
			   <fo:table-row padding-top="50pt">
			  <fo:table-cell>
				 <fo:block font-size="9pt">${uiLabelMap.scadminTvqQst} :  ${partyGroup.pstQstInfo} </fo:block>
			  </fo:table-cell>
			</fo:table-row>
		</#if>

		</fo:table-body>
</fo:table>
</#escape>
