<div class="col-md-12 col-sm-8">
<div class="rightform">
<div class="title2"><span>Invoice List</span></div>

<div class="table-responsive margintop20">
<table id="sortableData" width="100%" border="0" cellspacing="0" cellpadding="0" class="tabledetails">
  <tr>
    <th>Invoice Id</th>
    <th>Customer Name</th>
    <th>Invoice Date</th>
    <th>ACTIONS</th>
  </tr>
  <#if invoiceList?exists && invoiceList?has_content>
	<#list invoiceList as invoice>
  <tr>
    <td>${invoice.invoiceId!}</td>
    <td>${invoice.firstName!}</td>
    <td>${invoice.invoiceDate!}</td>
    <td class="action"><a target="_BLANK" href="<@ofbizUrl>InvoicePDF?invoiceId=${invoice.invoiceId}</@ofbizUrl>"><i class="fa fa-eye"></i></a> <a href="#"><i class="fa fa-edit"></i></a> <a href="#"><i class="fa fa-print"></i></a></td>
  </tr>
  </#list>
  </#if>
  
</table>
</div>

<div class="page-show row">
<div class="col-sm-5">Showing 1 to 10 of 10 entries</div>
<div class="col-sm-7 text-right"><a href="#">Previous</a> <a class="count" href="#">1</a> <a href="#">Next</a></div>
</div>

</div>