<#escape x as x?xml>
<#list orderContactMechValueMaps as orderContactMechValueMap>
    <#assign contactMech = orderContactMechValueMap.contactMech>
    <#assign contactMechPurpose = orderContactMechValueMap.contactMechPurposeType>
    <#if (contactMech.contactMechTypeId == "POSTAL_ADDRESS") && (contactMechPurpose.contactMechPurposeTypeId == "BILLING_LOCATION")>
        <#assign billingPostalAddress = orderContactMechValueMap.postalAddress>
    </#if>
    <#if (contactMech.contactMechTypeId == "POSTAL_ADDRESS") && (contactMechPurpose.contactMechPurposeTypeId == "SHIPPING_LOCATION")>
        <#assign shippingPostalAddress = orderContactMechValueMap.postalAddress>
    </#if>
</#list>
<#assign localeGeo = Static["org.ofbiz.base.util.UtilMisc"].parseLocale("en")/>
<#if orderHeader?exists && orderHeader?has_content && ("SALES_ORDER" == orderHeader.orderTypeId)>
 <#assign localeGeo = Static["org.ofbiz.base.util.UtilMisc"].parseLocale("fr")/>
    <#if stateProvinceAbbr?exists && stateProvinceAbbr?has_content && stateProvinceAbbr != "QC"  >
       <#assign localeGeo = Static["org.ofbiz.base.util.UtilMisc"].parseLocale("en")/>
    </#if>
</#if>
<#assign uiLabelMap = Static["org.ofbiz.base.util.UtilProperties"].getResourceBundleMap("scadminUiLabels", localeGeo)/>

<#-- bill to/ship to -->

	<#-- SV : WEB-2213 aad margin (Reviewer BP)-->

 <fo:table margin-top="10mm">
    <fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
    <fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
    <fo:table-column column-number="3" column-width="proportional-column-width(25)"/>
    <fo:table-column column-number="4" column-width="proportional-column-width(25)"/>
    <fo:table-body>
	    <fo:table-row padding-top="10pt">
	      <fo:table-cell padding="1mm" text-align="left" >
	      <#if orderHeader?exists && orderHeader?has_content && ("SALES_ORDER" == orderHeader.orderTypeId)>
	      	 <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminSoldTo} </fo:block>
	      <#else>
	         <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.supplierDetails} </fo:block>
	      </#if>
	      </fo:table-cell>
		  <fo:table-cell padding="1mm" text-align="left" >
	        	<fo:block font-size="8pt" font-weight="bold"></fo:block>
	      </fo:table-cell>	
		<#if orderHeader?exists && orderHeader?has_content && ("SALES_ORDER" == orderHeader.orderTypeId)>		
	      <fo:table-cell padding="1mm" text-align="left" >
	         <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminShipTo} </fo:block>
	      </fo:table-cell>
	      <#else>
	      <fo:table-cell padding="1mm" text-align="left" >
	         <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminShipTo} </fo:block>
	      </fo:table-cell>
	      </#if>
	    </fo:table-row>
	    <fo:table-row padding-top="10pt" border="solid thin black">
	      <fo:table-cell padding="1mm" text-align="left"  number-columns-spanned="2">
	        <fo:block font-size="8pt">
	            <#if billingPostalAddress?has_content>
	            	
	            	<#assign billingFormattedContactPhone = ""/>
	            	<#assign billingFormattedCellNumber = ""/>
	            	<#if billingPostalAddress.contactPhone?exists && billingPostalAddress.contactPhone?has_content>
	            	<#assign billingFormattedContactPhone = Static["org.ofbiz.scadmin.util.SCMAdminUtil"].getPhoneNumberInFormat(billingPostalAddress.contactPhone)/>
	            	</#if>
	            	<#if billingPostalAddress.cellNumber?exists && billingPostalAddress.cellNumber?has_content>
	            	<#assign billingFormattedCellNumber = Static["org.ofbiz.scadmin.util.SCMAdminUtil"].getPhoneNumberInFormat(billingPostalAddress.cellNumber)/>
	            	</#if>
	            	
	                <#if billingPostalAddress.toName?has_content><fo:block>${billingPostalAddress.toName?if_exists}</fo:block></#if>
	                <#if orderHeader?exists && orderHeader?has_content && ("SALES_ORDER" == orderHeader.orderTypeId)>
						<#if billingPostalAddress.attnName?has_content><fo:block>${billingPostalAddress.attnName?if_exists}</fo:block></#if>
					</#if>
	                <fo:block>${billingPostalAddress.address1?if_exists}</fo:block>
	                <#if billingPostalAddress.address2?has_content><fo:block>${billingPostalAddress.address2?if_exists}</fo:block></#if>
	                <#if billingPostalAddress.address3?has_content><fo:block>${billingPostalAddress.address3?if_exists}</fo:block></#if>
	                <#if billingPostalAddress.address4?has_content><fo:block>${billingPostalAddress.address4?if_exists}</fo:block></#if>
	                <fo:block>
	                    <#assign stateGeo = (delegator.findOne("Geo", {"geoId", billingPostalAddress.stateProvinceGeoId?if_exists}, false))?if_exists />
	                    ${billingPostalAddress.city?if_exists}<#if stateGeo?has_content>, ${stateGeo.geoName?if_exists}</#if>
						<#if billingPostalAddress.postalCode?has_content>, ${billingPostalAddress.postalCode?if_exists}</#if>
	                </fo:block>
	                <fo:block>
	                    <#assign countryGeo = (delegator.findOne("Geo", {"geoId", billingPostalAddress.countryGeoId?if_exists}, false))?if_exists />
	                    <#if countryGeo?has_content>${countryGeo.geoName?if_exists}</#if>
	                </fo:block>
	                 <#if billingPostalAddress.contactPhone?has_content><fo:block>${billingFormattedContactPhone?if_exists}</fo:block></#if>
		             <#if billingPostalAddress.cellNumber?has_content><fo:block >${billingFormattedCellNumber?if_exists}</fo:block></#if>
	            </#if>
	        </fo:block>
	      </fo:table-cell>
	      <fo:table-cell padding="1mm" text-align="left" border="solid thin black" number-columns-spanned="2">
	        <fo:block font-size="8pt">
	            <#if shippingPostalAddress?has_content>
	            	
	            	<#assign shippingFormattedContactPhone = ""/>
	            	<#assign shippingFormattedCellNumber = ""/>
	            	<#if shippingPostalAddress.contactPhone?exists && shippingPostalAddress.contactPhone?has_content>
	            	<#assign shippingFormattedContactPhone = Static["org.ofbiz.scadmin.util.SCMAdminUtil"].getPhoneNumberInFormat(shippingPostalAddress.contactPhone)/>
	            	</#if>
	            	<#if shippingPostalAddress.cellNumber?exists && shippingPostalAddress.cellNumber?has_content>
	            	<#assign shippingFormattedCellNumber = Static["org.ofbiz.scadmin.util.SCMAdminUtil"].getPhoneNumberInFormat(shippingPostalAddress.cellNumber)/>
	            	</#if>
	            
	                <#if shippingPostalAddress.toName?has_content><fo:block>${shippingPostalAddress.toName?if_exists}</fo:block></#if>
	                <#if shippingPostalAddress.attnName?has_content><fo:block>${shippingPostalAddress.attnName?if_exists}</fo:block></#if>
	                <fo:block>${shippingPostalAddress.address1?if_exists}</fo:block>
	                <#if shippingPostalAddress.address2?has_content><fo:block>${shippingPostalAddress.address2?if_exists}</fo:block></#if>
	                <#if shippingPostalAddress.address3?has_content><fo:block>${shippingPostalAddress.address3?if_exists}</fo:block></#if>
	                <#if shippingPostalAddress.address4?has_content><fo:block>${shippingPostalAddress.address4?if_exists}</fo:block></#if>
	                <fo:block>
	                    <#assign stateGeo = (delegator.findOne("Geo", {"geoId", shippingPostalAddress.stateProvinceGeoId?if_exists}, false))?if_exists />
	                    ${shippingPostalAddress.city?if_exists}<#if stateGeo?has_content>, ${stateGeo.geoName?if_exists}</#if>
						<#if shippingPostalAddress.postalCode?has_content>, ${shippingPostalAddress.postalCode?if_exists}</#if>
	                </fo:block>
	                <fo:block>
	                    <#assign countryGeo = (delegator.findOne("Geo", {"geoId", shippingPostalAddress.countryGeoId?if_exists}, false))?if_exists />
	                    <#if countryGeo?has_content>${countryGeo.geoName?if_exists}</#if>
	                </fo:block>
	                <#if shippingPostalAddress.contactPhone?has_content><fo:block >${shippingFormattedContactPhone?if_exists}</fo:block></#if>
		             <#if shippingPostalAddress.cellNumber?has_content><fo:block >${shippingFormattedCellNumber?if_exists}</fo:block></#if>
	            </#if>
	        </fo:block>
	      </fo:table-cell>
		  </fo:table-row>	
    </fo:table-body>
</fo:table>
<#-- /bill to/ship to -->
</#escape>
