<#escape x as x?xml>
  <fo:table >
    <fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
    <fo:table-body>
	<fo:table-row>
       <fo:table-cell text-align="left" number-columns-spanned="1">
				<fo:block text-align="left">
				    <#if logoImageUrl?exists && logoImageUrl?has_content><fo:external-graphic src="http://localhost:8085${logoImageUrl!}" overflow="hidden" text-align="right" height="40px" content-height="scale-to-fit" content-width="scale-to-fit"/></#if>
				</fo:block>
            </fo:table-cell>
    </fo:table-row>
    </fo:table-body>
  </fo:table>
</#escape>