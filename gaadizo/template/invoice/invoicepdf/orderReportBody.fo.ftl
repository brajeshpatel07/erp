<#escape x as x?xml>
<#assign localeGeo = Static["org.ofbiz.base.util.UtilMisc"].parseLocale("en")/>
<#if orderHeader?exists && orderHeader?has_content && ("SALES_ORDER" == orderHeader.orderTypeId)>
 <#assign localeGeo = Static["org.ofbiz.base.util.UtilMisc"].parseLocale("fr")/>
    <#if stateProvinceAbbr?exists && stateProvinceAbbr?has_content && stateProvinceAbbr != "QC"  >
       <#assign localeGeo = Static["org.ofbiz.base.util.UtilMisc"].parseLocale("en")/>
    </#if>
</#if>
<#assign uiLabelMap = Static["org.ofbiz.base.util.UtilProperties"].getResourceBundleMap("scadminUiLabels", localeGeo)/>
<#-- table -->
 <fo:table margin-top="5mm">
    <fo:table-column column-number="1" column-width="6%"/>
    <fo:table-column column-number="2" column-width="7%"/>
    <fo:table-column column-number="3" column-width="9%"/>
    <fo:table-column column-number="4" column-width="7%"/>
    <fo:table-column column-number="5" column-width="7%"/>
    <fo:table-column column-number="6" column-width="7%"/>
    <fo:table-column column-number="7" column-width="8%"/>
    <fo:table-column column-number="8" column-width="8%"/>
    <fo:table-column column-number="9" column-width="11%"/>
    <fo:table-column column-number="10" column-width="12%"/>
    <fo:table-column column-number="11" column-width="12%"/>
    <fo:table-column column-number="12" column-width="10%"/>
    <fo:table-header>
        <#-- header row -->
        <#if orderHeader?exists && orderHeader?has_content && ("SALES_ORDER" == orderHeader.orderTypeId)>
        <fo:table-row background-color="#DFDFDF" padding-top="10pt">
        
          <fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="4" >
             <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.Agent}</fo:block>
          </fo:table-cell>
            
          <fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="2" >
             <fo:block font-size="8pt" font-weight="bold">PAYMENT TERM</fo:block>
          </fo:table-cell>
                  
           <fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="3" >
             <fo:block font-size="8pt" font-weight="bold">SHIPMENT TERM</fo:block>
          </fo:table-cell>

		  <fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="3" >
             <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminShipVia}</fo:block>
          </fo:table-cell>
          
        </fo:table-row>
        
        <#-- /header row -->
        <#-- header info row -->
        <fo:table-row>
        
          <fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="4" >
             <fo:block font-size="8pt">${createdByAgentName?if_exists}</fo:block>
          </fo:table-cell>
          
          <fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="2" >
             <fo:block font-size="8pt" text-align="center" >
               <#if orderTerms?exists?has_content && orderTerms.size() gt 0>
                    <#list orderTerms as orderTerm>
                        <fo:block text-indent="0.2in">
                            ${orderTerm.getRelatedOne("TermType").get("description",locale)}<#-- ${orderTerm.termDays?default("")}-->
                        </fo:block>
                    </#list>
                </#if>
             </fo:block>
          </fo:table-cell>
          <fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="3" >
             <fo:block font-size="8pt">
			<#-- SV add French Description  -->
				<#if shippingTerm?exists && shippingTerm == "Collect">
						${uiLabelMap.BOL_COLLECT}
					<#elseif shippingTerm?exists && shippingTerm == "Prepaid">
						${uiLabelMap.BOL_PREPAID}
					<#elseif shippingTerm?exists && shippingTerm == "Prepaid and Charge">
						${uiLabelMap.BOL_PREPAID_CHARGE}
					<#else>
						${""}
					</#if>
			 </fo:block>
          </fo:table-cell>
          <fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="3" >
          
                   <#assign shippingMethod = shippingMethodName/>
                   <#if shipToStateProvinceGeoId?exists && shipToStateProvinceGeoId?has_content && shipToStateProvinceGeoId == "QC"  >
                   <#if shipmentMethodTypeId?exists && shipmentMethodTypeId?has_content>
                     <#assign shippingMethod =  uiLabelMap.get(shipmentMethodTypeId) />
                   <#else>
                     <#assign shippingMethod = shippingMethodName/>
                   </#if>
                   </#if>
             <fo:block font-size="8pt">${shippingMethod?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>
        
        <#-- SV : WEB-2213 Add Shipping Instruction ((Reviewer BP))-->

        <fo:table-row >
            <fo:table-cell padding="1mm" background-color="#DFDFDF" border="solid thin black" text-align="center" number-columns-spanned="4" >
            <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.SOShipIns}</fo:block>
          </fo:table-cell>
          
          <fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="8" >
             <#if shipGroup?exists && shipGroup?has_content>
             <fo:block font-size="8pt" font-weight="bold">${shipGroup.shippingInstructions?if_exists!""}</fo:block>
             </#if>
          </fo:table-cell>
        </fo:table-row>
        
        </#if>
        <#if orderHeader?exists && orderHeader?has_content && ("PURCHASE_ORDER" == orderHeader.orderTypeId)>
        <fo:table-row padding-top="10pt">
            <fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="2">
                 <fo:block font-weight="bold" font-size="8pt">Delivery Terms</fo:block>
            </fo:table-cell>
          <fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="6">
             <fo:block font-size="8pt">
                ${fobTerms?if_exists}
             </fo:block>
          </fo:table-cell>
		    <fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="2">
                 <fo:block font-weight="bold" font-size="8pt">Currency</fo:block>
            </fo:table-cell>
          <fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="2">
             <fo:block font-size="8pt">
                ${currencyUOMInPO?if_exists}
             </fo:block>
          </fo:table-cell>
        </fo:table-row>
        </#if>
        <#-- /header info row -->
        <#-- items header row -->
        <fo:table-row background-color="#DFDFDF" padding-top="10pt">
           <fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="1" >
                <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.ITEM_SEQ}</fo:block>
           </fo:table-cell>
          <#if orderHeader?exists && orderHeader?has_content && ("SALES_ORDER" == orderHeader.orderTypeId)>
           <fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="2" >
             <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.SOProductLOC!""}</fo:block>
           </fo:table-cell>
           <fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="6" >
           		<fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminDescription}</fo:block>
	       </fo:table-cell>
          <#else>

			  <#-- WEB-2291 SV add  ReleaseNumber -->
			<fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="2" >
	          	<fo:block font-size="8pt" font-weight="bold">${uiLabelMap.PO_ReleaseNumber}</fo:block>
	          </fo:table-cell>
              <fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="6" >
	          	<fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminDescription}</fo:block>
	          </fo:table-cell>
	         
          </#if>
          
          
          <fo:table-cell padding="1mm" border="solid thin black" text-align="center" >
             <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminUnit}</fo:block>
          </fo:table-cell>
          <fo:table-cell padding="1mm" border="solid thin black" text-align="center" >
             <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminUnitPrice} ($)</fo:block>
          </fo:table-cell>
          <fo:table-cell padding="1mm" border="solid thin black" text-align="center">
             <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminTotal} ($)</fo:block>
          </fo:table-cell>
        </fo:table-row>
      </fo:table-header>
	<fo:table-body>  
        <#-- /items header row -->
        <#-- items row -->
        <#assign itemLineSize = 0 />
        <#assign itemListSize = orderItemList?size * 2/>
        <#list orderItemList as orderItem>
			<#if (orderItem.statusId)?exists && (orderItem.statusId)?has_content && ("ITEM_CANCELLED" != orderItem.statusId)>
				<#if (orderItem.orderItemSeqId)?exists && (orderItem.orderItemSeqId)?has_content>
					<#assign orderItemsList = orderItemList >
					<#assign itemIndex = Static["org.ofbiz.scadmin.util.SCMAdminUtil"].getSequenceFromOrderItemSeqId(orderItemsList , orderItem.orderItemSeqId )>
				</#if>
				
				<#assign orderItemType = orderItem.getRelatedOne("OrderItemType")?if_exists>
				<#assign productId = orderItem.productId?if_exists>
				<#assign remainingQuantity = (orderItem.quantity?default(0) - orderItem.cancelQuantity?default(0))>
				<#assign itemPriceTotal = Static["org.ofbiz.scadmin.util.SCMAdminUtil"].getPriceInDecimalFormat(Static["org.ofbiz.scadmin.util.ScAdminUtility"].getItemSubTotalUtility(Static["org.ofbiz.base.util.UtilMisc"].toMap
																								("delegator",delegator,"quantityUOM",orderItem.getString("quantityUomId"),
																								"priceUOM",orderItem.getString("productUomId"),
																								"quantity",orderItem.getBigDecimal("quantity"),
																								"price",orderItem.getBigDecimal("unitPrice"),
																								"productId",orderItem.getString("productId"))))> 
														
				<fo:table-row keep-together.within-column="always">
				  <fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="1"  >
					 <fo:block font-size="8pt">${itemIndex!}</fo:block>
				  </fo:table-cell>
				<#if orderHeader?exists && orderHeader?has_content && ("SALES_ORDER" == orderHeader.orderTypeId)>
				  <fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="2"  >
					 <fo:block font-size="7pt">${orderItem.locationSeqId?default("N/A")}</fo:block>
				  </fo:table-cell>
				  <fo:table-cell padding="1mm" border="solid thin black" text-align="left"  number-columns-spanned="6" >
					 <#assign itemDescription = orderItem.itemDescription?if_exists/>
					 <#if shipToStateProvinceGeoId?exists && shipToStateProvinceGeoId?has_content && shipToStateProvinceGeoId == "QC"  >
						 <#assign orderItemProduct = orderItem.getRelatedOne("Product")?if_exists>
						 <#assign orderItemDescription = orderItemProduct.internalName?if_exists>
						 <#if orderItemProduct?has_content && orderItemProduct.altDescription?has_content && (itemDescription.trim() == orderItemDescription.trim())>
							 <#assign itemDescription = orderItemProduct.altDescription?if_exists/>
						 </#if>
					 </#if>
					 <#if itemDescription?exists && itemDescription?has_content && (itemDescription.trim().length() gt 65)>
						<#assign itemLineSize = itemLineSize + 2>
					 </#if> 
					 
					 <fo:block font-size="8pt" white-space-treatment="preserve">${itemDescription.trim()?if_exists}</fo:block>
					 <#if orderItem.comments?has_content && orderItem.comments.trim()?has_content>
						<fo:block font-size="8pt"  linefeed-treatment="preserve" 
							white-space-treatment="preserve" white-space-collapse="false">${orderItem.comments.trim()!}</fo:block>
							 <#assign itemLineSize = itemLineSize + 1>
							 
							 <#if orderItem.comments?exists && orderItem.comments?has_content && (orderItem.comments.trim().length() gt 65)>
								<#assign itemLineSize = itemLineSize + 1>
							 </#if> 
					</#if>
					<#if orderItem.heatNumber?has_content && orderItem.heatNumber.trim()?has_content>
						<fo:block font-size="8pt"  linefeed-treatment="preserve" 
							white-space-treatment="preserve" white-space-collapse="false">HT#: ${orderItem.heatNumber.trim()!}</fo:block>
						<#assign itemLineSize = itemLineSize + 1>
						 <#if orderItem.heatNumber?exists && orderItem.heatNumber?has_content && (orderItem.heatNumber.trim().length() gt 65)>
								<#assign itemLineSize = itemLineSize + 1>
							 </#if> 
					</#if>
					</fo:table-cell>
				  <#else>

					<#-- WEB-2291 SV add  ReleaseNumber info(Reviewer BP) -->
					<fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="2" >
					 <fo:block font-size="8pt">${orderItem.releaseNumber?if_exists!""}</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1mm" border="solid thin black" text-align="left"  number-columns-spanned="6" >
					 <fo:block font-size="8pt" white-space-treatment="preserve">${orderItem.itemDescription?if_exists}</fo:block>
					</fo:table-cell>

				   </#if>
				  
				  <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
						<#if ("PCE" == orderItem.quantityUomId || "FT" == orderItem.quantityUomId || "LBS" == orderItem.quantityUomId)>
							<#assign orderQty = Static["org.ofbiz.scadmin.util.SCMAdminUtil"].getValueInWholeFormat(orderItem.quantity)/>
							<fo:block font-size="8pt">${orderQty} ${orderItem.quantityUomId!}</fo:block>
						<#else>
							<#assign orderQty = Static["org.ofbiz.scadmin.util.SCMAdminUtil"].getValueInQuantityFormat(orderItem.quantity)/>
							<fo:block font-size="8pt">${orderQty} ${orderItem.quantityUomId!}</fo:block>
						</#if>
				  </fo:table-cell>
				  <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
					<#assign price = Static["org.ofbiz.scadmin.util.SCMAdminUtil"].getPriceInDecimalFormat(orderItem.unitPrice)/>
					 <fo:block font-size="8pt">${price?default('0')} / ${orderItem.productUomId?default('FT')}</fo:block>
				  </fo:table-cell>
				  <fo:table-cell padding="1mm" border="solid thin black" text-align="right">
					 <fo:block font-size="8pt">${itemPriceTotal}</fo:block>
				  </fo:table-cell>
				</fo:table-row>
			</#if>
        </#list>
        <#-- to draw blank lines to fill the page -->
        <#assign commentLineSize = 0 />
        <#if commentLines?exists && commentLines?has_content>
            <#assign commentLineSize = (commentLines.size() + 5) />
            <#assign blankLinesToDraw = commentLineSize />
        </#if>
        <#assign lineSize = 40> 
        <#if orderHeader?exists && orderHeader?has_content && orderHeader.orderTypeId.equals("SALES_ORDER")>
           <#if ("USD" == orderHeader.currencyUom) >
				<#assign itemLineSize = itemLineSize + 1/>
			</#if> 
			<#assign lineSize = 36> 
        </#if>
		
        <#if (itemListSize < lineSize)>
            <#assign numberOfItems = itemListSize + itemLineSize + commentsSize/>
			
            <#assign blankLinesToDraw = (lineSize-numberOfItems) />
            <#if (blankLinesToDraw < commentLineSize)>
                <#assign blankLinesToDraw = (commentLineSize) />
            </#if>  
			
        </#if> 
		
        <#if blankLinesToDraw?exists>
            <#list 1..blankLinesToDraw as index>
			<#if (index == blankLinesToDraw) >
				<fo:table-row  border-bottom="solid thin black">
			<#else>
				<fo:table-row >
			</#if>
              <fo:table-cell padding="1mm" border="solid thin black" border-bottom-width="0" border-top-width="0" number-columns-spanned="1" >
              </fo:table-cell>
            <#if orderHeader?exists && orderHeader?has_content && ("SALES_ORDER" == orderHeader.orderTypeId)>
              <fo:table-cell padding="1mm" border="solid thin black" border-bottom-width="0" border-top-width="0" number-columns-spanned="2" >
              </fo:table-cell>
              <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" number-columns-spanned="6" >
               <#if (index == blankLinesToDraw - 2) && orderHeader?exists && orderHeader?has_content 
                    && ("SALES_ORDER" == orderHeader.orderTypeId) && ("USD" == orderHeader.currencyUom)>
                    <fo:block font-size="10pt" text-align="center">
                    ${uiLabelMap.scadminUSAFund}
                     </fo:block>    
              </#if>
              </fo:table-cell>        
            <#else>
				<fo:table-cell padding="1mm" border="solid thin black" border-bottom-width="0" border-top-width="0" number-columns-spanned="2" >
              </fo:table-cell>
              <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" number-columns-spanned="6" >
              </fo:table-cell>
            </#if>
              
              <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" >
              </fo:table-cell>
              <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" >
              </fo:table-cell>
              
              <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" >
              </fo:table-cell>
            </fo:table-row>
            </#list>
        </#if> 
        <#-- footer row -->
        <fo:table-row >

        <#-- SV : WEB-2213 DRC# 410 Order PDF -  Added Order Comments (Reviewer BP) -->

        <fo:table-cell number-columns-spanned="8" border="solid thin black" >
            <fo:table height="105px" >
                <fo:table-column column-number="1" column-width="100%"/>
                <fo:table-body>
                    <fo:table-row>
                      <fo:table-cell padding="1mm" text-align="left" >
                         <fo:block font-size="8pt" font-weight="bold" white-space-treatment="preserve" >${uiLabelMap.scadminOrderComments} </fo:block>
                      </fo:table-cell>
                   </fo:table-row>
                   <fo:table-row>
                      <fo:table-cell padding="1mm" text-align="left">

					<#-- SV WES-114 Showing Cooment line wise --> 

                      
                      	  <#list commentLines as commentLine>
	                      	<fo:block font-size="8pt" font-weight="bold" white-space-treatment="preserve">${commentLine?if_exists!""}</fo:block>
	                      </#list>
	                  
                      </fo:table-cell>
                    </fo:table-row>                
                </fo:table-body>
            </fo:table>
          </fo:table-cell>

          <fo:table-cell text-align="center" number-columns-spanned="4" keep-together="always">
             <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-number="1" column-width="55%"/>
                <fo:table-column column-number="2" column-width="45%"/>
                <fo:table-body>
                    <#if orderHeader?exists && orderHeader?has_content && ("SALES_ORDER" == orderHeader.orderTypeId)>
                    <fo:table-row>
                      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
                         <fo:block font-weight="bold" font-size="8pt">${uiLabelMap.scadminSubTotal}</fo:block>
                      </fo:table-cell>
                      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
                         <fo:block font-size="8pt">$ ${orderSubTotal!}</fo:block>
                      </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row>
                      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
                         <fo:block font-weight="bold" font-size="8pt">${uiLabelMap.scadminGlobalDis}</fo:block>
                      </fo:table-cell>
                      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
                         <fo:block font-size="8pt">$ ${globalDiscount?default('0.00')}</fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
                         <fo:block font-weight="bold" font-size="8pt">${uiLabelMap.scadminShipAndHandle}</fo:block>
                      </fo:table-cell>
                      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
                         <fo:block font-size="8pt">$ ${shippingAndHandling?default('0.00')}</fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
                         <fo:block font-weight="bold" font-size="8pt">${uiLabelMap.scadminTpsGst}</fo:block>
                      </fo:table-cell>
                      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
                         <fo:block font-size="8pt">$ ${gstTax?default('0.00')}</fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
                         <fo:block font-weight="bold" font-size="8pt">${uiLabelMap.scadminTvqQst}</fo:block>
                      </fo:table-cell>
                      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
                         <fo:block font-size="8pt">$ ${pstTax?default('0.00')}</fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row>
                      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
                         <fo:block font-weight="bold" font-size="8pt">${uiLabelMap.scadminDeposit}</fo:block>
                      </fo:table-cell>
                      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
                         <fo:block font-size="8pt">$ ${depositTotal?default('0.00')}</fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                    </#if>
                    
                    <#-- SV : WEB-2213 DRC# 410 Order PDF - Changes, Reference Date PO, Field Move, Remove Order Date below address, Added Order Comments (Reviewer BP) -->

                    <#if orderHeader?exists && orderHeader?has_content && ("PURCHASE_ORDER" == orderHeader.orderTypeId)>
                    <fo:table-row height='81px'>
                      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" number-columns-spanned="2" >
                         <fo:block font-weight="bold" font-size="8pt"></fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                    </#if>
                    <fo:table-row>
                      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
                         <fo:block font-weight="bold" font-size="8pt">${uiLabelMap.scadminGrandTotal}</fo:block>
                      </fo:table-cell>
                      <fo:table-cell padding="1mm" border="solid thin black" text-align="right" >
                         <fo:block font-size="8pt">$ ${orderGrandTotal?if_exists}</fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
          </fo:table-cell>
        </fo:table-row>
        <#-- /footer row -->
    </fo:table-body>
  </fo:table>
<#-- /table -->
</#escape>
