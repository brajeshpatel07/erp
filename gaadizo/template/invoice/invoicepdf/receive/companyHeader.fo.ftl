<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
<#escape x as x?xml>
	<#if orderContactMechValueMaps?exists && orderContactMechValueMaps?has_content>
	    <#list orderContactMechValueMaps as orderContactMechValueMap>
	        <#assign contactMech = orderContactMechValueMap.contactMech>
	        <#assign contactMechPurpose = orderContactMechValueMap.contactMechPurposeType>
	        <#if (contactMech.contactMechTypeId == "POSTAL_ADDRESS") && (contactMechPurpose.contactMechPurposeTypeId == "SHIPPING_LOCATION")>
	            <#assign shippingPostalAddress = orderContactMechValueMap.postalAddress>
	        </#if>
	    </#list>
	</#if>
    <#assign localeGeo = Static["org.ofbiz.base.util.UtilMisc"].parseLocale("fr")/>
    <#if stateProvinceAbbr?exists && stateProvinceAbbr?has_content && stateProvinceAbbr != "QC"  >
       <#assign localeGeo = Static["org.ofbiz.base.util.UtilMisc"].parseLocale("en")/>
    </#if>
    <#assign uiLabelMap = Static["org.ofbiz.base.util.UtilProperties"].getResourceBundleMap("scadminUiLabels", localeGeo)/>

<fo:table table-layout="fixed" width="100%">
    <fo:table-column column-number="1" column-width="proportional-column-width(90)"/>
    <fo:table-column column-number="2" column-width="proportional-column-width(10)"/>
    <fo:table-body>
	   <fo:table-row>
                      <fo:table-cell>
                         <fo:block color="#FFFFFF">BLANK</fo:block>
                     </fo:table-cell>
        </fo:table-row>
         <fo:table-row>
                      <fo:table-cell>
                         <fo:block color="#FFFFFF">BLANK</fo:block>
                     </fo:table-cell>
        </fo:table-row>
        <fo:table-row>
            <fo:table-cell>
                <fo:block font-size="12pt">
                    <#-- <fo:block font-size="8pt" font-weight="bold">${uiLabelMap.scadminCompanyName}</fo:block> -->
                    <#-- <fo:block>${toPartyGroup.facilityName}</fo:block> -->
                    <#if toPostalAddress?exists>
                        <#if postalAddress?has_content>
                            ${setContextField("postalAddress", toPostalAddress)}
                            ${screens.render("component://party/widget/partymgr/PartyScreens.xml#postalAddressPdfFormatter")}
                        </#if>
                    <#else>
                        <fo:block>${uiLabelMap.CommonNoPostalAddress}</fo:block>
                        <fo:block>${uiLabelMap.CommonFor}: ${toPartyGroup.address1}</fo:block>
                    </#if>
                     <fo:block>${toPhone?if_exists}</fo:block>
                
                </fo:block>
            </fo:table-cell>
            <fo:table-cell text-align="center">
             </fo:table-cell>
        </fo:table-row>
            
        
         <fo:table-row>
                      <fo:table-cell>
                         <fo:block font-size="8pt"> </fo:block>
                      </fo:table-cell>
        </fo:table-row>
		</fo:table-body>
</fo:table>
</#escape>
