<#assign localeGeo = Static["org.ofbiz.base.util.UtilMisc"].parseLocale("en")/>
<#assign uiLabelMap = Static["org.ofbiz.base.util.UtilProperties"].getResourceBundleMap("scadminUiLabels", localeGeo)/>
<#escape x as x?xml>
  <fo:table >
    <fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
    <fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
    <fo:table-body>
    <fo:table-row>
       <fo:table-cell text-align="right" number-columns-spanned="2">
				<fo:block text-align="right">
				    <#if logoImageUrl?exists && logoImageUrl?has_content><fo:external-graphic src="<@ofbizContentUrl>${logoImageUrl!}</@ofbizContentUrl>" overflow="hidden" text-align="right" height="40px" content-height="scale-to-fit" content-width="scale-to-fit"/></#if>
				</fo:block>
            </fo:table-cell>
    </fo:table-row>
    <fo:table-row background-color="#DFDFDF" border-bottom-style="solid" padding-top="10pt" border-bottom-width="thin" border-bottom-color="black" border-top-width="thin" border-top-color="black">
      <fo:table-cell padding="2mm" border="solid 0.1mm black" text-align="center" number-columns-spanned="2" >
		
         <fo:block font-weight="bold" font-size="10pt">RECEIVING SLIP ( ${purchaseOrderReceiptId!} )</fo:block>
		 
			
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row>
      <fo:table-cell font-size="8pt" padding="1mm" border="solid 0.1mm black"><fo:block>Date</fo:block></fo:table-cell>
      <#assign orderDate = "">
      <#assign dateFormat = Static["java.text.DateFormat"].MEDIUM>
     	<#if datetimeReceived?exists && datetimeReceived?has_content>
      <#assign orderDate = Static["java.text.DateFormat"].getDateInstance(dateFormat,locale).format(datetimeReceived)>
     	</#if>	
     
      <fo:table-cell  font-size="8pt" padding="1mm" border="solid 0.1mm black" text-align="center"><fo:block>${orderDate!}</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row>
    <#if orderId?exists>
      <fo:table-cell font-size="8pt" padding="1mm" border="solid 0.1mm black"><fo:block>Purchase Order Number</fo:block></fo:table-cell>
      <fo:table-cell font-size="8pt" padding="1mm" border="solid 0.1mm black" text-align="center"><fo:block>${orderId!}</fo:block></fo:table-cell>
    <#else>
     	<#if transitId?exists>
     		<fo:table-cell font-size="8pt" padding="1mm" border="solid 0.1mm black"><fo:block>Transit Number</fo:block></fo:table-cell>
      		<fo:table-cell font-size="8pt" padding="1mm" border="solid 0.1mm black" text-align="center"><fo:block>${transitId!}</fo:block></fo:table-cell>
     	</#if>
    </#if>
    </fo:table-row>
    <#if poIntrnsitId?exists && poIntrnsitId?has_content>
	    <fo:table-row>
			<fo:table-cell font-size="8pt" padding="1mm" border="solid 0.1mm black"><fo:block>Transit Number</fo:block></fo:table-cell>
			<fo:table-cell font-size="8pt" padding="1mm" border="solid 0.1mm black" text-align="center"><fo:block>${poIntrnsitId!}</fo:block></fo:table-cell>
	    </fo:table-row>

		<#-- SV WEB-2291 PO Module(add load number )(Reviewer BP) -->

	    <fo:table-row>
			<fo:table-cell font-size="8pt" padding="1mm" border="solid 0.1mm black"><fo:block>${uiLabelMap.PO_LOADNUMBER}</fo:block></fo:table-cell>
			<fo:table-cell font-size="8pt" padding="1mm" border="solid 0.1mm black" text-align="center"><fo:block>${poloadNumber!}</fo:block></fo:table-cell>
	    </fo:table-row>
    </#if>
    <#if orderId?exists>
		<fo:table-row>
	      <fo:table-cell font-size="8pt" padding="1mm" border="solid 0.1mm black"><fo:block>Sales Order Number</fo:block></fo:table-cell>
	      <fo:table-cell font-size="8pt" padding="1mm" border="solid 0.1mm black" text-align="center"><fo:block>${salesOrderNumber!}</fo:block></fo:table-cell>
	    </fo:table-row>
    </#if>
	
    </fo:table-body>
  </fo:table>
</#escape>
