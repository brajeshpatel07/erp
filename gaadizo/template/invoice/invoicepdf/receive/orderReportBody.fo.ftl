<#assign localeGeo = Static["org.ofbiz.base.util.UtilMisc"].parseLocale("en")/>
<#assign uiLabelMap = Static["org.ofbiz.base.util.UtilProperties"].getResourceBundleMap("scadminUiLabels", localeGeo)/>
<#escape x as x?xml>
<#assign shipmentItemSize = 0/>
 <fo:table margin-top="5mm">
    <fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
    <fo:table-column column-number="2" column-width="proportional-column-width(20)"/>
    <fo:table-column column-number="3" column-width="proportional-column-width(20)"/>
    <fo:table-column column-number="4" column-width="proportional-column-width(25)"/>
    <fo:table-column column-number="5" column-width="proportional-column-width(5)"/>
    <fo:table-column column-number="6" column-width="proportional-column-width(10)"/>
    <fo:table-body>
	    <fo:table-row background-color="#DFDFDF" padding-top="10pt">
	      <fo:table-cell padding="1mm" border="solid thin black" text-align="center">
	         <fo:block font-size="6pt">FREIGHT </fo:block>
	      </fo:table-cell>
	      <fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="2">
	         <fo:block font-size="6pt">MODE OF DELIVERY </fo:block>
	      </fo:table-cell>
	      <#if carLabel?exists && carLabel?has_content>
		      <fo:table-cell padding="1mm" border="solid thin black" text-align="center">
		         <fo:block font-size="6pt">${carLabel!}</fo:block>
		      </fo:table-cell>
		  </#if>
		  <#if bolLabel?exists && bolLabel?has_content>	
		      <fo:table-cell padding="1mm" border="solid thin black" text-align="center"  
					<#if modeOfDelivery?exists && modeOfDelivery?has_content && modeOfDelivery.equals("Truck")>	number-columns-spanned="3" 
					<#elseif  modeOfDelivery?exists && modeOfDelivery?has_content && modeOfDelivery.equals("Container")>
					<#else> number-columns-spanned="2"</#if>>
		         <fo:block font-size="6pt">${bolLabel!}</fo:block>
		      </fo:table-cell>
		  </#if>
		  <#if carrierLabel?exists && carrierLabel?has_content>			
	      <fo:table-cell padding="1mm" border="solid thin black" text-align="center">
	         <fo:block font-size="6pt">${carrierLabel!}</fo:block>
	      </fo:table-cell>
		  </#if>	
	    </fo:table-row>
	    <fo:table-row>
	      <fo:table-cell padding="1mm" border="solid thin black" text-align="center" >
				<fo:block font-size="6pt">Paid</fo:block>
	      </fo:table-cell>
	      <fo:table-cell padding="1mm" border="solid thin black" text-align="center" number-columns-spanned="2">
				<fo:block font-size="6pt">${modeOfDelivery!}</fo:block>
	      </fo:table-cell>
		  <#if carLabel?exists && carLabel?has_content>	
		      <fo:table-cell padding="1mm" border="solid thin black" text-align="center">
					<fo:block font-size="6pt">${carNo!}</fo:block>
		      </fo:table-cell>
	      </#if>
		  <#if bolLabel?exists && bolLabel?has_content>	
	       <fo:table-cell padding="1mm" border="solid thin black" text-align="center"  
					<#if modeOfDelivery?exists && modeOfDelivery?has_content && modeOfDelivery.equals("Truck")>	number-columns-spanned="3" 
					<#elseif  modeOfDelivery?exists && modeOfDelivery?has_content && modeOfDelivery.equals("Container")>
					<#else> number-columns-spanned="2"</#if>>
		         <fo:block font-size="6pt">${bol!}</fo:block>
	      </fo:table-cell>
	      </#if>
	      <#if carrierLabel?exists && carrierLabel?has_content>			
	      <fo:table-cell padding="1mm" border="solid thin black" text-align="center">
	         <fo:block font-size="6pt">${carrierNo!}</fo:block>
	      </fo:table-cell>
		  </#if>	
	    </fo:table-row>
	</fo:table-body>
  </fo:table> 
<fo:table margin-top="5mm">
    <fo:table-column column-number="1" column-width="proportional-column-width(200)"/>
    <fo:table-body>
	    <fo:table-row background-color="#DFDFDF" padding-top="10pt">
	      <fo:table-cell padding="1mm" border="solid thin black" text-align="left">
	         <fo:block font-size="6pt">Notes </fo:block>
	      </fo:table-cell>
	    </fo:table-row>
	    <fo:table-row>
	      <fo:table-cell padding="1mm" border="solid thin black" text-align="left" >
				<fo:block font-size="6pt" linefeed-treatment="preserve">${notes!}</fo:block>
	      </fo:table-cell>
	    </fo:table-row>
	</fo:table-body>
  </fo:table> 
	    <#-- /header info row -->
<fo:table margin-top="5mm">
    <fo:table-column column-number="1" column-width="proportional-column-width(10)"/>
    <fo:table-column column-number="2" column-width="proportional-column-width(20)"/>
    <fo:table-column column-number="3" column-width="proportional-column-width(20)"/>
    <fo:table-column column-number="4" column-width="proportional-column-width(10)"/>
    <fo:table-column column-number="5" column-width="proportional-column-width(25)"/>
    <fo:table-column column-number="6" column-width="proportional-column-width(5)"/>
    <fo:table-column column-number="7" column-width="proportional-column-width(10)"/>
    <#-- fo:table-column column-number="8" column-width="proportional-column-width(5)"/ -->
    <fo:table-header>
	    <#-- items header row -->
	    <fo:table-row background-color="#DFDFDF" padding-top="10pt">
		   <fo:table-cell padding="1mm" border="solid thin black" text-align="center" >
	         <fo:block font-size="6pt">Item#</fo:block>
	      </fo:table-cell>
	      <fo:table-cell padding="1mm" border="solid thin black" text-align="center"  number-columns-spanned="3" >
	         <fo:block font-size="6pt">Description</fo:block>
	      </fo:table-cell>
	      <fo:table-cell padding="1mm" border="solid thin black" text-align="center" >
	         <fo:block font-size="6pt">${uiLabelMap.PO_ReleaseNumber!""}</fo:block>
	      </fo:table-cell>
	      <fo:table-cell padding="1mm" border="solid thin black" text-align="center" >
	      	
	      	 	<fo:block font-size="6pt">Unit</fo:block>
	      	
	      </fo:table-cell>
	      
	      <fo:table-cell padding="1mm" border="solid thin black" text-align="center">
	         <fo:block font-size="6pt">Qty Received</fo:block>
	      </fo:table-cell>
		 <#-- <fo:table-cell padding="1mm" border="solid thin black" text-align="center" >
	         <fo:block font-size="6pt">Heat Number</fo:block>
	      </fo:table-cell> -->
	    </fo:table-row>
	</fo:table-header>
	<fo:table-body>
	    <#-- /items header row -->
	    <#-- items row -->
        <#list orderShipmentInfoSummaryList as orderShipmentInfoSummary>
        	<#-- SV WEB-2437 (Requirement same as in WEB-2262 -->
			
			<#assign transitItemSeqId = "">
        	<#assign resleseNo = "">
        	<#if orderShipmentInfoSummary.orderItemSeqId?exists && orderShipmentInfoSummary.orderItemSeqId?has_content && poIntrnsitId?exists && poIntrnsitId?has_content>
        		<#assign inTransitItemsList = delegator.findByAnd("InTransitItem", Static["org.ofbiz.base.util.UtilMisc"].toMap("transitId", poIntrnsitId))>
				<#assign  inTransitList = Static["org.ofbiz.entity.util.EntityUtil"].filterByCondition(inTransitItemsList, Static["org.ofbiz.entity.condition.EntityCondition"].makeCondition("shipmentItemSeqId", Static["org.ofbiz.entity.condition.EntityOperator"].EQUALS, orderShipmentInfoSummary.orderItemSeqId))>
		  		<#if inTransitList?has_content>
					<#assign inTransits = Static["org.ofbiz.entity.util.EntityUtil"].getFirst(inTransitList)?if_exists />
					<#if inTransits?has_content && inTransits.releaseNumber?exists && inTransits.releaseNumber?has_content >
						<#assign resleseNo = inTransits.releaseNumber!""/>
						<#assign transitItemSeqId = inTransits.transitItemSeqId!""/>
					</#if>
					<#if inTransits?has_content && inTransits.transitItemSeqId?exists && inTransits.transitItemSeqId?has_content >
						<#assign transitItemSeqId = inTransits.transitItemSeqId!""/>
					</#if>
				</#if>
		   </#if>
			<fo:table-row padding-top="10pt">
			 <fo:table-cell padding="1mm" border="solid thin black" text-align="center" >
					<fo:block font-size="6pt">${Static["org.ofbiz.scadmin.util.SCMAdminUtil"].getSequenceFromInTransitItemSeqId(inTransitItemsList , transitItemSeqId)}</fo:block>
			  </fo:table-cell>
			  <fo:table-cell padding="1mm" border="solid thin black" number-columns-spanned="3" >
			  	 <#assign shipmentItemSize = shipmentItemSize+1/>
				  <fo:block font-size="6pt">${orderShipmentInfoSummary.internalName!}</fo:block>
				  <#if orderShipmentInfoSummary.receivingComments?exists && orderShipmentInfoSummary.receivingComments?has_content>
				  <#assign shipmentItemSize = shipmentItemSize+1/>
			         	<fo:block font-size="6pt" >${orderShipmentInfoSummary.receivingComments!}</fo:block>
			         	<!-- this block is added to match space of border -->
			         	<fo:block font-size="0.3pt" color="#FFFFFF">BLANK</fo:block>
			       </#if>
			  </fo:table-cell>
			  

			<#-- WEB-2291 SV add  ReleaseNumber (Reviewer BP) -->

			
			  <fo:table-cell padding="1mm" border="solid thin black" text-align="center" >
					<fo:block font-size="6pt">${resleseNo!""}</fo:block>
			  </fo:table-cell>
			  <fo:table-cell padding="1mm" border="solid thin black" text-align="center" >
				
					<fo:block font-size="6pt">${orderShipmentInfoSummary.weightUomId!}</fo:block>
				
			  </fo:table-cell>
			  
			  <fo:table-cell padding="1mm" border="solid thin black" text-align="center">
				 <fo:block font-size="6pt">
                 	${Static["org.ofbiz.scadmin.util.SCMAdminUtil"].getValueInQtyFormat(orderShipmentInfoSummary.quantityAccepted, orderShipmentInfoSummary.weightUomId)} ${orderShipmentInfoSummary.internalCode!}
				 </fo:block>
				 
			  </fo:table-cell>
		 
	        </fo:table-row>
		</#list>
		<#assign blankLinesToDraw = 26 - shipmentItemSize />

		<#-- SV WES-29- issue> PO > Format issue!  Alignment off and headers on pages 2 and 3 missing!(reviewer BP) -->

	    <#if (blankLinesToDraw?exists && blankLinesToDraw >= 0)>
		    <#list 1..blankLinesToDraw as index>
		    <fo:table-row >
				<fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" >
		      </fo:table-cell>
		      <fo:table-cell padding="1mm" border="solid thin black" border-bottom-width="0" border-top-width="0" number-columns-spanned="3" >
		      	<fo:block font-size="6pt" color="#FFFFFF">BLANK</fo:block>
		      </fo:table-cell>
		       <fo:table-cell padding="1mm" border="solid thin black" border-bottom-width="0" border-top-width="0" >
		      	<fo:block font-size="6pt" color="#FFFFFF">BLANK</fo:block>
		      </fo:table-cell>
		      <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" >
		      </fo:table-cell>
		      <fo:table-cell padding="1mm" border="solid thin black"  border-bottom-width="0" border-top-width="0" >
		      </fo:table-cell>
		    </fo:table-row>
		    </#list>
			
	     
	    <fo:table-row >

	    	<#-- SV  WEB-2244 : PO>InTransit Receipt>Pdf>Bottom line for item section in pdf coming break -->

		      <fo:table-cell padding="1mm" border="solid thin black" border-bottom-width="1" border-top-width="0" number-columns-spanned="7" >
		      </fo:table-cell>
		</fo:table-row>
		</#if>
    </fo:table-body>
  </fo:table>
<#-- /table -->
</#escape>
