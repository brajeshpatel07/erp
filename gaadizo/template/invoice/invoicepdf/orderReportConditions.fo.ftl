<#escape x as x?xml>

<#if shipGroups?exists && shipGroups.size() gt 1>
    <fo:table table-layout="fixed" border-spacing="3pt" space-before="0.3in" font-size="9pt">
        <fo:table-column column-width="1in"/>
        <fo:table-column column-width="1in"/>
        <fo:table-column column-width="0.5in"/>
        <fo:table-header>
            <fo:table-row font-weight="bold">
                <fo:table-cell><fo:block>${uiLabelMap.OrderShipGroup}</fo:block></fo:table-cell>
                <fo:table-cell><fo:block>${uiLabelMap.OrderProduct}</fo:block></fo:table-cell>
                <fo:table-cell text-align="right"><fo:block>${uiLabelMap.OrderQuantity}</fo:block></fo:table-cell>
            </fo:table-row>
        </fo:table-header>
        <fo:table-body>
            <#list shipGroups as shipGroup>
                <#assign orderItemShipGroupAssocs = shipGroup.getRelated("OrderItemShipGroupAssoc")?if_exists>
                <#if orderItemShipGroupAssocs?has_content>
                    <#list orderItemShipGroupAssocs as shipGroupAssoc>
                        <#assign orderItem = shipGroupAssoc.getRelatedOne("OrderItem")?if_exists>
                        <fo:table-row>
                            <fo:table-cell><fo:block>${shipGroup.shipGroupSeqId}</fo:block></fo:table-cell>
                            <fo:table-cell><fo:block>${orderItem.productId?if_exists}</fo:block></fo:table-cell>
                            <fo:table-cell text-align="right"><fo:block>${shipGroupAssoc.quantity?string.number}</fo:block></fo:table-cell>
                        </fo:table-row>
                    </#list>
                </#if>
            </#list>
        </fo:table-body>
    </fo:table>
</#if>
</#escape>
