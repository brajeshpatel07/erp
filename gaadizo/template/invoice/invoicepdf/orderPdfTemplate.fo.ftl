<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
<#escape x as x?xml>
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format"
    <#-- inheritance -->
    <#if defaultFontFamily?has_content>font-family="${defaultFontFamily}"</#if>
>
    <fo:layout-master-set>
        <fo:simple-page-master master-name="main-page"
              page-width="8.5in" page-height="11in"
              margin-top="0.2in" margin-bottom="0.2in"
              margin-left="0.2in" margin-right="0.2in">
            <#-- main body -->

			<#-- SV -WES-29- issue> PO > Format issue!  Alignment off and headers on pages 2 and 3 missing!(reviewer BP) -->
			<#-- SV WEB-2291 increase margin-top -->
			<fo:region-body margin-top="1.2in" margin-bottom="0.4in"/>
            <#-- the header -->
            <fo:region-before extent="1.2in"/>
            <#-- the footer -->
            <fo:region-after extent="0.4in"/>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="main-page-landscape"
              page-width="11in" page-height="8.5in"
              margin-top="0.4in" margin-bottom="0.4in"
              margin-left="0.6in" margin-right="0.4in">
            <#-- main body -->
            <fo:region-body margin-top="0.1in" margin-bottom="0.4in"/>
            <#-- the header -->
            <fo:region-before extent="0.2in"/>
            <#-- the footer -->
            <fo:region-after extent="0.4in"/>
        </fo:simple-page-master>
    </fo:layout-master-set>

    <fo:page-sequence master-reference="${pageLayoutName?default("main-page")}">

        <#-- Header -->
        <#-- The elements it it are positioned using a table composed by one row
             composed by two cells (each 50% of the total table that is 100% of the page):
             in the left side cell the "topLeft" template is included
             in the right side cell the "topRight" template is included
        -->
        <fo:static-content flow-name="xsl-region-before">
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
                <fo:table-column column-number="2" column-width="proportional-column-width(80)"/>
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell>
							${sections.render("topLeft")}
                        </fo:table-cell>
                        <fo:table-cell padding-left="5mm" padding-right="5mm">
							${sections.render("topRight")}
                        </fo:table-cell>
                    </fo:table-row>
                    
                </fo:table-body>
            </fo:table>
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
                <fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
                <fo:table-body>
                	<fo:table-row width="100%">
            			<fo:table-cell text-align="right" margin-top="0.1in">
            				<fo:block  font-size="10pt" padding-top="10">${invoicePurpose!} </fo:block>
                		</fo:table-cell>
                		<fo:table-cell text-align="right" margin-top="0.1in">
            				<fo:block font-size="8pt" padding-top="10">GST No. : ${partyGroup.officeSiteName!} </fo:block>
                		</fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
                <fo:table-body>
            		<fo:table-row>
                    	<fo:table-cell text-align="center" margin-top="0.2in"  margin-bottom="0.2in">
                			<fo:block border-bottom-style="solid"></fo:block>
                		</fo:table-cell>
                    </fo:table-row>
                 </fo:table-body>
            </fo:table>
        </fo:static-content>

        <#-- the footer -->
        <fo:static-content flow-name="xsl-region-after">
			<#if orderHeader?exists && orderHeader?has_content && ("PURCHASE_ORDER" == orderHeader.orderTypeId)>
				<fo:table table-layout="fixed" width="100%">
					<fo:table-column column-number="1" column-width="proportional-column-width(70)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(30)"/>
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell>
								<fo:block font-size="8pt" text-align="left" margin-top="0.1in">
									Signature : 
								</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="8pt" text-align="left" margin-top="0.1in">
									Authorised By : ${orderHeader.authorisedByPO?if_exists}
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
            </#if>

				<#-- SV -WES-29 issue> PO > Format issue!  add page number(reviewer BP) -->

				<fo:table table-layout="fixed" width="100%">
					<fo:table-body>
						 <fo:table-row>
							<fo:table-cell>
								<fo:block font-size="8pt" text-align="center" margin-top="0.1in"  >
									${uiLabelMap.CommonPage} <fo:page-number/> ${uiLabelMap.CommonOf} <fo:page-number-citation ref-id="theEnd"/>
								 </fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
        </fo:static-content>

        <#-- the body -->
        <fo:flow flow-name="xsl-region-body">
			${sections.render("body")}
            <fo:block id="theEnd"/>  <#-- marks the end of the pages and used to identify page-number at the end -->
        </fo:flow>
    </fo:page-sequence>
</fo:root>
</#escape>
