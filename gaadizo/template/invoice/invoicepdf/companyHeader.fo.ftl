<#escape x as x?xml>
	<#if orderContactMechValueMaps?exists && orderContactMechValueMaps?has_content>
	    <#list orderContactMechValueMaps as orderContactMechValueMap>
	        <#assign contactMech = orderContactMechValueMap.contactMech>
	        <#assign contactMechPurpose = orderContactMechValueMap.contactMechPurposeType>
	        <#if (contactMech.contactMechTypeId == "POSTAL_ADDRESS") && (contactMechPurpose.contactMechPurposeTypeId == "SHIPPING_LOCATION")>
	            <#assign shippingPostalAddress = orderContactMechValueMap.postalAddress>
	        </#if>
	    </#list>
	</#if>
<fo:table width="100%" text-align="left">
    <fo:table-column column-number="1" column-width="proportional-column-width(80)"/>
    <fo:table-body>
        <fo:table-row>
            <fo:table-cell text-align="center">
                    <fo:block font-size="14pt" font-weight="bold" margin-bottom="0.1in" text-align="center">${companyName}</fo:block>
                    <fo:block font-size="10pt" text-align="center">(A Service Franchisee of Gaadizo)</fo:block>
                    <#if postalAddress?exists && postalAddress?has_content>
                            <fo:block font-size="8pt" text-align="center">${postalAddress.address1!}
  							<#if postalAddress.address2?has_content>${postalAddress.address2}</#if>
  							${postalAddress.city!}
  							<#if postalAddress.stateProvinceGeoId?has_content>,
  								${postalAddress.stateProvinceGeoId}
  							</#if> 
  							${postalAddress.postalCode!}
  				       		<#if postalAddress.countryGeoId?has_content><#assign country = postalAddress.getRelatedOne("CountryGeo", true)>
      							${country.get("geoName", locale)?default(country.geoId)}
      						</#if>
      						 </fo:block>
  				   </#if>
               
                 <fo:block font-size="9pt" text-align="center">Tel. : 9818404640, Fax : , Email: naresh@gaadizo.com, Web: www.gaadizo.com</fo:block>
                
            </fo:table-cell>
        </fo:table-row>
	</fo:table-body>
</fo:table>
</#escape>