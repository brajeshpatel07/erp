<script language="JavaScript" type="text/javascript">
    function clearLine(facilityId, orderId, orderItemSeqId, productId, shipGroupSeqId, inventoryItemId, packageSeqId) {
        document.clearPackLineForm.facilityId.value = facilityId;
        document.clearPackLineForm.orderId.value = orderId;
        document.clearPackLineForm.orderItemSeqId.value = orderItemSeqId;
        document.clearPackLineForm.productId.value = productId;
        document.clearPackLineForm.shipGroupSeqId.value = shipGroupSeqId;
        document.clearPackLineForm.inventoryItemId.value = inventoryItemId;
        document.clearPackLineForm.packageSeqId.value = packageSeqId;
        document.clearPackLineForm.submit();
    }
</script>
<#if security.hasEntityPermission("FACILITY", "_VIEW", session)>
    <#assign showInput = requestParameters.showInput?default("Y")>
    <#assign hideGrid = requestParameters.hideGrid?default("N")>

	<#if (requestParameters.forceComplete?has_content && !invoiceIds?has_content)>
        <#assign forceComplete = "true">
        <#assign showInput = "Y">
    </#if>

	<form name="clearPackForm" method="post" action="<@ofbizUrl>ClearPackAll</@ofbizUrl>">
        <input type="hidden" name="orderId" value="${orderId!}"/>
        <input type="hidden" name="shipGroupSeqId" value="${shipGroupSeqId!}"/>
        <input type="hidden" name="facilityId" value="${facilityId!}"/>
    </form>
    <form name="incPkgSeq" method="post" action="<@ofbizUrl>SetNextPackageSeq</@ofbizUrl>">
        <input type="hidden" name="orderId" value="${orderId!}"/>
        <input type="hidden" name="shipGroupSeqId" value="${shipGroupSeqId!}"/>
        <input type="hidden" name="facilityId" value="${facilityId!}"/>
    </form>
    <form name="clearPackLineForm" method="post" action="<@ofbizUrl>ClearPackLine</@ofbizUrl>">
        <input type="hidden" name="facilityId"/>
        <input type="hidden" name="orderId"/>
        <input type="hidden" name="orderItemSeqId"/>
        <input type="hidden" name="productId"/>
        <input type="hidden" name="shipGroupSeqId"/>
        <input type="hidden" name="inventoryItemId"/>
        <input type="hidden" name="packageSeqId"/>
    </form>
    <#-- =============================================================== -->
<div class="col-md-9 col-sm-8">
<div class="rightform">
<div class="title2">
     <span>Customer Invoice for JobCard # ${orderHeaderAndJobCard.jobCardId!}</span>
</div>
<#if orderHeaderAndJobCard?exists && orderHeaderAndJobCard?has_content && orderHeaderAndJobCard.jobCardId?has_content>
	<form action="" method="get">
	
	<div class="row">
		<div class="col-sm-4">
			<label>Customer Name</label>
			<input type="text" name="" class="form-control" placeholder="" value="${personGV.firstName!} ${personGV.lastName!}" readonly>
		</div>

	<div class="col-sm-4">
		<label>Address</label>
		<input type="text" name="" class="form-control" placeholder=""  value="${(partyAndPostalAddressGV.address1)!} ${(partyAndPostalAddressGV.address2)!} ${(partyAndPostalAddressGV.city)!} ${(partyAndPostalAddressGV.postalCode)!}" readonly>
	</div>

	<div class="col-sm-4">
		<label>Vehicle Manufacturer	</label>
		<input type="text" name="" class="form-control" placeholder=""  value="${orderHeaderAndJobCard.vehicleManufacturer!}" readonly>
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		<label>Mobile No.</label>
		<input type="text" name="" class="form-control" placeholder=""  value="${(partyAndPostalAddressGV.contactNumber)!}" readonly>
	</div>

	<div class="col-sm-4">
		<label>Vehicle Model</label>
		<input type="text" name="" class="form-control" placeholder="" value="${orderHeaderAndJobCard.vehicleId!}" readonly>
	</div>
	<div class="col-sm-4">
		<label>Reg.\Vehilce no.</label>
		<input type="text" name="" class="form-control" placeholder="" value="${orderHeaderAndJobCard.registationNumber!}" readonly>
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		<label>Email</label>
		<input type="text" name="" class="form-control" placeholder="" value="${(partyEmail)!}" readonly>
	</div>

	<div class="col-sm-4">
		<label>Variant</label>
		<input type="text" name="" class="form-control" placeholder="" value="${(vehicleGV.variant)!}" readonly>
	</div>

	<div class="col-sm-4">
		<label>Service Date</label>      
		<div class="input-group">
			<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
				<input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text" value="${orderHeaderAndJobCard.serviceDate!}" readonly/>
			</div>
		</div>
   </div>
</div>

<div class="row">
<div class="col-sm-4">
<label>Insurance Company</label>
<input type="text" name="" class="form-control" placeholder="" value="${orderHeaderAndJobCard.vehicleInsuranceComp!}" readonly/>
</div>

<div class="col-sm-4">
<label>Payment Mode</label>
<input type="text" name="" class="form-control" placeholder="" value="${orderHeaderAndJobCard.paymentMode!}" readonly/>
</div>

<div class="col-sm-4">
<label>Previous Service Date</label>
<div class="input-group">
<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
<input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text" value="${orderHeaderAndJobCard.lastServiceDate!}" readonly/>
</div>
</div>

</div>

<#-- div class="row">
	<div class="col-sm-4">
		<label>Insurance Company</label>
		<div class="input-group">
			<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
			<input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text" value="${orderHeaderAndJobCard.jobCardId!}" readonly/>
		</div>
	</div>

	<div class="col-sm-4">
		<label>Payment mode</label>
		<select class="form-control">
			<option>Credit Card</option>
		</select>
	</div>

	<div class="col-sm-4">
		<label>Previous Service Date</label>
		<div class="input-group">
			<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
			<input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text" value="${orderHeaderAndJobCard.jobCardId!}" readonly/>
		</div>
	</div>
</div -->

<div class="row form-group">
<div class="col-sm-4">
<label>Insurance Expiry Date</label>
<div class="input-group">
<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
<input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text" value="${orderHeaderAndJobCard.insuranceExpiryDate!}" readonly/>
</div>
</div>

<div class="col-sm-4">
<label>Customer Type</label>
<input type="text" name="" class="form-control" placeholder="" value="${orderHeaderAndJobCard.customerType!}" readonly/>
</div>

<div class="col-sm-4">
<label>Job Card Creation time</label>
<input type="text" name="" class="form-control" placeholder="" value="${orderHeaderAndJobCard.createdDate!}" readonly/>
</div>

</div>
<#-- input type="submit" class="btn btn-danger" value="Submit" -->
</form>
<#assign jobCardItemCount = 0 />
 <form name="multiPackForm" method="post" action="<@ofbizUrl>ProcessBulkPackOrder</@ofbizUrl>">
<div class="title2 margintop20"><span>Service Detail</span></div>
 
<div class="table-responsive margintop20">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabledetails">
  <tr>
    <th>Srl No.</th>
    <th>Service item ID</th>
    <th>Description</th>
    <th>Remarks</th>
    <th>Service Charge</th>
    <th>Discount</th>
    <th>Taxable Amount</th>
    <th>Tax%</th>
    <th>Tax Amount</th>
    <th>Total Amount</th>
  </tr>
       <input type="hidden" name="facilityId" value="${orderHeaderAndJobCard.serviceProviderId!}">
       <input type="hidden" name="orderId" value="${orderHeaderAndJobCard.orderId!}">
       <input type="hidden" name="shipGroupSeqId" value="00001">
       <input type="hidden" name="originFacilityId" value="${orderHeaderAndJobCard.serviceProviderId!}">
       <input type="hidden" name="hideGrid" value="N">
  	   <#assign servicesSubTotal = Static["java.math.BigDecimal"].ZERO />
  	   <#if serviceMap?exists && serviceMap?has_content>
	   <#list serviceMap.keySet() as serviceProduct>
  			<tr>
  	  			<#assign orderItem = serviceMap[serviceProduct]!/>
                <#assign shippedQuantity = orderReadHelper.getItemShippedQuantity(orderItem)!>
                <#assign orderItemQuantity = orderItem.quantity/>
                <#assign orderProduct = serviceMap.get(serviceProduct)!/>
                <#assign product = Static["org.apache.ofbiz.product.product.ProductWorker"].findProduct(delegator, orderProduct.productId)!/>
                <#assign inputQty = orderItemQuantity />
		    	<td><input type="checkbox" name="sel_${jobCardItemCount}" value="Y" <#if (inputQty >0)>checked=""</#if>/>${serviceProduct_index+1}</td>
    			<td>${productMap.get(orderItem.productId).productId!}</td>
    			<td>${productMap.get(orderItem.productId).productName!}</td>
    			<td>${productMap.get(orderItem.productId).productTypeId!}</td>
				<td><@ofbizCurrency amount=orderItem.unitPrice isoCode="INR"/></td>
    			<td><@ofbizCurrency amount=orderReadHelper.getOrderItemAdjustmentsTotal(orderItem) isoCode="INR"/></td>
    			<td><@ofbizCurrency amount=orderItem.unitPrice isoCode="INR"/></td>
    			<td><@ofbizCurrency amount=orderReadHelper.getOrderItemAdjustmentsTotal(orderItem) isoCode="INR"/></td>
    			<td><@ofbizCurrency amount=orderReadHelper.getOrderItemAdjustmentsTotal(orderItem) isoCode="INR"/></td>
    			<td><@ofbizCurrency amount=orderReadHelper.getOrderItemSubTotal(orderItem) isoCode="INR"/></td>
    			<#assign servicesSubTotal =  servicesSubTotal.add(orderReadHelper.getOrderItemSubTotal(orderItem)) />
  			</tr>
			<tr><td>
                <input type="hidden" size="7" name="qty_${jobCardItemCount}" value="${inputQty}" />
                <input type="hidden" size="7" name="wgt_${jobCardItemCount}" value="" />
                <#-- td align="center">
                    <select name="pkg_${jobCardItemCount}">
                        <#if packingSession.getPackageSeqIds()?exists>
                        	<#list packingSession.getPackageSeqIds() as packageSeqId>
                            	<option value="${packageSeqId}">${uiLabelMap.ProductPackage} ${packageSeqId}</option>
                            </#list>
                           	<#assign nextPackageSeqId = packingSession.getPackageSeqIds().size() + 1>
                            <option value="${nextPackageSeqId}">${uiLabelMap.ProductNextPackage}</option>
                        </#if>
                    </select>
                </td -->
                <input type="hidden" size="7" name="numPackages_${jobCardItemCount}" value="1" />
                <input type="hidden" name="prd_${jobCardItemCount}" value="${orderItem.productId!}"/>
                <input type="hidden" name="ite_${jobCardItemCount}" value="${orderItem.orderItemSeqId}"/>
                <#assign jobCardItemCount = jobCardItemCount + 1>
            </td></tr>
  		</#list>
  		</#if>
  		<tr><td><div class="title2 margintop20"><span>Spare Part Details</span></div></td></tr>
  		 <tr>
    		<th>Srl No.</th>
    		<th>Part No.</th>
    		<th>Part Name</th>
    		<th>Brand</th>
    		<th>Price</th>
    		<th>Discount</th>
    		<th>Taxable Amount</th>
    		<th>Tax%</th>
    		<th>Tax Amount</th>
    		<th>Total Amount</th>
  		  </tr>
  		  <#assign sparePartSubTotal =  Static["java.math.BigDecimal"].ZERO  />
  		  <#if sparePartMap?exists && sparePartMap?has_content>
  		  <#list sparePartMap.keySet() as sparePartProduct>
  	<tr>
         	<#assign orderItem = sparePartMap[sparePartProduct]/>
                        <#assign shippedQuantity = orderReadHelper.getItemShippedQuantity(orderItem)!>
                        <#assign orderItemQuantity = orderItem.quantity/>
                        <#assign orderProduct = sparePartMap.get(sparePartProduct)!/>
                        <#assign product = Static["org.apache.ofbiz.product.product.ProductWorker"].findProduct(delegator, sparePartMap.get(sparePartProduct).productId)!/>
                        <#assign inputQty = orderItemQuantity />
  	    
    	<td><input type="checkbox" name="sel_${jobCardItemCount}" value="Y" <#if (inputQty > 0)>checked=""</#if>/>${sparePartProduct_index+1}</td>
    	<td>${productMap.get(orderItem.productId).productId!}</td>
    	<td>${productMap.get(orderItem.productId).productName!}</td>
    	<td>${productMap.get(orderItem.productId).productTypeId!}</td>
    	<td><@ofbizCurrency amount=orderItem.unitPrice isoCode="INR"/></td>
    	<td><@ofbizCurrency amount=orderReadHelper.getOrderItemAdjustmentsTotal(orderItem) isoCode="INR"/></td>
    	<td><@ofbizCurrency amount=orderItem.unitPrice isoCode="INR"/></td>
    	<td><@ofbizCurrency amount=orderReadHelper.getOrderItemAdjustmentsTotal(orderItem) isoCode="INR"/></td>
    	<td><@ofbizCurrency amount=orderReadHelper.getOrderItemAdjustmentsTotal(orderItem) isoCode="INR"/></td>
    	<td><@ofbizCurrency amount=orderReadHelper.getOrderItemSubTotal(orderItem) isoCode="INR"/></td>
    	<#assign sparePartSubTotal =  sparePartSubTotal.add(orderReadHelper.getOrderItemSubTotal(orderItem)) />
  	</tr>
  	  <tr> <td>              
                          
                            <input type="hidden" size="7" name="qty_${jobCardItemCount}" value="${inputQty!}" />
                            <input type="hidden" size="7" name="wgt_${jobCardItemCount}" value="" />
                            <input type="hidden" size="7" name="pkg_${jobCardItemCount}" value="1" />
                            
                            <input type="hidden" size="7" name="numPackages_${jobCardItemCount}" value="1" />
                          <input type="hidden" name="prd_${jobCardItemCount}" value="${(orderItem.productId)!}"/>
                          <input type="hidden" name="ite_${jobCardItemCount}" value="${(orderItem.orderItemSeqId)!}"/>
                         <#assign jobCardItemCount = jobCardItemCount + 1>
                         </td></tr>
  	
  </#list>
  </#if>
  <input type="submit" value="${uiLabelMap.ProductPackItem}" />
  
	</table>
</div> </form>



 
<div class="table-responsive margintop20">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabledetails">
  <tr>
    <th>Srl No.</th>
    <th>Part No.</th>
    <th>Part Name</th>
    <th>Brand</th>
    <th>Price</th>
    <th>Discount</th>
    <th>Taxable Amount</th>
    <th>Tax%</th>
    <th>Tax Amount</th>
    <th>Total Amount</th>
  </tr>
  
 

  
</table>
</div>
<#-- ==================================================== -->

<!-- complete form -->
              <#if showInput != "N">
                <form name="completePackForm" method="post" action="<@ofbizUrl>CompletePack</@ofbizUrl>">
                  <input type="hidden" name="orderId" value="${orderId!}"/>
                  <input type="hidden" name="shipGroupSeqId" value="${shipGroupSeqId!}"/>
                  <input type="hidden" name="facilityId" value="${facilityId!}"/>
                  <input type="hidden" name="forceComplete" value="${forceComplete?default('false')}"/>
                  <input type="hidden" name="weightUomId" value="${defaultWeightUomId!}"/>
                  <input type="hidden" name="showInput" value="N"/>
                  <hr/>
                  <table class="basic-table" cellpadding="2" cellspacing='0'>
                    <tr>
                    <#if packingSession?exists && packingSession?has_content>
                        <#assign packageSeqIds = packingSession.getPackageSeqIds()/>
                        <#if packageSeqIds?has_content>
                            <td>
                                <span class="label">${uiLabelMap.ProductPackedWeight} (${("uiLabelMap.ProductShipmentUomAbbreviation_" + defaultWeightUomId)?eval}):</span>
                                <br />
                                <#list packageSeqIds as packageSeqId>
                                    ${uiLabelMap.ProductPackage} ${packageSeqId}
                                    <input type="text" size="7" name="packageWeight_${packageSeqId}" value="${packingSession.getPackageWeight(packageSeqId?int)!}" />
                                    <br />
                                </#list>
                                <#if orderItemShipGroup?has_content>
                                    <input type="hidden" name="shippingContactMechId" value="${orderItemShipGroup.contactMechId!}"/>
                                    <input type="hidden" name="shipmentMethodTypeId" value="${orderItemShipGroup.shipmentMethodTypeId!}"/>
                                    <input type="hidden" name="carrierPartyId" value="${orderItemShipGroup.carrierPartyId!}"/>
                                    <input type="hidden" name="carrierRoleTypeId" value="${orderItemShipGroup.carrierRoleTypeId!}"/>
                                    <input type="hidden" name="productStoreId" value="${productStoreId!}"/>
                                </#if>
                            </td>
                            <#if carrierShipmentBoxTypes?has_content>
                              <td>
                                <span class="label">${uiLabelMap.ProductShipmentBoxType}</span>
                                <br/>
                                <#list packageSeqIds as packageSeqId>
                                  <select name="boxType_${packageSeqId}">
                                    <option value=""></option>
                                    <#list carrierShipmentBoxTypes as carrierShipmentBoxType>
                                      <#assign shipmentBoxType = carrierShipmentBoxType.getRelatedOne("ShipmentBoxType", false) />
                                      <option value="${shipmentBoxType.shipmentBoxTypeId}">${shipmentBoxType.description?default(shipmentBoxType.shipmentBoxTypeId)}</option>
                                    </#list>
                                  </select>
                                  <br/>
                                </#list>
                              </td>
                            </#if>
                        </#if>
                        <td nowrap="nowrap">
                            <span class="label">${uiLabelMap.ProductAdditionalShippingCharge}:</span>
                            <br />
                            <input type="text" name="additionalShippingCharge" value="${packingSession.getAdditionalShippingCharge()!}" size="20"/>
                            <#if packageSeqIds?has_content>
                                <a href="javascript:document.completePackForm.action='<@ofbizUrl>calcPackSessionAdditionalShippingCharge</@ofbizUrl>';document.completePackForm.submit();" class="buttontext">${uiLabelMap.ProductEstimateShipCost}</a>
                                <br />
                            </#if>
                        </td>
                      <td>
                        <span class="label">${uiLabelMap.ProductHandlingInstructions}:</span>
                        <br />
                        <textarea name="handlingInstructions" rows="2" cols="30">${packingSession.getHandlingInstructions()!}</textarea>
                      </td>
                      <td align="right">
                        <div>
                          <#assign buttonName = "${uiLabelMap.ProductComplete}">
                          <#if forceComplete?default("false") == "true">
                            <#assign buttonName = "${uiLabelMap.ProductCompleteForce}">
                          </#if>
                          <input type="button" value="${buttonName}" onclick="javascript:document.completePackForm.submit();"/>
                        </div>
                      </td>
                      </#if>
                    </tr>
                  </table>
                  <br />
                </form>
              </#if>
<#-- ============================================================= -->
               
<div class="table-responsive margintop20">
<table border="0" cellspacing="0" cellpadding="0" class="tabletotle">
  <tr>
    <td>Service Total</td>
    <td><@ofbizCurrency amount=servicesSubTotal isoCode="INR"/></td>
  </tr>
  <tr>
    <td>Spare Parts Total</td>
    <td><@ofbizCurrency amount=sparePartSubTotal isoCode="INR"/></td>
  </tr>
  <tr>
    <td>GST @ 18%</td>
    <td>457.28</td>
  </tr>
  <tr>
    <td>GST@ 14%</td>
    <td>49</td>
  </tr>
  <tr>
    <td>GST @ 28%</td>
    <td>224</td>
  </tr>
  <tr>
    <td>Total Tax Amount</td>
    <td>730.29</td>
  </tr>
  <tr>
    <td><strong>Invoice Value</strong></td>
    
    <td><strong><#if orderReadHelper?exists && orderReadHelper?has_content><@ofbizCurrency amount=orderReadHelper.getOrderGrandTotal() isoCode="INR"/></#if></strong></td>
  </tr>
</table>

</div>


<div class="row">
<div class="col-sm-4">
<label>Payment Mode</label>
<select class="form-control">
<option>Debit Card</option>
</select>
</div>

<div class="col-sm-8">
<div class="button-tab">

<#-- if orderHeaderAndJobCard.status?has_content && ("PERFORM_INVOICE_GENE" != orderHeaderAndJobCard.status!)> -->
	<div class="form-actions mt-10 fright">
        <form method="post" action="<@ofbizUrl>generatePerFormInvoiceForJobCard</@ofbizUrl>">
            <input type="hidden" name="facilityId" value="${orderHeaderAndJobCard.serviceProviderId!}"/>
            <input type="hidden" name="orderIdList" value="${orderHeaderAndJobCard.orderId!}"/>
            <input type="hidden" name="jobCardId" value="${orderHeaderAndJobCard.jobCardId!}"/>
            <input type="hidden" size="4" name="maxNumberOfOrders" value="20"/>
            <input  class="btn btn-success btn-lable-wrap left-label" type="submit" value="Post Perform Invoice"/>
            <button type="submit" name="" class="btn cancel">Post</button>
        </form> 
	</div>
<#-- </#if> -->

<button type="submit" name="" class="btn create">Print</button>
<button type="submit" name="" class="btn order">Email</button></div>
</div>
</div>


</div>
</div>


</div>
</section>

  
</#if> 
</#if> 
<#-- script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
<script>
$(document).ready(function(){
var date_input=$('input[name="date"]'); //our date input has the name "date"
var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
date_input.datepicker({
	format: 'mm/dd/yyyy',
	container: container,
	todayHighlight: true,
	autoclose: true,
})
})
</script>
</body>
</html -->
